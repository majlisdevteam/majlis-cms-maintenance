import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from "../service/common-web.service";
import { NotificationModel } from "../model/Notification";
import { FileUploader } from "ng2-file-upload";
import { AppParams } from "../app.module";
import { CMSUsersModel } from "../model/CMSUsers";
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";
import { GroupModel } from "../model/Group";
import { EventModel } from "../model/Event";
import { MDCodeModel } from "../model/MDCode";
import { LoignValidator } from "../service/login-validator";
import { NotificationSendModel } from "../model/NotificationSend";
import { AlertComponent } from "../alert/alert.component";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  providers: [CommonWebService, NotificationModel, AlertComponent]
})
export class NotificationComponent implements OnInit {

  fakeArray = new Array(12);
  public NotificationModelArr = new Array<NotificationModel>();
  public arrTemplates = [];
  public NotificationModel = new NotificationModel();

  public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-NotificationManagement-1.0/service/notification/saveNotificationIcon", queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  public showLoader: boolean = true;

  public confirmLoader = false;

  public successMessage = "";
  public isSuccess: boolean;
  public status: String;
  public groupId2 = 0;

  public showAddNewWindow = true;
  public sendType;

  public notificationTitleBar = "New Notification";

  public avilNoti;

  public editMode = false;

  public notificationStatus;

  public sentNoti;

  public isSuperAdmin;

  public selectedNoitificationObj = new NotificationModel();

  public waitForNotiCount = true;

  public selectedNotificationId: number;

  public CMSUser = new CMSUsersModel();
  public groupModel = new Array<GroupModel>();
  public eventModel = new Array<EventModel>();

  public successMsg;

  public failMessage;

  @ViewChild("sendNotification") sendNotification;

  @ViewChild("confirmation") confirmation;

  @ViewChild("success") success;

  @ViewChild("fail") fail;

  @ViewChild("newNotification") newNotification;

  constructor(public commonService: CommonWebService, public validator: LoignValidator, public alert: AlertComponent) {


    this.validator.validateUser("notification");


  }

  ngOnInit() {

    this.isSuperAdmin = this.validator.user.groups[0].groupId == 'SuperAdminGrp';

    this.loadNotificationCount()

    this.loadToGrid()
  }

  loadNotificationCount() {

    this.waitForNotiCount = true;

    let lotificationCountUrl;

    console.log("validator 0 ", this.validator.user);

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      console.log("validator 1 ", this.validator.user);
      lotificationCountUrl = AppParams.BASE_PATH + "groups/getCount";
    } else {
      console.log("validator 2 ", this.validator.user);
      lotificationCountUrl = AppParams.BASE_PATH + "groups/getCount?userId=" + this.validator.getUserId();
    }

    this.commonService.processGet(lotificationCountUrl).subscribe(respose => {

      let res = respose.getCount.responseData;

      this.avilNoti = res.tot_notification_limit;

      this.sentNoti = res.tot_notification;

      if (this.avilNoti == -1) {
        this.avilNoti = "Unlimited";
      }


      this.waitForNotiCount = false;

    }, error => {

    });
  }

  openSendNotificationEdit(notificationId) {

    this.editMode = true;

    this.notificationTitleBar = "Edit Notification"

    let notification = this.NotificationModelArr.filter(e => {
      return e.notificationId == notificationId;
    })[0];

    console.log("notification ", notification);

    this.NotificationModel = notification;

    this.loadGroupsToList(0, 5000);
    this.newNotification.show();

    let url = AppParams.BASE_PATH + "events/getEventsByGroupId?groupId=" + notification.notificationGroupId.groupId;
    // let url = "http://192.0.0.59:8080/Majlis-EventManagement_merge-1.0/service/events/getEventsByGroupId?groupId="+ this.groupId2;
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      //this.groupModel = res.getGroupList.responseData;
      this.eventModel = res.getEventsByGroupId.responseData;
      console.log("events ", this.eventModel)


    }, error => {

      console.log("error ", error);

    });

  }

  loadToGrid() {

    let urlPublic;
    // let urlPublic = "http://192.0.0.59:8080/Majlis-NotificationManagement-1.0/service/notification/getAllNotificationList";


    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      urlPublic = AppParams.BASE_PATH + "notification/getAllNotificationList";
    } else {
      urlPublic = AppParams.BASE_PATH + "notification/getAllNotificationList?userId=" + this.validator.getUserId();
    }

    this.commonService.processGet(urlPublic).subscribe(res => {

      console.log("res", res);

      let response = res.getAllNotificationList;

      if (response.responseCode != 999) {


        this.NotificationModelArr = response.responseData;
        this.showLoader = false;


      }

      console.log("NotificationModelArr", this.NotificationModelArr)

      // this.loadGroupsToList(0, 50);


    }, error => {
      console.log("console ", error);
    });



    this.uploader.onAfterAddingFile = (file) => {
      this.showAddNewWindow = true;
      file.withCredentials = false;

    }

    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);
      this.showAddNewWindow = false;
      console.log("aaa", responsePath);
      this.NotificationModel.notificationIconPath = responsePath.responseData;
    }

  }

  selectEvents(target) {

    this.NotificationModel.notificationEventId = this.eventModel.filter(e => {
      return e.eventCaption == target.value;
    })[0];


    if (this.editMode == true) {

    } else {

    }


  }

  saveNewNotification(evt) {

    this.showAddNewWindow = true;

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    //  let reader=new FileReader();

    console.log("saveNewNotification ", this.NotificationModel);
    let url = AppParams.BASE_PATH + "notification/createNotification";

    this.CMSUser.userId = this.validator.getUserId();
    this.NotificationModel.notificationUserId = this.CMSUser;

    // let status:MDCodeModel;
    // status.codeId =  this.status;//parseInt(this.status);

    // console.log("status",this.status);

    this.NotificationModel.notificationStatus = new MDCodeModel();

    this.NotificationModel.notificationStatus.codeId = this.notificationStatus;

    this.commonService.processPostWithHeaders(url, this.NotificationModel, option).subscribe(res => {

      let response = res.createNotification
      this.showAddNewWindow = false;
      if (response.responseCode == 1) {

        this.loadToGrid();
        this.newNotification.hide();
        this.successMessage = "Notification has been saved!";
        this.success.show();

      } else {
        this.loadToGrid();
        this.failMessage = "Cannot create notification at this time. Please try agian later!"
        this.fail.show();
      }

    }, error => {
      this.loadToGrid();
      console.log("error >>> ", error);
      this.showAddNewWindow = false;

      this.failMessage = "Cannot connect to the server."
      this.fail.show();

    })
  }


  saveNewNotificationChanges() {

    this.showAddNewWindow = true;

    this.NotificationModel.notificationStatus = new MDCodeModel();

    this.NotificationModel.notificationStatus.codeId = this.notificationStatus;

    console.log(">>> ", this.NotificationModel);

    let url = AppParams.BASE_PATH + "notification/updateNotificationCms";

    this.commonService.processPost(url, this.NotificationModel).subscribe(res => {

      this.isSuccess = true;

      console.log("Notification send", res);

      let response = res.updateNotificationCms

      if (response.responseCode == 1) {
        this.successMessage = "Notification send!"

        this.newNotification.hide();

        this.successMessage = "Notification has been updated!";
        this.success.show();


      } else {

        this.loadToGrid();
        this.failMessage = "Cannot save the notification at this time. Trye agin later!"
        this.fail.show();
      }
      this.showAddNewWindow = false;

    }, error => {
      this.failMessage = "Cannot save the notification at this time. Trye agin later!"
      this.fail.show();
      this.loadToGrid();

      this.showAddNewWindow = false;

    })


  }

  sendToConfirm() {
    this.confirmLoader = true;
    let url = AppParams.BASE_PATH + "notification/createNotificationDetail";

    let notifi = new NotificationSendModel();
    let type = this.sendType;

    console.log("notifi", this.NotificationModel);

    notifi.notificationId = this.NotificationModel.notificationId;

    if (type == 17) {

      notifi.notificationSendTo = this.NotificationModel.notificationGroupId.groupId;

    } else if (type == 18 || type == 19 || type == 20) {

      notifi.notificationSendTo = this.NotificationModel.notificationEventId.eventId;

    }

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("userGroup", this.validator.user.groups[0].groupId);

    option.headers = header

    notifi.notifictionType = type;

    this.commonService.processPostWithHeaders(url, notifi, option).subscribe(res => {

      this.isSuccess = true;

      console.log("Notification send", res);

      try {
        let response = res.createNotificationDetail;

        this.confirmation.hide();

        if (response.responseCode == 1) {
          this.successMessage = "Notification has been sent to the relevant parties";
          this.success.show();


        } else if (response.responseCode == 2) {
          this.failMessage = "Operation failed! You have exceeded your allowed notification quota"
          this.fail.show();
        } else {
          this.failMessage = "Operation cannot complete at this time. Please try agian later"
          this.fail.show();

        }
        this.loadNotificationCount();

        this.loadToGrid();

        this.confirmLoader = false;

      } catch (error) {
        this.confirmation.hide();
        this.failMessage = "Cannot connect to the server."
        this.fail.show();
      }



    }, error => {

      console.log("error >>> ", error);
      this.confirmation.hide();
      this.failMessage = "Cannot connect to the server."
      this.fail.show();

    });

  }

  sendTo(type) {

    this.sendType = type;

    this.sendNotification.hide();

    this.confirmation.show();



  }


  openSendNotificationForm(selectedNoitificationObj) {



    this.NotificationModel = selectedNoitificationObj;


    this.sendNotification.show();

  }


  loadGroupsToList(start, limit) {

    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "groups/getAllGroupsById";
    } else {
      url = AppParams.BASE_PATH + "groups/getAllGroupsById?user=" + this.validator.getUserId();
    }


    // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getAllGroupsById?userId=1";
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.groupModel = res.getAllGroupsById.responseData;
      // this.groupModel = res.responseData;
      console.log("response api ", this.groupModel)
      this.showAddNewWindow = false;

    }, error => {

      console.log("error ", error);

    })


  }

  selectEventsForGroups(target) {

    this.showAddNewWindow = true;

    this.NotificationModel.notificationGroupId = this.groupModel.filter(e => {
      return e.groupTitle == target.value;
    })[0];

    console.log("notification mode", this.NotificationModel);

    this.groupId2 = this.NotificationModel.notificationGroupId.groupId;
    console.log("groupId if", this.groupId2);


    // this.groupId =5;
    let url = AppParams.BASE_PATH + "events/getEventsByGroupId?groupId=" + this.groupId2;
    // let url = "http://192.0.0.59:8080/Majlis-EventManagement_merge-1.0/service/events/getEventsByGroupId?groupId="+ this.groupId2;
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      //this.groupModel = res.getGroupList.responseData;
      this.eventModel = res.getEventsByGroupId.responseData;
      console.log("events ", this.eventModel)

      this.showAddNewWindow = false;
    }, error => {

      this.showAddNewWindow = false;

    })

  }


  openAddNew() {

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-NotificationManagement-1.0/service/notification/saveNotificationIcon", queueLimit: 1 });
    this.uploader.onAfterAddingFile = (file) => {
      this.showAddNewWindow = true;
      file.withCredentials = false;

    }

    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);
      this.showAddNewWindow = false;
      console.log("aaa", responsePath);
      this.NotificationModel.notificationIconPath = responsePath.responseData;
    }

    this.NotificationModel = new NotificationModel();

    console.log("notification model", this.NotificationModel);

    this.editMode = false;
    this.loadGroupsToList(0, 5000);

    this.notificationTitleBar = "Add New Notification"

    this.newNotification.show();
  }

}
