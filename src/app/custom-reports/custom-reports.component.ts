import { Component, OnInit, ViewChild } from '@angular/core';
// import { PagerService } from "../_services/index";
import { Http, Response, Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";
import { CommonWebService } from "../service/common-web.service";
import { CustomReportModel } from "../model/CustomReport";
import { AppParams } from "../app.module";
import { LoignValidator } from "../service/login-validator";

@Component({
  selector: 'app-custom-reports',
  templateUrl: './custom-reports.component.html',
  styleUrls: ['./custom-reports.component.css'],
  providers: [CommonWebService]
})
export class CustomReportsComponent implements OnInit {
  successMessage: string;

  fakeArray = new Array(12);


  public CustomReportModelArr = new Array<CustomReportModel>();
  public customReportModel = new CustomReportModel();
  public customReportModelEdit = new CustomReportModel();
  public reportId: number;
  public showLoader: boolean = true;

  constructor(private http: Http, public commonService: CommonWebService, public validator: LoignValidator) {

    this.validator.validateUser("custom-reports");

  }

  @ViewChild("editReport") editReportForm;

  ngOnInit() {

    this.loadToGrid()



  }

  loadToGrid() {

    let urlPublic = AppParams.BASE_PATH + "reports/getReportList";
    //let urlPublic = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/getReportList";

    this.commonService.processGet(urlPublic).subscribe(res => {
      console.log("res", res);

      let response = res.getReportList;

      if (response.responseCode != 999) {

        this.CustomReportModelArr = response.responseData;
        this.showLoader = false;

        console.log("this.CustomReportModelArr", this.CustomReportModelArr);
      }


    }, error => {
      console.log("console ", error);
    });

  }

  openEditReportForm(reportId) {

    console.log("reportId", reportId);
    this.reportId = reportId;

    let urlPublic = AppParams.BASE_PATH + "reports/getReportDetail?reportId=" + reportId;
    //let urlPublic = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/getReportDetail?reportId=" + reportId;

    this.commonService.processGet(urlPublic).subscribe(res => {

      console.log("res", res);

      let response = res.getReportDetail;

      if (response.responseCode != 999) {
        this.customReportModelEdit = response.responseData[0];
        // this.customReportModelEdit = res.responseData[0];
        console.log("this.customReportModelEdit", this.customReportModelEdit);

      }
    });
    this.editReportForm.show();

  }

  saveNewReport(evt) {

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    let url = AppParams.BASE_PATH + "reports/createReport";
    //let url = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/createReport";
    this.commonService.processPostWithHeaders(url, this.customReportModel,option).subscribe(res => {


      let response = res.createReport

      if (response.responseCode == 1) {
        //if (res.responseCode == 1) {
        this.loadToGrid()
        this.successMessage = "Report Added!"
        this.customReportModel.query = "";
        this.customReportModel.reportName = "";


      } else {
        this.successMessage = "Error in Report creation!"
      }

    }, error => {

      console.log("error >>> ", error);

    })
  }




  editReportDetails() {

          let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    console.log("inside edit report..");
    let url = AppParams.BASE_PATH + "reports/updateReport";
    //let url = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/updateReport";

    this.commonService.processPostWithHeaders(url, this.customReportModelEdit,option).subscribe(res => {


      let response = res.updateReport;

      if (response.responseCode == 1) {
        // if (res.responseCode == 1) {
        this.successMessage = "Report Edited!"


      } else {
        this.successMessage = "Error in Report Edit!"
      }

    }, error => {

      console.log("error >>> ", error);

    });

    console.log("this.successMessage", this.successMessage);
  }




  generateReport(query) {

  

    console.log("query", query);
    let urlPublic = AppParams.MEDIA_SERVER + "Majlis-CustomReports-1.0/service/reports/getReport?query=" + query;
    console.log("url ",urlPublic)
    window.open(urlPublic,"_blank");
  
    console.log("report not genereated");

  }





}


  // setPage(page: number) {
  //       if (page < 1 || page > this.pager.totalPages) {
  //           return;
  //       }

  //       // get pager object from service
  //       this.pager = this.pagerService.getPager(this.allItems.length, page);

  //       // get current page of items
  //       this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  //   }



