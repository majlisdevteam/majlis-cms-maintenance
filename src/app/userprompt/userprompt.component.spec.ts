import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserpromptComponent } from './userprompt.component';

describe('UserpromptComponent', () => {
  let component: UserpromptComponent;
  let fixture: ComponentFixture<UserpromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserpromptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserpromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
