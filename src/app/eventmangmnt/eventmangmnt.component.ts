import { Component, OnInit, ViewChild } from '@angular/core';
import { NguiMapModule } from '@ngui/map';
import { EventModel } from "../model/Event" // import Event model from the model package
import { CommonWebService } from "../service/common-web.service";//Add common service class
import { CategoryModel } from "../model/Category"
import { MDCodeModel } from "../model/MDCode"
import { GroupModel } from "../model/Group";
import { CommentModel } from "../model/Comment"
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { AppParams } from "../app.module";
import { LoignValidator } from "../service/login-validator";
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";
import { TabsetConfig } from "ng2-bootstrap";
import { DeleteEventDataModel } from "../model/DeleteEventData";
import { EventImagesModel } from "../model/EventImages";
import { EventVideoModel } from "../model/EventVideo";
import { EventParticipantsModel } from "../model/EventParticipants";


@Component({
  selector: 'app-eventmangmnt',
  templateUrl: './eventmangmnt.component.html',
  styleUrls: ['./eventmangmnt.component.css'],
  providers: [CommonWebService, EventModel, CategoryModel, GroupModel, EventParticipantsModel, TabsetConfig, CommentModel, EventImagesModel]
})
export class EventmangmntComponent implements OnInit {
  statusAction: any;
  statusUserId: any;
  participantsLoader: boolean = false;
  eventParticipantsList: EventParticipantsModel[];
  eventVideoLoader: boolean;
  videoList: EventVideoModel[];

  imgArray = new Array<EventImagesModel>();
  eventImageLoader: boolean;
  deleteEventData: DeleteEventDataModel;
  commentsLoader: boolean;
  public position: google.maps.LatLng;
  failMessage: string;

  fakeArray = new Array(12);
  fakeArrayImages = new Array(20);

  public commentList = new Array<CommentModel>();

  public mode = "create";

  editOpertionProcess = false;

  @ViewChild('videoPlayer') videoplayer: any;

  @ViewChild("newEvent") newEvent;

  @ViewChild("viewInfo") viewInfo;

  @ViewChild("success") success;

  @ViewChild("confirmation") confirmation;

  @ViewChild("showImages") showImages;

  public deleteType;

  public deletedCommentId;

  public deleteEventId;

  public deleteObjId;

  public selectedImageUrl;

  public selectedImageId;

  @ViewChild("fail") fail;

  @ViewChild("editEventModal") editEventModal;

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  public eventModelList = new Array<EventModel>(); // Create an event model list
  public eventModelObj = new EventModel();
  public imageList = new Array<String>();

  public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });

  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public categoryModelObj = new CategoryModel();
  public categoryModelList = new Array<CategoryModel>();

  public statusList = new Array<MDCodeModel>();
  public levelsList = new Array<MDCodeModel>();
  public groupList = new Array<GroupModel>();

  public eventEdit = new EventModel();

  public mdCode = new MDCodeModel();

  public isSuccess: boolean = false;
  public showLoader: boolean = true;



  public successMessage = "";

  constructor(
    public commonService: CommonWebService,
    public eventModel: EventModel,
    public categoryModel: CategoryModel,
    public groupModel: GroupModel,
    public validator: LoignValidator

  ) {

    this.validator.validateUser("event-management");



  }


  changeUserEventStatus($event, userId, action) {
    console.log(">>>>>>> ", $event);

    this.statusUserId = userId;

    this.statusAction = action;

  }


  viewInfomation(m) {

    this.position = new google.maps.LatLng(m.eventLocationLat, m.eventLocationLont);

    this.eventEdit = new EventModel();
    this.eventEdit = m;
    this.viewInfo.show();

    this.loadEventComments();

    this.loadEventImages();

    this.loadEventVideo();

    this.loadEventParticipants();
  }

  editEventOpen(event) {

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);


      this.eventModel.eventIconPath = responsePath.responseData;
    }

    this.mode = 'edit';

    this.editEventModal.show();

    this.eventEdit = new EventModel();

    this.eventEdit = event;
    console.log("event id ", this.eventEdit);

    this.position = new google.maps.LatLng(this.eventEdit.eventLocationLat, this.eventEdit.eventLocationLont);

  }


  loadEventParticipants() {

    console.log(">>> getEventParticipants");

    this.participantsLoader = true;

    this.eventParticipantsList = new Array<EventParticipantsModel>();

    let url = AppParams.BASE_PATH + "events/getEventParticipants?eventId=" + this.eventEdit.eventId;

    this.commonService.processGet(url).subscribe(response => {

      let res = response.getEventParticipants;

      if (res.responseCode == 1) {

        this.eventParticipantsList = res.responseData;

      }

      this.participantsLoader = false;

    }, error => {
      this.participantsLoader = false;
    })

  }

  deleteVideo(eventId, videoId) {
    this.deleteType = "video"

    this.deleteObjId = videoId;
    this.deleteEventId = this.eventEdit.eventId;

    this.confirmation.show();
  }

  loadEventVideo() {

    this.videoList = new Array<EventVideoModel>();
    this.eventVideoLoader = true;
    let url = AppParams.BASE_PATH + "events/getEventVideos?eventId=" + this.eventEdit.eventId;

    this.commonService.processGet(url).subscribe(response => {
      let res = response.getEventVideos;

      if (res.responseCode == 1) {
        this.videoList = res.responseData;

      } else {

      }
      this.eventVideoLoader = false;
    }, error => {

      this.eventVideoLoader = false;
    })

  }

  loadEventComments() {

    this.commentList = [];

    let url = AppParams.BASE_PATH + "events/getEventComments?eventId=" + this.eventEdit.eventId;
    this.commentsLoader = true;
    this.commonService.processGet(url).subscribe(response => {
      console.log("esponse ", response);

      let res = response.getEventComments;

      if (res.responseCode == 1) {

        this.commentList = res.responseData;

        console.log("coomments ", this.commentList)

      } else {

      }

      this.commentsLoader = false;
    }, error => {

      this.commentsLoader = false;

    })
  }

  deleteSelectedImage() {

    this.deleteType = "image"

    this.deleteObjId = this.selectedImageId;
    this.deleteEventId = this.eventEdit.eventId;

    this.confirmation.show();
  }

  openImage(eventId, imgId, path) {

    this.selectedImageId = imgId;

    this.selectedImageUrl = path;
    this.showImages.show();

  }

  loadEventImages() {
    let url = AppParams.BASE_PATH + "events/getEventImagesCMS?eventId=" + this.eventEdit.eventId;

    this.eventImageLoader = true;

    this.commonService.processGet(url).subscribe(response => {

      this.imgArray = [];
      let res = response.getEventImagesCMS;

      if (res.responseCode == 1) {

        this.imgArray = res.responseData;

        console.log("console ", this.imgArray);

      } else {

      }

      this.eventImageLoader = false;
    }, error => {

      this.eventImageLoader = false;
    })

  }

  deleteComment(eventId, commentId) {
    this.deleteType = "comment"

    this.deleteObjId = commentId;
    this.deleteEventId = eventId;

    this.confirmation.show();
  }

  confirmDelete() {

    let me = this;
    this.confirmation.hide();
    this.deleteEventData = new DeleteEventDataModel();

    this.deleteEventData.eventId = this.deleteEventId;

    this.deleteEventData.deletedObjId = this.deleteObjId;

    this.commentsLoader = true;

    if (this.deleteType == "comment") {

      this.deletedCommentId = this.deleteObjId;

      this.deleteEventData.operaton = this.deleteType;

      let url = AppParams.BASE_PATH + "events/deleteEventData";

      this.commonService.processPost(url, this.deleteEventData).subscribe(response => {

        let res = response.deleteEventData;
        this.commentsLoader = false;
        if (res.responseCode == 1) {

          this.deletedCommentId = this.deleteObjId;

          this.loadEventComments();

        } else {
          this.failMessage = "The comment cannot delete at this time. Try again!"
          this.fail.show();
        }

        this.deleteEventData.deletedObjId = this.deleteObjId;
      }, error => {
        this.commentsLoader = false;
        this.failMessage = "Cannot connect to the server. Try again!"
        this.fail.show();
      });

    } else if (this.deleteType == "image") {

      this.deleteEventData.operaton = this.deleteType;

      let url = AppParams.BASE_PATH + "events/deleteEventData";

      this.eventImageLoader = true;

      this.commonService.processPost(url, this.deleteEventData).subscribe(response => {

        let res = response.deleteEventData;
        this.eventImageLoader = false;
        if (res.responseCode == 1) {

          this.showImages.hide();
          this.loadEventImages();

        } else {
          this.failMessage = "The image cannot delete at this time. Try again!"
          this.fail.show();
        }

        this.deleteEventData.deletedObjId = this.deleteObjId;
      }, error => {
        this.eventImageLoader = false;
        this.failMessage = "Cannot connect to the server. Try again!"
        this.fail.show();
      });



    } else if (this.deleteType == "video") {

      this.deleteEventData.operaton = this.deleteType;
      this.eventVideoLoader = true;

      let url = AppParams.BASE_PATH + "events/deleteEventData";

      this.commonService.processPost(url, this.deleteEventData).subscribe(response => {

        let res = response.deleteEventData;
        this.eventVideoLoader = false;
        if (res.responseCode == 1) {

          this.loadEventVideo();

        } else {
          this.failMessage = "The image cannot delete at this time. Try again!"
          this.fail.show();
        }

        this.deleteEventData.deletedObjId = this.deleteObjId;
      }, error => {
        this.eventVideoLoader = false;
        this.failMessage = "Cannot connect to the server. Try again!"
        this.fail.show();
      });


    }

  }

  saveChangesEvent() {
    console.log("save chnages ", this.eventEdit);
    this.eventEdit.eventIconPath = this.eventModel.eventIconPath;
    this.editOpertionProcess = true;
    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());

    option.headers = header

    let url = AppParams.BASE_PATH + "events/updateEventDetails";
    this.commonService.processPostWithHeaders(url, this.eventEdit, option).subscribe(response => {

      console.log("response ", response)

      let res = response.updateEventDetails;

      if (res.responseCode == 1) {
        this.editEventModal.hide();
        this.successMessage = "Event has been updated successfully!"
        this.success.show();

        this.loadEventsToGrid(0, 5000);

      } else {

        this.failMessage = "The event cannot be updated at this time. Please try again later!"
        this.fail.show();

      }

      this.editOpertionProcess = false;
    }, error => {
      this.failMessage = "Cannot connect to the server."
      this.fail.show();
      this.editOpertionProcess = false;
    });

  }

  ngOnInit() {
    this.loadEventsToGrid(0, 100)
    this.loadCategories();
    this.loadGroups();
    this.loadStatus("EVENTS");
    this.loadGroupPublicLevels("GROUP_LEVELS");



    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);


      this.eventModel.eventIconPath = responsePath.responseData;
    }


  }

  submitForm(form: any): void {
    console.log('Form Data: ');
    console.log(form);
    console.log();
  }

  loadEventsToGrid(start, limit) {
    //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getEventList?start=" + start + "&limit=" + limit;

    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "events/getEventsList?start=" + start + "&limit=" + limit;
    } else {
      url = AppParams.BASE_PATH + "events/getEventsList?userId=" + this.validator.getUserId() + "&start=" + start + "&limit=" + limit;
    }



    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.eventModelList = res.getEventsList.responseData;
      this.showLoader = false;

      console.log("event model list", this.eventModelList);

    }, error => {

      console.log("error ", error);

    })


  }

  addNewEvent() {

    this.position = new google.maps.LatLng(null, null);;

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);


      this.eventModel.eventIconPath = responsePath.responseData;
    }


    this.mode = 'create';
    this.eventModel = new EventModel();

    this.newEvent.show();
  }


  createEvent() {


    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header


    //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/createEvent"
    let url = AppParams.BASE_PATH + "events/createEvent"



    console.log("date parsing...", this.eventModel.eventDate)
    console.log("event time", this.eventModel.eventTime)
    let time = this.eventModel.eventTime
    this.eventModel.groupId.groupCountry = null;
    this.commonService.processPost(url, this.eventModel).subscribe(res => {

      this.isSuccess = true;


      let response = res.createEvent

      if (response.responseCode == 1) {
        this.successMessage = "Event has created!"

        this.loadEventsToGrid(0, 1000)
        this.showLoader = false;
        this.resetFields(this.eventModel)
        this.newEvent.hide();

        this.successMessage = "The Event has been created";

        this.success.show();

      } else {
        this.failMessage = "Cannot connect to the server";
        this.fail.show();
      }

    }, error => {

      this.failMessage = "Cannot connect to the server";
      this.fail.show();

    })
  }

  resetFields(mod: EventModel) {
    this.showLoader = true;
    this.isSuccess = false;
    mod.eventCaption = ""
    mod.eventMessage = ""
    mod.eventLocation = ""
    mod.eventLocationLat = ""
    mod.eventLocationLont = ""
    mod.manualCount = ""
  }

  loadCategories() {

    let url = AppParams.BASE_PATH + "category/getCategoriesCMS"

    this.commonService.processGet(url).subscribe(res => {

      console.log("category list ", res);

      this.categoryModelList = res.getCategoriesCMS.responseData;

    }, error => {

      console.log("error ", error);

    })

  }

  loadGroups() {

    //  let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getGroupList"
    let url = AppParams.BASE_PATH + "groups/getGroupList"

    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.groupList = res.getGroupList.responseData;

      console.log("response api ", this.groupList)



    }, error => {

      console.log("error ", error);

    })

  }

  loadStatus(type) {

    // let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getStatus?type="+type

    let url = AppParams.BASE_PATH + "events/getStatus?type=" + type

    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.statusList = res.getStatus.responseData;




    }, error => {

      console.log("error ", error);

    })

  }

  loadGroupPublicLevels(subType) {

    //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getPublicLevels?subType="+subType

    let url = AppParams.BASE_PATH + "events/getPublicLevels?subType=" + subType

    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.levelsList = res.getPublicLevels.responseData;



    }, error => {

      console.log("error ", error);

    })

  }




  // onMapReady(map) {
  //   console.log('map', map);
  //   console.log('markers', map.markers);  // to get all markers as an array 
  // }
  onIdle(event) {
    console.log('map', event.target);
  }
  onMarkerInit(marker) {
    console.log('marker', marker);
  }
  onMapClick(event) {


    this.eventModel.eventLocationLat = event.latLng.lat();
    this.eventModel.eventLocationLont = event.latLng.lng();
    this.position = event.latLng;

    event.target.panTo(event.latLng);


  }

  onMapClickEdit(event) {

    this.eventEdit.eventLocationLat = event.latLng.lat();
    this.eventEdit.eventLocationLont = event.latLng.lng();
    this.position = event.latLng;

    event.target.panTo(event.latLng);
  }

  function() {
    console.log("######### show video #########");
  }


}
