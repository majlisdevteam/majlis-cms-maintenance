import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventmangmntComponent } from './eventmangmnt.component';

describe('EventmangmntComponent', () => {
  let component: EventmangmntComponent;
  let fixture: ComponentFixture<EventmangmntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventmangmntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventmangmntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
