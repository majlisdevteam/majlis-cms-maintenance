import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileuserComponent } from './mobileuser.component';

describe('MobileuserComponent', () => {
  let component: MobileuserComponent;
  let fixture: ComponentFixture<MobileuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
