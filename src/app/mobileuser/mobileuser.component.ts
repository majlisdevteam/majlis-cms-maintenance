import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppParams } from "../app.module";
import { MobileUserModel } from "../model/MobileUser";
import { CommonWebService } from "../service/common-web.service";
import { GroupModel } from "../model/Group";
import { LoignValidator } from "../service/login-validator";
// import { DatepickerModule } from 'ngx-bootstrap/datepicker';
// import { DialogService } from "ng2-bootstrap-modal";


@Component({
  selector: 'app-mobileuser',
  templateUrl: './mobileuser.component.html',
  styleUrls: ['./mobileuser.component.css']
})
export class MobileuserComponent implements OnInit {


  myName: String = "Kushan";
  fakeArray = new Array(12);
  fakeArray1 = new Array(10);

  public signinMask = false;

  adminUser: String = "super";

  public mobileUserModel = new Array<MobileUserModel>();
  public mobileUserModelDetail = new MobileUserModel();


  public mobileUserModel2 = new MobileUserModel();

  public groupModel = new Array<GroupModel>();
  public groupModelDetail = new GroupModel();

  public userIdVal;

  @ViewChild('childModal') public childModal: ModalDirective;
  private searchByUserId: any;
  public totalItems: number = 64;
  public currentPage: number = 1;
  public smallnumPages: number = 0;

  constructor(public commonService: CommonWebService, public validator: LoignValidator) {

    this.validator.validateUser("mobileuser");

  }

  @ViewChild("assignToGroup") assignToGroup;

  ngOnInit() {
    this.myName = this.myName.substring(0, 1);

    this.loadMobileUserToGrid();
  }

  public showChildModal(): void {
    this.childModal.show();

  }

  public hideChildModal(): void {
    this.childModal.hide();
  }

  // btnSearch = function(){
  //   console.log('User entered: ${searchByUserId.value}');
  //   // this.router.navigate(['./templatemngmnt'])
  // }

  performSearch(searchTerm): void {
    console.log(`User entered: ${searchTerm.value}`);

  }

  loadMobileUserToGrid() {
    this.signinMask = true;

    console.log("USER zz");

    // let url = AppParams.BASE_PATH + "users/getAllUsers?start=" + start + "&limit=" + limit;
    // let url = AppParams.BASE_PATH + "users/getAllMobileUsers";
    let url = AppParams.BASE_PATH + "users/getAllMobileUsers?adminUserId=" + this.adminUser;

    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.getAllMobileUsers.responseCode == 1) {
        this.signinMask = true;
        this.mobileUserModel = res.getAllMobileUsers.responseData;
        console.log("response api ", this.mobileUserModel)
      }

    }, error => {

      console.log("error ", error);

    })


  }

  getUserGroupStatus(groupId) {

    let obj = this.mobileUserModel2.majlisGroupList.filter(e => {
      return e.groupId == groupId;
    });


    return obj.length == 0




    // console.log("groupObj "+groupId,groupObj)

  }

  getMobileGroupsById(a) {

    console.log("aaa ", a);

    this.mobileUserModel2 = a;

    console.log("logged user ", this.validator.user.groups[0].groupId)



    this.userIdVal = a.mobileUserId;
    this.signinMask = true;
    console.log("GroupsById>>", a.mobileUserId);

    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "groups/getGroupList";

    } else {

      url = AppParams.BASE_PATH + "groups/getGroupList?userId=" + this.validator.getUserId();

    }


    this.commonService.processGet(url).subscribe(res => {


      res = res.getGroupList;
      if (res.responseCode == 1) {
        this.signinMask = false;
        this.groupModel = res.responseData;
        console.log("groupModel >>> ", this.groupModel);
      }
     

    }, error => {

      console.log("error >>> ", error);
      this.signinMask = false;
    })
    // this.showGroups.show()
    this.assignToGroup.show()
  }

  assignGroup(groupId, $event) {

    console.log("check box", $event.target.checked);

    let url = AppParams.BASE_PATH + "groups/updateGroupsForUser";

    let obj = {
      userId: this.userIdVal,
      groupId: groupId,
      operation: $event.target.checked
    }

    this.commonService.processPost(url, obj).subscribe(res => {

      console.log("response ", res);

      this.loadMobileUserToGrid();




    }, error => {

    })



  }

  showPrompt() {
    console.log("aDD nEW uSER");

  }

  btnUSubmit() {

    // $('')
    console.log("Submit form");
    // console.log(form);
  }

  // submitForm($e) {
  //   // $e.preventDefault();
  //   console.log("Submit formzzz");
  // }
  // btnSesarch(searchByUserId): void {
  //   console.log('User entered: ${searchByUserId.value}');
  // }form.value

  public pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
  }

}



