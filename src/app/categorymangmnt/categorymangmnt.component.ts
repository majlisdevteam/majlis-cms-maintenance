// import { Component, OnInit } from '@angular/core';
// import { Component, ChangeDetectorRef } from '@angular/core';
// import { Component, ViewChild } from '@angular/core';
import { Component, OnInit, ViewChild, Directive } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { CategoryModel } from "../model/Category";
import { CommonWebService } from "../service/common-web.service";
import { CategoryStatusModel } from "../model/CategoryStatus";
import { AppParams } from "../app.module";
import { MDCodeModel } from "../model/MDCode";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FileUploader } from "ng2-file-upload";
import { Router } from "@angular/router";
import { LoignValidator } from "../service/login-validator";

@Component({
  selector: 'app-categorymangmnt',

  templateUrl: './categorymangmnt.component.html',
  styleUrls: ['./categorymangmnt.component.css'],
  providers: [CommonWebService, CategoryModel]
})

export class CategorymangmntComponent {
  failMessage: string;
  successMessage: string;

  fakeArray = new Array(12);

  public categoryModel = new Array<CategoryModel>();
  public categoryModelDeatail = new CategoryModel;

  // public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", });
  public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-CategoryManagement-1.0/service/categories/saveCategoryIcon", queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;


  public operationType;
  public formTitle;

  public showLoader = true;

  serviceProgress = false;


  @ViewChild('childModal') public childModal: ModalDirective;
  // public categoryStatusModel = new Array<CategoryStatusModel>();  
  // public categoryStatusModelDeatail = new CategoryStatusModel;
  @ViewChild("editCategory") editCategory;

  @ViewChild("addCategory") addCategory;

  @ViewChild("success") success;

  @ViewChild("fail") fail;

  // private doctors = [];

  private arrMdStatus = new Array<MDCodeModel>();
  // public arrGroupActiveStatus = new Array<MDCodeModel>();
  private categoryStatus = new Array<CategoryStatusModel>();
  constructor(http: Http, public commonService: CommonWebService, public validator: LoignValidator) {

    this.validator.validateUser("category-management");

  }

  changeCategoryStatus(taeget) {

    console.log("cat status ", taeget.value)

    console.log("arrMdStatus ", this.arrMdStatus);

    this.categoryModelDeatail.categoryStatus = this.arrMdStatus.filter(val => {
      return val.codeMessage === taeget.value;

    })[0];

    console.log("category details ", this.categoryModelDeatail)
  }

  ngOnInit() {

    this.loadGroupsToGrid();

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    }

    this.uploader.onBeforeUploadItem = (e => {
      this.serviceProgress = true;
    })

    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);

      console.log("aaa", responsePath);
      this.categoryModelDeatail.categoryIconPath = responsePath.responseData;
    }

    // Category Status
    let urlActiveStatus = AppParams.BASE_PATH + "categories/getStatus?type=CATEGORY&subType=CATEGORY_STATUS";

    this.commonService.processGet(urlActiveStatus).subscribe(res => {

      let response = res.getStatus;

      if (response.responseCode == 1) {


        this.categoryStatus = response.responseData;

      }


    }, error => {
      console.log("console ", error);
    });
  }

  getStatusOfCategory(statusId) {

    let m = this.categoryStatus.filter(res => {
      return res.codeSeq == statusId;
    })[0];

    return m;
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
  private data: any[];
  selectedEntities: any[];
  public setSelectedEntities($event: any) {
    this.selectedEntities = $event;
  }

  loadGroupsToGrid() {

    this.showLoader = true;


    let k = this;

    let url = AppParams.BASE_PATH + "categories/getAllCategoryById";
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res.allCategories);

      this.arrMdStatus = res.categorystatus.responseData;



      this.categoryModel = res.allCategories.responseData;

      console.log("response api ", this.arrMdStatus);

      this.showLoader = false;


    }, error => {

      this.showLoader = false;

      console.log("error ", error);

    })
  }


  fireFormAction(event) {

    if (this.operationType == 'insert') {

      this.createCategory(event);

    } else if (this.operationType == 'update') {
      this.saveCategoryChanges(event);
    }
  }

  openCategoryForm(type, categoryId) {

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-CategoryManagement-1.0/service/categories/saveCategoryIcon", queueLimit: 1 });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    }

    this.uploader.onBeforeUploadItem = (e => {
      this.serviceProgress = true;
    })

    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);

      this.serviceProgress = false;

      console.log("aaa", responsePath);
      this.categoryModelDeatail.categoryIconPath = responsePath.responseData;
    }

    if (type == "insert") {
      this.operationType = "insert";

      this.categoryModelDeatail = new CategoryModel;

      this.formTitle = "New Category";

      this.categoryModelDeatail.categoryStatus.codeId = 1;


      this.addCategory.show();

    } else if (type == "update") {

      this.operationType = "update";

      this.formTitle = "Edit Category";

      this.getCategoryDetailsById(categoryId)

    }


  }


  createCategory(evt) {

    console.log("Add Category ");
    let url = AppParams.BASE_PATH + "categories/createCategory";


    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    this.commonService.processPostWithHeaders(url, this.categoryModelDeatail, option).subscribe(res => {


      this.loadGroupsToGrid();
      this.addCategory.hide();



    }, error => {

      console.log("error >>> ", error);

    })

  }



  getStatusOfUser(statusId) {

    let m = this.arrMdStatus.filter(res => {
      return res.codeSeq == statusId;
    })[0];
    return m;
  }

  saveCategoryChanges(evt) {


    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    console.log("saveGroupChanges ", this.categoryModelDeatail);
    let url = AppParams.BASE_PATH + "categories/updatecategory";
    this.commonService.processPostWithHeaders(url, this.categoryModelDeatail, option).subscribe(res => {

      let response = res.updateCategory

      if (response.responseCode == 1) {
        this.addCategory.hide();
        
        this.successMessage = "Category has been updated successfully!"
        this.success.show();
      } else {

        this.failMessage = "The Category cannot be updated at this time. Please try again later!"
        this.fail.show();

      }


      this.loadGroupsToGrid()

    }, error => {

      this.failMessage = "The Category cannot be updated at this time. Please try again later!"
      this.fail.show();

    })
  }

  getCategoryDetailsById(categoryId) {

    let url = AppParams.BASE_PATH + "categories/getCategoryById?categoryId=" + categoryId;

    this.commonService.processGet(url).subscribe(res => {

      this.categoryModelDeatail = res.getCategoryById.responseData[0];

      this.addCategory.show();

    }, error => {



    });

  }


}
