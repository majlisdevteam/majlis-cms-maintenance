import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NewUserModel } from "../model/NewUserModel";
import { AppParams } from "../app.module";
import { CommonWebService } from "../service/common-web.service";
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";
import { LdapGroupModel } from "../model/LDapGroup";
import { GroupModel } from "../model/Group";
import { User } from "../model/User";
import { LoignValidator } from "../service/login-validator";

// import { DatepickerModule } from 'ngx-bootstrap/datepicker';
// import { DialogService } from "ng2-bootstrap-modal";


@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.css']
})
export class UsermanagementComponent implements OnInit {
  selectedGroupId: any;
  selectedUserId: any;
  public allGroups: GroupModel[];
  public userGroupList: GroupModel[];
  gridLoader: boolean = false;
  createUserLoader: boolean;
  failMessage: string;
  updateUserLoade: boolean = false;

  public isSuccess: boolean = false;
  public isLoading: boolean = true;

  public successMessage = "";

  @ViewChild("success") success;

  @ViewChild("fail") fail;

  myName: String = "Kushan";
  fakeArray = new Array(12);
  fakeArray1 = new Array(10);

  public userModel = new Array<NewUserModel>();

  public newUserModel = new User();

  public user = new User();

  public arrNewUserModel = new Array<User>();

  public userModelDetail = new NewUserModel();

  public userGrpModel = new Array<LdapGroupModel>();
  public userGrpModelDetail = new Array<LdapGroupModel>();

  @ViewChild('childModal') public childModal: ModalDirective;
  private searchByUserId: any;
  public totalItems: number = 64;
  public currentPage: number = 1;
  public smallnumPages: number = 0;

  constructor(public commonService: CommonWebService, public validator: LoignValidator) {
    this.validator.validateUser("user-management");
  }

  @ViewChild("assignToGroup") assignToGroup;
  @ViewChild("makeGroupAdminSetting") makeGroupAdminSetting;
  @ViewChild("makeUserSetting") makeUserSetting;
  @ViewChild("showGroups") showGroups;

  public groupModel = new Array<GroupModel>();

  public groupModelDetail = new GroupModel();

  ngOnInit() {
    this.myName = this.myName.substring(0, 1);

    this.loadUserToGrid();


  }

  public showChildModal(): void {
    this.childModal.show();

  }

  public hideChildModal(): void {
    this.childModal.hide();
  }

  openCreateUser() {
    this.makeUserSetting.show();
    this.newUserModel = new User();
  }

  changeGroupAdmin($event, id) {

    console.log("Status... ", $event.target.checked);

    this.selectedGroupId = id;

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.selectedUserId);
    header.set("groupId", id);
    header.set("operation", $event.target.checked)


    option.headers = header

    let url = AppParams.BASE_PATH + "users/changeGroupUser";

    let user = new User();

    this.commonService.processPostWithHeaders(url, user, option).subscribe(response => {


      if (response.changeGroupUser.responseCode == 1) {
        this.getUserDetailsById(this.selectedUserId);
      } else {
        this.failMessage = "Cannot complete the operation at this time!";
        this.fail.show();
      }

      this.selectedGroupId = 0;

    }, error => {

      this.failMessage = "Cannot complete the operation at this time!";
      this.fail.show();
      this.selectedGroupId = 0;

    });

  }

  createUser(event) {

    this.createUserLoader = true;

    let url = AppParams.BASE_PATH + "users/addUser";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    option.headers = header

    this.newUserModel.user.groups = [{
      "groupId": "GroupAdminGrp",
      "groupName": "Group Admin"
    }]
    this.commonService.processPostWithHeaders(url, this.newUserModel, option).subscribe(res => {


      let response = res
      console.log("response", response);
      if (response.addUser.flag == 100) {

        this.isSuccess = true;
        this.successMessage = "User created successfully!"

        this.isLoading = false;

        this.resetFields(this.newUserModel);

        this.successMessage = "User has been created successfully. Password will be sent to the email address";
        this.success.show();
      } else if (response.addUser.flag == 808) {
        this.failMessage = response.addUser.exceptionMessages[0];
        this.fail.show();
      } else {
        this.failMessage = "Cannot create the user at this time"
        this.fail.show();
      }

      this.createUserLoader = false;

      this.loadUserToGrid();
    }, error => {
      this.createUserLoader = false;
      this.failMessage = "Error in connection. Try again later!"
      this.fail.show();

    })
  }


  resetFields(mod: User) {
    this.isSuccess = false;
    mod.user.userId = "";
    mod.user.firstName = "";
    mod.user.lastName = "";
    mod.user.telephoneNumber = "";
    mod.user.email = "";
    mod.user.address = "";
    mod.allowedNotificationCnt = ""


  }
  loadUserToGrid() {

    let url = AppParams.BASE_PATH + "users/getCMSUsers?start=0&limit=1000";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "super");
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");


    option.headers = header;
    console.log("hhh ", header);
    this.commonService.processGetWithHeaders(url, option).subscribe(res => {



      // this.userModel = res.users.responseData;
      this.arrNewUserModel = res.users.data;
      this.isLoading = false;

    }, error => {
      this.isLoading = false;
      console.log("error ", error);

    })


  }

  getUserId(userId) {

    this.user = userId;

    if (this.user.allowedNotificationCnt == 0) {

      this.user.allowedNotificationCnt = -1
      console.log("byuserId>>>>>>>>>>>", userId);
    }

    this.updateUserLoade = false;

    this.makeGroupAdminSetting.show()

  }

  getUserDetailsById(userId) {

    this.selectedUserId = userId;

    this.gridLoader = true;
    let url = AppParams.BASE_PATH + "users/getCmsUserGroup?userId=" + userId;

    this.commonService.processGet(url).subscribe(res => {


      console.log(">>>> ", res.getCmsUserGroup)

      this.userGroupList = new Array<GroupModel>();

      this.allGroups = new Array<GroupModel>();

      if (res.getCmsUserGroup.responseCode == 1) {

        this.userGroupList = res.getCmsUserGroup.responseData.userGroups;

        this.allGroups = res.getCmsUserGroup.responseData.allGroups;

      } else {
        this.failMessage = "Cannot fetch the data at this time";
        this.fail.show();
      }

      this.gridLoader = false;

    }, error => {
      this.failMessage = "Cannot fetch the data at this time";
      this.fail.show();
      this.gridLoader = false;
      console.log("error >>> ", error);

    })
    this.assignToGroup.show()

  }

  getUserGroupStatus(id) {

    let idx = this.userGroupList.findIndex(e => e.groupId == id);

    if (idx == -1) {
      return false;
    } else {
      return true;
    }

  }

  getGroupsById(userId) {
    console.log("GroupsById>>", userId);

    let url = AppParams.BASE_PATH + "groups/getAllGroupsById?user=" + userId;

    this.commonService.processGet(url).subscribe(res => {

      this.groupModel = res.getAllGroupsById.responseData;
      console.log("groupModel >>> ", this.groupModel);


    }, error => {

      console.log("error >>> ", error);

    })
    this.showGroups.show()
  }

  saveUserChanges(evt) {

    this.updateUserLoade = true;

    console.log("saveUserChanges ", this.userModelDetail);

    let url = AppParams.BASE_PATH + "users/modifyCmsUser";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    option.headers = header;


    option.headers = header;

    this.commonService.processPostWithHeaders(url, this.user, option).subscribe(res => {

      this.isSuccess = true;

      let response = res.updateGroup;

      try {

        if (response.responseCode == 1) {
          this.makeGroupAdminSetting.show();
          this.successMessage = "Changes saved successfully!"
          this.success.show();
        } else {
          this.failMessage = "Operation failed! Cannot change the user details at this time."
          this.fail.show();
        }
        this.updateUserLoade = false;
      } catch (res) {
        this.failMessage = "Operation failed!"
        this.fail.show();
        this.updateUserLoade = false;
      }


    }, error => {
      this.failMessage = "Operation failed!"
        this.fail.show();

    })
  }



  public pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
  }

}



