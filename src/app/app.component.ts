import { Component } from '@angular/core';
import { LoggedInUserModel } from "./model/loggedInUserModel";
import { CommonWebService } from "./service/common-web.service";
import { AppParams } from "./app.module";
import { RequestOptionsArgs, Headers, RequestOptions } from "@angular/http";




@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  template:'<router-outlet></router-outlet>',
  styleUrls: ['./app.component.css', './styles/loaders.css'],
  providers: [LoggedInUserModel, CommonWebService]
})
export class AppComponent {

  private username: string;
  private password: any;

  private btnSignin = true;
  private signinMask = false;

  private loginFailMsg: string = "";



  constructor(private userData: LoggedInUserModel, private webservice: CommonWebService) {

  }

  // login($event) {
  //   $event.preventDefault()
  //   console.log("login.....");

  //   this.loginFailMsg = '';
  //   this.btnSignin = false;
  //   this.signinMask = true;
  //   this.userData.userId = this.username;
  //   this.userData.password = this.password;

  //   let header: Headers = new Headers();

  //   let option: RequestOptionsArgs = new RequestOptions();

  //   header.set("system", "MajlisCMS");

  //   option.headers = header;

  //   let url = AppParams.BASE_PATH + "user/validateCmsUser";

  //   this.webservice.processPostWithHeaders(url, this.userData, option).subscribe(response => {

  //     console.log("response", response.validateCmsUser);

  //     let responseData = response.validateCmsUser;

  //     if(responseData != null){

  //     let flag = responseData['flag'];

  //     if (flag == 1000) {
  //       console.log("success");
  //     } else {
  //       this.loginFailMsg = responseData['exceptionMessages'][0];
  //     }
  //   }else{
  //     this.loginFailMsg = "Cannot connect with the authentication server";
  //   }
  //     this.signinMask = false;
  //     this.btnSignin = true;

  //   }, error => {

  //     this.signinMask = false;
  //     this.btnSignin = true;

  //   })
  // }


}
