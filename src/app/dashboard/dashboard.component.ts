import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleChartComponent } from '../google-chart/google-chart.component';
import { HeatmapLayer } from '@ngui/map';
import { AppParams } from "../app.module";
import { CommonWebService } from "../service/common-web.service";
import { DashBoardItemModel } from "../model/DashboardItems";
import { ChartGroupModel } from "../model/ChartGroups";
import { ChartCategoryModel } from "../model/ChartCategory";
import { SharedService } from "../shared-service";
import { ActivatedRoute, Router } from "@angular/router";
import { LoignValidator } from "../service/login-validator";
import { EventModel } from "../model/Event";





@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [CommonWebService, DashBoardItemModel, SharedService]

})
export class DashboardComponent implements OnInit {
  [name: string]: any;


  public arrEventModelForMap = new Array<EventModel>();

  @ViewChild(HeatmapLayer) heatmapLayer: HeatmapLayer;
  heatmap: google.maps.visualization.HeatmapLayer;
  map: google.maps.Map;
  points = [];

  public dashboardItem = new DashBoardItemModel();
  public chartGroupModel = new Array<ChartGroupModel>();
  public chartCategoryModel = new Array<ChartCategoryModel>();

@ViewChild("showGroupDetails") showGroupDetails;

  @ViewChild('chartz') Chartzz;

  constructor(public commonService: CommonWebService,
    public sharedService: SharedService, public route: ActivatedRoute,
    public validator: LoignValidator,public router: Router) {

    this.validator.validateUser("dashboard");



  }

  ngOnInit() {

    this.loadMapData();

    this.heatmapLayer['initialized$'].subscribe(heatmap => {
      this.heatmap = heatmap;
      this.map = this.heatmap.getMap();
      this.heatmap.set('radius', this.heatmap.get('radius') ? null : 20);
    });
    this.loadGroupsToList();
    this.loadBarChartGroupData();
    this.loadPieChartCategoryData();


  }

  openGroupDetails(){

    console.log("open ldap");
    this.router.navigate(['./group-management'],{queryParams :{dashview:true}})

  }

  openEventPage(){
    this.router.navigate(['./event-management'])
  }

  loadMapData() {

    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "events/getEventLocationsForMap";
    } else {
      url = AppParams.BASE_PATH + "events/getEventLocationsForMap?userId=" + this.validator.getUserId();
    }

    this.commonService.processGet(url).subscribe(response => {

      console.log(">>> ", response);

      let res = response.getEventLocationsForMap;

      if (res.responseCode == 1) {

        this.arrEventModelForMap = res.responseData;

        console.log("response data ", this.arrEventModelForMap)

        this.arrEventModelForMap.forEach(e => {

          this.points.push(new google.maps.LatLng(e.eventLocationLat, e.eventLocationLont))

        })

      }

    }, error => {
      console.log("err", error)
    });

  }

  toggleHeatmap() {
    this.heatmap.setMap(this.heatmap.getMap() ? null : this.map);
  }



  changeGradient() {
    let gradient = [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 255, 1)',
      'rgba(0, 191, 255, 1)',
      'rgba(0, 127, 255, 1)',
      'rgba(0, 63, 255, 1)',
      'rgba(0, 0, 255, 1)',
      'rgba(0, 0, 223, 1)',
      'rgba(0, 0, 191, 1)',
      'rgba(0, 0, 159, 1)',
      'rgba(0, 0, 127, 1)',
      'rgba(63, 0, 91, 1)',
      'rgba(127, 0, 63, 1)',
      'rgba(191, 0, 31, 1)',
      'rgba(255, 0, 0, 1)'
    ];
    this.heatmap.set('gradient', this.heatmap.get('gradient') ? null : gradient);
  }


  changeRadius() {
    this.heatmap.set('radius', this.heatmap.get('radius') ? null : 20);
  }

  changeOpacity() {
    this.heatmap.set('opacity', this.heatmap.get('opacity') ? null : 0.2);
  }

  loadRandomPoints() {
    this.points = [];

    for (let i = 0; i < 9; i++) {
      this.addPoint();
    }
  }

  addPoint() {
    let randomLat = Math.random() * 0.0099 + 37.782551;
    let randomLng = Math.random() * 0.0099 + -122.445368;
    let latlng = new google.maps.LatLng(randomLat, randomLng);
    this.points.push(latlng);
  }


  public pie_ChartData = [];
  public pie_ChartOptions = {
    // title: 'User involvement for category',
    width: '70%',
    height: 300,
    pieHole: 0.6,
    colors: ['#009688', '#6eba8c', '#b9f2a1', '#10c4b5', '#005562'],
    legend: { position: 'none' },
    chartArea: { left: 10, top: 10, right: 10, bottom: 10 },
    animation: {
      duration: 1000,
      easing: 'out',
      startup: true
    }

  };
  public bar_ChartData = [];

  public bar_ChartOptions = {
    title: 'Top Groups',
    width: '70%',
    height: 300,
    colors: ['#009688', '#6eba8c', '#b9f2a1', '#10c4b5', '#005562', '#0e8174', '#639a4b', '#2d6b64'],
    legend: { position: 'none' },
    chartArea: { left: 10, top: 10, right: 10, bottom: 10 },
    animation: {
      duration: 2000,
      easing: 'out',
      startup: true
    }
  };



  loadGroupsToList() {
    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "groups/getCount";
    } else {
      url = AppParams.BASE_PATH + "groups/getCount?userId=" + this.validator.getUserId();
    }


    // let url = AppParams.BASE_PATH + "groups/getCount";
    // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getCount";
    this.commonService.processGet(url).subscribe(res => {

      let response = res.getCount;

      if (res.responseCode != 999) {
        this.dashboardItem = response.responseData;

        if (this.dashboardItem.tot_notification_limit == -1) {
          this.dashboardItem.tot_notification_limit = "Unlimited"
        }
      }



    }, error => {

      console.log("error ", error);

    })


  }


  loadBarChartGroupData() {

    let url = AppParams.BASE_PATH + "groups/getTopGroups";
    //let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getTopGroups";
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      let response = res.getTopGroups;

      if (res.responseCode != 999) {
        this.chartGroupModel = response.responseData;
      }

      console.log("response api ", this.chartGroupModel);

      this.bar_ChartData = [
      //   ['Task', 'User count'],
      // [this.chartGroupModel[0].groupName, this.chartGroupModel[0].count],
      // [this.chartGroupModel[1].groupName, this.chartGroupModel[1].count],
      // [this.chartGroupModel[2].groupName, this.chartGroupModel[2].count],
      // [this.chartGroupModel[3].groupName, this.chartGroupModel[3].count],
      // [this.chartGroupModel[4].groupName, this.chartGroupModel[4].count],
      ];


    }, error => {

      console.log("error ", error);

    })
  }

  loadPieChartCategoryData() {

    let url = AppParams.BASE_PATH + "categories/getTopCategories";
    //let url = "http://192.0.0.59:8080/Majlis-CategoryManagement-1.0/service/categories/getTopCategories";
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      let response = res.getTopCategories;

      if (res.responseCode != 999) {
        this.chartCategoryModel = response.responseData;
      }

      console.log("response api ", this.chartCategoryModel);



      // this.pie_ChartData = [['Task', 'User count'],
      // [this.chartCategoryModel[0].categoryName, this.chartCategoryModel[0].count],
      // [this.chartCategoryModel[1].categoryName, this.chartCategoryModel[1].count],
      // [this.chartCategoryModel[2].categoryName, this.chartCategoryModel[2].count],
      // [this.chartCategoryModel[3].categoryName, this.chartCategoryModel[3].count],
      // [this.chartCategoryModel[4].categoryName, this.chartCategoryModel[4].count],
      // ];

      this.chartCategoryModel.forEach(model => {
        console.log("model");
        let chartCategoryModelTemp = new ChartCategoryModel();
        if (model.id == 1) {
          model.color = '#009688';
        }
        else if (model.id == 2) {
          model.color = '#6eba8c';
        }
        else if (model.id == 3) {
          model.color = '#b9f2a1';
        }
        else if (model.id == 4) {
          model.color = '#10c4b5';
        }
        else {
          model.color = '#005562';
        }
      });



    }, error => {

      console.log("error ", error);

    })
  }



}
