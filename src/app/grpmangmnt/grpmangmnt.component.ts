import { Component, OnInit, ViewChild, Directive } from '@angular/core';
import { CommonWebService } from "../service/common-web.service";
import { GroupModel } from "../model/Group";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppParams } from "../app.module";
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { MDCodeModel } from "../model/MDCode";
import { LoignValidator } from "../service/login-validator";
import { MobileUserModel } from "../model/MobileUser";
import { CountryModel } from "../model/country";
import { EventUserGroupModel } from "../model/EventUserGroup";
import { Router, Route, ActivatedRoute } from "@angular/router";
import { ProvinceModel } from "../model/province";
import { ProvinceDetailsModel } from "../model/province-details";
import { CityModel } from "../model/city";

const HEROES = [
  { id: 1, name: 'Sanga' },
  { id: 2, name: 'Mahela' },
  { id: 3, name: 'Dilshan' },
];

@Component({
  selector: 'app-grpmangmnt',
  templateUrl: './grpmangmnt.component.html',
  // template:'<table><tboby></tboby></table>',  
  styleUrls: ['./grpmangmnt.component.css'],
  providers: [CommonWebService, GroupModel, CountryModel]

})
@Directive({ selector: '[ng2FileSelect]' })
export class GrpmangmntComponent implements OnInit {
  cities: CityModel[];
  dashview: boolean;

  fakeArray = new Array(12);

  public isSuccess: boolean = false;

  public successMessage = "";

  public addUserLoader;

  public showLoader = true;

  public userAcceptLoader;

  public failMessage;

  public viewMode = false;

  public completedAddGroupUser;

  public groupId;

  public serviceProgress = false;

  public formloaded = false;

  public modalTitle = "";

  public operationType;

  public selectedUser;

  @ViewChild("groupPublicLevel") groupPublicLevel;

  @ViewChild("editGroupForm") editGroupForm;

  @ViewChild("acceptUsers") acceptUsers;

  @ViewChild("fail") fail;

  @ViewChild("success") success;

  @ViewChild("noUsers") noUsers;

  public isFromDash;


  public imagePath: any;
  public base64Image: string;

  public arrMobile = new Array<MobileUserModel>();

  public arrGroupPublic = new Array<MDCodeModel>();
  public arrGroupActiveStatus = new Array<MDCodeModel>();

  private arrMdStatus = new Array<MDCodeModel>();



  public arrCountries = [];

  public arrProvince = new Array<ProvinceModel>();

  public provinces = new Array<ProvinceDetailsModel>()

  // public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER+"Majlis-GroupManagement-1.0/service/groups/saveGroupIcon"});
  public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  /////////////////////////////////////////////

  public groupModelObj = new GroupModel();

  //////////////////////////////////////////


  public groupModel = new Array<GroupModel>();

  public groupModelDetail = new GroupModel();

  @ViewChild("editGroup") editGroup;


  constructor(public commonService: CommonWebService, public validator: LoignValidator,private route : ActivatedRoute) {


    this.validator.validateUser("group-management");

  }

  getDistrictList($event){


    let country = this.groupModelDetail.groupCountry;
    console.log("country ",country)
    let provinces = this.arrProvince.filter(e=>{
      return e.countryCode == country;
    })
    
    console.log("avil province ",provinces.length)

    if(provinces.length >0){
      console.log("inside province")
      this.provinces = provinces[0].provinces;
    }else{
      this.provinces = [];
    }
   

    console.log("province list ",this.provinces)
  }

  getCityList(){
    console.log("district ",this.groupModelDetail.groupDistrict);

    this.cities = this.provinces.filter(e=>{
      return e.provinceName == this.groupModelDetail.groupDistrict
    })[0].cities;


  }

  viewGroupDetail(groupId){

    console.log("group details ",groupId);

    this.viewMode = true;


    this.editGroup.show();

    this.getGroupDetailsById2(groupId);

  }

  addUserToGroup(userId) {

    console.log("user is ", userId)

    this.addUserLoader = true;

    let url = AppParams.BASE_PATH + "groups/approveUser";

    let user = new EventUserGroupModel()

    user.groupId = this.groupId;

    user.userId = userId;

    user.operation = null;

    this.selectedUser = userId;

    console.log("user ", user);

    this.commonService.processPost(url, user).subscribe(response => {

      this.completedAddGroupUser = true;
      this.addUserLoader = false;

      this.successMessage = "User has been added to the group";
      this.acceptUsers.hide();
      this.success.show();

      this.getGroupAcceptData(this.groupId);

    }, error => {
      this.failMessage = "Operation cannot complete at this time. Please try agian later"

      this.fail.show();

    });


  }

  changePublicLevel(taeget) {
    this.groupModelDetail.groupPublicLevel = this.arrGroupPublic.filter(val => {
      return val.codeMessage === taeget.value;

    })[0];
  }


  changeGroupStatus(taeget) {
    this.groupModelDetail.groupStatus = this.arrGroupActiveStatus.filter(val => {
      return val.codeMessage === taeget.value;

    })[0];
  }

  ngOnInit() {

    this.route.queryParams.subscribe(e=>{
     
      this.dashview = e['dashview'] || false;

       console.log("eee ",this.dashview)
    })

    this.commonService.processGet("/assets/countries.json").subscribe(res => {

      this.arrCountries = res;

      console.log("country list ", this.arrCountries);

    }, error => {


    });


      this.commonService.processGet("/assets/provinces.json").subscribe(res => {

      this.arrProvince = res;


    }, error => {


    })

    this.loadGroupsToGrid(0, 1025);

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
 
      var responsePath = JSON.parse(v);

      console.log("aaa", responsePath);
      this.groupModelDetail.groupIconPath = responsePath.responseData;
    }


    let urlPubic = AppParams.BASE_PATH + "groups/getStatus?type=GROUPS&subType=GROUP_LEVELS";

    this.commonService.processGet(urlPubic).subscribe(res => {

      console.log("res", res);

      let response = res.getStatus;

      if (response.responseCode == 1) {


        this.arrGroupPublic = response.responseData;

        console.log("public group ", this.arrGroupPublic)

      }


    }, error => {
      console.log("console ", error);
    });


    let urlActiveStatus = AppParams.BASE_PATH + "groups/getStatus?type=GROUPS&subType=ACTIVE_STATUS";

    this.commonService.processGet(urlActiveStatus).subscribe(res => {

      let response = res.getStatus;

      if (response.responseCode == 1) {


        this.arrGroupActiveStatus = response.responseData;

      }


    }, error => {
      console.log("console ", error);
    });


  }

  getStatusOfUser(statusId) {

    let m = this.arrGroupActiveStatus.filter(res => {
      return res.codeSeq == statusId;
    })[0];

    return m;
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  performSearch(searchTerm): void {
    console.log(`User entered: ${searchTerm.value}`);

  }

  openNewGroup() {

    
    this.viewMode = false;

    this.serviceProgress = false;
    this.isSuccess = false;
    this.modalTitle = "Create Group";
    this.operationType = "insert";
    this.groupModelDetail = new GroupModel();
    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }

    this.uploader.onBeforeUploadItem = (e) => {
      this.serviceProgress = true;
    }

    this.uploader.onCompleteItem = (m, v) => {

      console.log("completed! 1")


      this.serviceProgress = false;

      var responsePath = JSON.parse(v);



      console.log("aaa", responsePath);
      this.groupModelDetail.groupIconPath = responsePath.responseData;
    }


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      var responsePath = JSON.parse(v);

      this.serviceProgress = false;

      console.log("aaa", responsePath);
      this.groupModelDetail.groupIconPath = responsePath.responseData;
    }

    this.editGroup.show()

  }

  getCountry(country) {

    let ct = this.arrCountries.filter(e => {
      return e.alpha2Code == country;
    })[0];

    return ct.name;


  }

  getGroupAcceptData(groupId) {

    this.groupId = groupId;

    this.userAcceptLoader = true;

    let url = AppParams.BASE_PATH + "groups/getAllAcceptUsers?groupId=" + groupId

    this.commonService.processGet(url).subscribe(response => {

      console.log("getAccept ", response);

      let res = response.getAllAcceptUsers;

      if (res.responseCode == 1) {

        this.arrMobile = res.responseData;

        if (this.arrMobile.length == 0) {

          this.noUsers.show();

        } else {
          this.acceptUsers.show();
        }

      } else {

        this.fail.show();

      }

      this.userAcceptLoader = false;

    }, error => {
      this.userAcceptLoader = false;
      console.log("error ", error);
    })



  }

  loadGroupsToGrid(start, limit) {

    let url;

    if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
      url = AppParams.BASE_PATH + "groups/getGroupList?start=" + start + "&limit=" + limit
    } else {
      url = AppParams.BASE_PATH + "groups/getGroupList?start=" + start + "&limit=" + limit + "&userId=" + this.validator.getUserId();
    }



    // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/group/getGroupList?start=" + start + "&limit=" + limit;
    this.commonService.processGet(url).subscribe(res => {

      console.log("response ", res);

      this.groupModel = res.getGroupList.responseData;

      console.log("response api ", this.groupModel)

      this.showLoader = false;


    }, error => {

      console.log("error ", error);

    })


  }

  saveGroupChanges(evt) {

    this.serviceProgress = true;


    //  let reader=new FileReader();

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    console.log("saveGroupChanges ", this.groupModelDetail);
    let url = AppParams.BASE_PATH + "groups/updateGroup";
    // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/group/updateGroup";
    this.commonService.processPostWithHeaders(url, this.groupModelDetail, option).subscribe(res => {

      this.isSuccess = true;
      this.serviceProgress = false;

      let response = res.updateGroup

      if (response.responseCode == 1) {
        this.successMessage = "Group Details have been updated!"
      } else {
        this.successMessage = "Error in Group Details updated!"
      }

      this.loadGroupsToGrid(0, 5000);

    }, error => {

      console.log("error >>> ", error);

    })
  }


  getGroupDetailsById2(groupid) {

    this.serviceProgress = true;

    
    this.viewMode = true;

    this.formloaded = true;

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }

    this.uploader.onBeforeUploadItem = (e) => {
      this.serviceProgress = true;

    }

    this.uploader.onCompleteItem = (m, v) => {

      var responsePath = JSON.parse(v);

      this.serviceProgress = false;
;
      this.groupModelDetail.groupIconPath = responsePath.responseData;
    }


    this.modalTitle = "Edit Group"
    this.operationType = "update";

    this.isSuccess = false;

    this.groupModelDetail = new GroupModel();

    console.log(">>>>>>>>>>>", groupid);

    this.serviceProgress = true;

    let url = AppParams.BASE_PATH + "groups/getGroupDetail?groupid=" + groupid;
    // let url = "http://192.0.0.48:9090/Majlis-GroupManagement-1.0/service/group/getGroupDetail?groupid=" + groupid;

    this.commonService.processGet(url).subscribe(res => {

      this.groupModelDetail = res.getGroupDetail.responseData[0];

      console.log("res >>> ", this.groupModelDetail);

      this.formloaded = false;

      this.serviceProgress = false;

    }, error => {

      this.formloaded = false;

      console.log("error >>> ", error);

      this.serviceProgress = false;

    })
    this.editGroup.show()

  }

  getGroupDetailsById(groupid) {

    this.serviceProgress = true;

    this.groupModelDetail = new GroupModel();

    
    this.viewMode = false;

    this.formloaded = true;

    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }

    this.uploader.onBeforeUploadItem = (e) => {
      this.serviceProgress = true;

    }

    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);

      this.serviceProgress = false;

      console.log("aaa", responsePath);
      this.groupModelDetail.groupIconPath = responsePath.responseData;
    }


    this.modalTitle = "Edit Group"
    this.operationType = "update";

    this.isSuccess = false;

    this.groupModelDetail = new GroupModel();

    console.log(">>>>>>>>>>>", groupid);

    this.serviceProgress = true;

    let url = AppParams.BASE_PATH + "groups/getGroupDetail?groupid=" + groupid;
    // let url = "http://192.0.0.48:9090/Majlis-GroupManagement-1.0/service/group/getGroupDetail?groupid=" + groupid;

    this.commonService.processGet(url).subscribe(res => {


      this.groupModelDetail = res.getGroupDetail.responseData[0];

      console.log("res >>> ", this.groupModelDetail);

      this.getDistrictList(this);


      this.groupModelDetail.groupDistrict = this.groupModelDetail.groupDistrict
      this.getCityList();

      this.formloaded = false;

      this.serviceProgress = false;

    }, error => {

      this.formloaded = false;

      console.log("error >>> ", error);

      this.serviceProgress = false;

    })
    this.editGroup.show()

  }


  fireFormAction(event) {
    if (this.operationType == 'insert') {

      this.createGroup(event);

    } else if (this.operationType == 'update') {
      this.saveGroupChanges(event);
    }
  }

  createGroup(evt) {

    let me = this;

    this.serviceProgress = true;

    this.groupModelObj = new GroupModel();

    if (this.groupModelDetail.groupPublicLevel.codeId == null) {
      this.groupModelDetail.groupPublicLevel = this.arrGroupPublic.filter(val => {
        return val.codeSeq === 1;

      })[0];
    }

    if (this.groupModelDetail.groupStatus.codeId == null) {
      this.groupModelDetail.groupStatus = this.arrGroupActiveStatus.filter(val => {
        return val.codeSeq === 1;

      })[0];
    }

    console.log("Create Group", this.groupModelDetail);
    console.log("saveGroupChanges ", this.groupModelDetail);
    let url = AppParams.BASE_PATH + "groups/createGroup";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header

    this.groupModelObj.groupStatus.codeSeq;

    this.groupModelDetail.groupCountry = this.groupModelDetail.groupCountry

    this.commonService.processPostWithHeaders(url, this.groupModelDetail, option).subscribe(res => {

      this.isSuccess = true;

      if (res.createGroup.responseCode == 1) {

        this.successMessage = "Group created successfully!"
        this.groupModelObj = res.createGroup.responseData;

        this.loadGroupsToGrid(0, 5000);

      } else {

        this.successMessage = "Error in Group created unsuccessfully!"
      }


      this.serviceProgress = false;

    }, error => {

      console.log("error >>> ", error);

    })
  }





}


