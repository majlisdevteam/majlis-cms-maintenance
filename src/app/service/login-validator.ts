
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { LoggedInUserModel } from "../model/loggedInUserModel";

@Injectable()
export class LoignValidator {

    public user: LoggedInUserModel;

    constructor(public router: Router) {

    }

    public getUserId() {

        let userToken = sessionStorage.getItem('id_token');

        this.user = JSON.parse(atob(userToken));
        
        return this.user.userId;
    }

    public validateUser(menuName) {

        let userToken = sessionStorage.getItem('id_token');

        if (userToken == null) {

            this.router.navigate(['./login']);

        } else {

            this.user = JSON.parse(atob(userToken));

            let index = this.user.functions.findIndex(e => e.menuAction == menuName);

            if (index == -1 && this.getUserId() == null) {
                this.router.navigate(['./login']);
                return false;
            } else {
                return true;
            }

        }




        return true;
    }




}