import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from "@angular/http";

@Injectable()
export class CommonWebService {

  constructor(private http: Http) {

  }

  /**
   * 
   * @param url - Destination URL
   */
  public processGet(url: string) {

    return this.http.get(url).map(this.extractData);

  }
  /**
   * 
   * @param url 
   * @param options - Options with header params. If there is no header put null
   */
  public processGetWithHeaders(url: string, options: RequestOptionsArgs) {

    return this.http.get(url, options).map(this.extractData);
  }

  /**
   * 
   * @param url - Destination URL
   * @param query - JSON query of request payload
   */
  public processPost(url: string, query: any) {

    return this.http.post(url, query).map(this.extractData);


  }

  /**
   * 
   * @param url 
   * @param query 
   * @param options - Option with header params. If there is no header put null
   */
  public processPostWithHeaders(url: string, query: any, options: RequestOptionsArgs) {

    return this.http.post(url, query, options).map(this.extractData);
  }


  private extractData(res: Response) {

    return res.json() || {};
  }

}
