import { Component, OnInit } from '@angular/core';
import { LoignValidator } from "../service/login-validator";

@Component({
  selector: 'app-pagetop',
  templateUrl: './pagetop.component.html',
  styleUrls: ['./pagetop.component.css']
})
export class PagetopComponent implements OnInit {
  userId: string;

  constructor(public validator: LoignValidator) {
       
    
  }

  ngOnInit() {
    this.userId=this.validator.getUserId();
  }

}
