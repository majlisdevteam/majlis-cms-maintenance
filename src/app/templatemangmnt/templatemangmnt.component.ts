import { Component, OnInit } from '@angular/core';
import { CommonWebService } from "../service/common-web.service";
import { BirthdayTemplateModel } from "../model/BirthdayTemplate";
import { AppParams } from "../app.module";
import { MDCodeModel } from "../model/MDCode";
import { FileUploader } from "ng2-file-upload";
import { LoignValidator } from "../service/login-validator";
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";


@Component({
  selector: 'app-templatemangmnt',
  templateUrl: './templatemangmnt.component.html',
  styleUrls: ['./templatemangmnt.component.css'],
  providers: [CommonWebService, BirthdayTemplateModel]
})
export class TemplatemangmntComponent implements OnInit {

  fakeArray = new Array(12);
  public birthdayModelArr = new Array<BirthdayTemplateModel>();
  public arrTemplates = [];
  public BirthdayModel = new BirthdayTemplateModel();

  public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/saveTemplateIcon",queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  public showLoader: boolean = true;


  public isSuccess: boolean = false
  public successMessage = "";
  public status: number;


  constructor(public commonService: CommonWebService, public validator : LoignValidator) {

    this.validator.validateUser("template-management");

  }

  ngOnInit() {
    this.loadToGrid()
  }

  loadToGrid() {

    let urlPublic = AppParams.BASE_PATH + "birthdaytemplate/getTemplateList";
    //let urlPublic = "http://192.0.0.59:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/getTemplateList";

    this.commonService.processGet(urlPublic).subscribe(res => {

      console.log("res", res);

      let response = res.getTemplateList;

      if (response.responseCode != 999) {
        // if (res.responseCode != 999) {


        this.birthdayModelArr = response.responseData;
        this.showLoader = false
        // this.birthdayModelArr = res.responseData;
        console.log("this.birthdayModelArr", this.birthdayModelArr);

        // this.birthdayModelArr.forEach(){

        // }

        this.birthdayModelArr.forEach(model => {
          console.log("model");
          let mdcode = new MDCodeModel();
          mdcode = model.templateStatus;
          console.log("mdcode:", mdcode);

          if (mdcode.codeMessage == "Active") {
            this.status = model.templateId;
            console.log("status:", this.status);

          }
        });

        console.log("birthday model", this.birthdayModelArr)

      }


    }, error => {
      console.log("console ", error);
    });



    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);

      console.log("aaa", responsePath);
      this.BirthdayModel.templateIconPath = responsePath.responseData;
    }


  }
  selectTemplate(value) {

    console.log("value" + value);

    let urlPublic = AppParams.BASE_PATH + "birthdaytemplate/activation?templateId=" + value;
    //let urlPublic = "http://localhost:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/activation?templateId=" + value;

    this.commonService.processGet(urlPublic).subscribe(res => {

      console.log("res", res);

      let response = res.activation;

      if (response.responseCode != 999) {
        // if (res.responseCode != 999) {

        console.log("success");

      }


    }, error => {
      console.log("console ", error);
    });




  }



  saveBirthdayTemplate(evt) {

      let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.validator.getUserId());
    header.set("room", "MajlisCMSRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "AE");
    header.set("division", "MajlisAE");
    header.set("organization", "majlis");
    header.set("system", "MajlisCMS");

    console.log("headers", header)
    option.headers = header


    //  let reader=new FileReader();

    console.log("saveBirthdayTemplate ", this.BirthdayModel);
    let url = AppParams.BASE_PATH + "birthdaytemplate/createBirthDayTemplate";
    //let url = "http://192.0.0.59:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/createBirthDayTemplate";
    this.commonService.processPostWithHeaders(url, this.BirthdayModel,option).subscribe(res => {


      let response = res.createBirthDayTemplate

      if (response.responseCode == 1) {
        // if (res.responseCode == 1) {
        this.isSuccess = true
        this.showLoader = false
        this.successMessage = "Birthday Template Added!"
        this.loadToGrid()
        this.resetFields(this.BirthdayModel)


      } else {
        this.successMessage = "Error in birthday Template!"
      }

    }, error => {

      console.log("error >>> ", error);

    })
  }

  resetFields(mod: BirthdayTemplateModel) {

    this.showLoader = true;

    this.BirthdayModel.templateMessage = "";
    this.BirthdayModel.templateTitle = "";
    this.isSuccess = false
  }

  openNewTemplate() {

    this.BirthdayModel = new BirthdayTemplateModel();
    this.uploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/saveTemplateIcon" });


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      //  console.log("zzz",m);
      //  console.log("vvv",v);
      var responsePath = JSON.parse(v);

      console.log("aaa", responsePath);
      this.BirthdayModel.templateIconPath = responsePath.responseData;
    }


    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

    }


    this.uploader.onCompleteItem = (m, v) => {
      var responsePath = JSON.parse(v);

      console.log("aaa", responsePath);
      this.BirthdayModel.templateIconPath = responsePath.responseData;
    }

  }










}
