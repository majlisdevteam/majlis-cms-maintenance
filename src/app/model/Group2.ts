import { CMSUsersModel } from "./CMSUsers";
import { MDCodeModel } from "./MDCode";

export class Group2Model {
    groupId = null
    groupSystemId = null;
    groupTitle = null;
    groupIconPath = null;
    groupDescription = null;
    groupCountry = null;
    groupDistrict = null;
    groupCity = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    expiryDate = null;
    groupAdmin = new CMSUsersModel();
    groupPublicLevel = new MDCodeModel();
    groupStatus = new MDCodeModel();
}