
import { CategoryModel } from "./Category";
import { GroupModel } from "./Group";

export class MobileUserModel {

    mobileUserId : string = null;
    mobileUserMobileNo : string = null;
    mobileUserName : string = null;
    mobileUserEmail : string = null;
    mobileUserDob : string = null;
    mobileUserCountry : string = null;
    mobileUserProvince : string = null;
    mobileUserCity : string = null;
    mobileUserDateRegistration : string = null;
    mobileUserDateModified : string = null;
    mobileUserPassCode : string = null;
    majlisCategoryList = new Array<CategoryModel>();
    majlisGroupList = new Array<GroupModel>();
}