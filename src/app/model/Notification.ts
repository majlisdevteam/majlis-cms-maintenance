import { MDCodeModel } from "./MDCode";
import { CategoryModel } from "./Category";
import { GroupModel } from "./Group";
import { CMSUsersModel } from "./CMSUsers";
import { EventModel } from "./Event";

export class NotificationModel {


    notificationId = null;
    notificationSystemId = null;
    notificationIconPath = null;
    notificationMessage = null;
    dateInserted = null;
    notificationEventId = new EventModel();
    notificationGroupId = new GroupModel();
    notificationUserId = null;
    notificationStatus = new MDCodeModel();


}