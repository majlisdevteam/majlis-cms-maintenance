
import { EventModel } from "./Event";

export class CommentModel {

    public commentEventId = null;
    public commentId = null;
    public commentLike = null;
    public commentMessage = null;
    public userInserted = null;
    public dateInserted = null;
}