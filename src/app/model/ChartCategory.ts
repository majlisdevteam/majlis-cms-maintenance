export class ChartCategoryModel {

    id = null;
    categoryName = null;
    count = null;
    percentage = null;
    color = null;
}