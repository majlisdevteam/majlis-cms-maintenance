
import { MDCodeModel } from "./MDCode";

export class BirthdayTemplateModel {

    templateId= null
    templateSystemId= null;
    templateTitle= null;
    templateIconPath= null;
    templateMessage= null;
    dateInserted= null;
    userInserted= null;
    dateModified= null;
    userModified= null;
    templateStatus=  new MDCodeModel();
    
    }