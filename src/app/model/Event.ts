import { CategoryModel } from "./Category"
import { MDCodeModel } from "./MDCode";
import { GroupModel } from "./Group";
export class EventModel {

    eventId = null;
    eventSystemId = null;
    eventCaption = null;
    eventIconPath = null;
    eventMessage = null;
    eventLocation = null;
    eventLocationLat = null;
    eventLocationLont = null;
    eventTime = null;
    eventDate = null;
    userInserted = null;
    dateInserted = null;
    manualCount = null;
    userModified = null;
    dateModified = null;
    eventCategory = new CategoryModel();
    eventStatus = new MDCodeModel();
    groupId = new GroupModel();
  
}