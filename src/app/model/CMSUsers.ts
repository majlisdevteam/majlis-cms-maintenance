import { MDCodeModel } from "./MDCode";

export class CMSUsersModel {

    userId = null;
    userFullName = null;
    emailAddress = null;
    expiryDate = null;
    noAllowedNotifications = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    status = new MDCodeModel();
}