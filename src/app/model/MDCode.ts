export class MDCodeModel {

    codeId = null;
    codeType = null;
    codeSubType = null;
    codeLocale = null;
    codeSeq = null;
    codeMessage = null;
}