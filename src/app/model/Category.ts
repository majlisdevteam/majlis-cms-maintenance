import { CategoryStatusModel } from "./CategoryStatus";

export class CategoryModel {
    categoryId = null;
    categorySystemId = null;
    categoryTitle = null;
    categoryIconPath = null;
    categoryDescription = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    categoryStatus = new CategoryStatusModel();
}