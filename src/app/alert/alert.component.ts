import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  public successMsg;
  
  @ViewChild("success") success;

  constructor() { }

  ngOnInit() {

  }


  showSuccessMessage(successMsg){
    this.successMsg = successMsg;

    this.success.show();
  }



}
