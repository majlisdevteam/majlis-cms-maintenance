import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoggedInUserModel } from "../model/loggedInUserModel";


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  list:any;
  selected :any;

  methodName:any;

  public user : LoggedInUserModel;
  

  constructor(private router: Router) { 
    // this.list = [
    //    'MajlisDashboard',
    //    'MajlisUser',
    //    'MajlisGroup',
    //    'MajlisCategory',
    //    'MajlisEvent',
    //    'MajlisTemplate',
    //    'MajlisNotification',
    //    'MajlisReport'
    // ];
    
    // this.methodName = [
    //    'btnClickDash',
    //    'btnClickUser',
    //    'btnGrp',
    //    'btnCat',
    //    'btnEvt',
    //    'btnTmplt',
    //    'btnNotif',
    //    'btnReport'
    // ];

//     this.list={
//       "iconName" : 'MajlisDashboard',
//       "methodName" : 'btnClickDash',
//       "dashTitle" : 'Dashboard'
//     };
// console.log(this.list);

// let obj = this.list;
// console.log('###################################',obj.iconName);

  
          let userToken = sessionStorage.getItem('id_token');

          this.user = JSON.parse(atob(userToken));

          console.log("user model",this.user);

  }

  getPermission(menuName){

    let index = this.user.functions.findIndex(e=>e.menuAction == menuName);

    if(index == -1){
      return false;
    }else{
      return true;
    }

  }
  
  select(item) {
      this.selected = item; 
  };
  isActive(item) {
      return this.selected === item;
  };

  ngOnInit() {
  }
  btnClickDash = function () {
    console.log('Dashboard')
    // this.router.navigateByUrl('/usermgt');
    this.router.navigate(['./dash']);
  }

  btnClickUser = function () {
    console.log('User Mgt')
    // this.router.navigateByUrl('/usermgt');
    this.router.navigate(['./usermgt']);
  }

  btnGrp = function () {
    console.log('Grp Mgt')
    this.router.navigate(['./grpmngmnt'])
  }

  btnCat = function () {
    console.log('Cat Mgt')
    this.router.navigate(['./catgrymngmnt'])
  }

  btnEvt = function () {
    console.log('Evt Mgt')
    this.router.navigate(['./evntmngmnt'])
  }

  btnTmplt = function () {
    console.log('Tmplte Mgt')
    this.router.navigate(['./templatemngmnt'])
  }

  btnNotif = function () {
    console.log('Tmplte Mgt')
    this.router.navigate(['./templatemngmnt'])
  }

  btnReport = function () {
    console.log('Tmplte Mgt')
    this.router.navigate(['./templatemngmnt'])
  }
}
