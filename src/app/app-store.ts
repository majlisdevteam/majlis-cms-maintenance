

import { LoggedInUserModel } from "./model/loggedInUserModel";

export interface AppStore {
    user : LoggedInUserModel;
}