import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'

import { DashboardComponent } from './dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { MobileuserComponent } from './mobileuser/mobileuser.component';
import { GrpmangmntComponent } from './grpmangmnt/grpmangmnt.component';
import { CategorymangmntComponent } from './categorymangmnt/categorymangmnt.component';
import { EventmangmntComponent } from './eventmangmnt/eventmangmnt.component';
import { TemplatemangmntComponent } from './templatemangmnt/templatemangmnt.component';
import { NotificationComponent } from "./notification/notification.component";
import { CustomReportsComponent } from "./custom-reports/custom-reports.component";

const appRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'user-management',
        component: UsermanagementComponent
    },
    {
        path: 'mobileuser',
        component: MobileuserComponent
    },
    {
        path: 'group-management',
        component: GrpmangmntComponent
    },
    {
        path: 'category-management',
        component: CategorymangmntComponent
    },
    {
        path: 'event-management',
        component: EventmangmntComponent
    },
    {
        path: 'template-management',
        component: TemplatemangmntComponent
    },
    {
        path: 'notification',
        component: NotificationComponent
    },
    {
        path: 'custom-reports',
        component: CustomReportsComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);