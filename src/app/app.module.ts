import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { AlertModule, ModalModule, DatepickerModule, TabsModule } from 'ng2-bootstrap'
import { AppComponent } from './app.component';
import { Observable } from "rxjs/Rx";


import {  } from '@angular/platform-browser'

// import { AgmCoreModule } from 'angular2-google-maps/core';
import { DashboardComponent } from './dashboard/dashboard.component';
// import {TooltipModule, DatepickerModule} from 'ng2-bootstrap/ng2-bootstrap';
// import { DataTablesModule } from 'angular-datatables';
import { DataTableModule } from "ng2-data-table"

import { routing } from './app.routing';
import { LoginComponent } from './login/login.component';
import { PagetopComponent } from './pagetop/pagetop.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { GrpmangmntComponent } from './grpmangmnt/grpmangmnt.component';
import { CategorymangmntComponent } from './categorymangmnt/categorymangmnt.component';
import { EventmangmntComponent } from './eventmangmnt/eventmangmnt.component';
import { TemplatemangmntComponent } from './templatemangmnt/templatemangmnt.component';
import { UserpromptComponent } from './userprompt/userprompt.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
// import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { GoogleChartComponent } from './google-chart/google-chart.component';
import { NguiMapModule } from '@ngui/map';
import { NotificationComponent } from './notification/notification.component';
import { CustomReportsComponent } from './custom-reports/custom-reports.component';

import { FileUploadModule } from 'ng2-file-upload'
import { SharedService } from "./shared-service";
import { MobileuserComponent } from "./mobileuser/mobileuser.component";
import { LoignValidator } from "./service/login-validator";
import { AlertComponent } from './alert/alert.component';
// import { PagerService } from './_services/index';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    PagetopComponent,
    SideBarComponent,
    UsermanagementComponent,
    GrpmangmntComponent,
    CategorymangmntComponent,
    EventmangmntComponent,
    TemplatemangmntComponent,
    UserpromptComponent,
    GoogleChartComponent,
    NotificationComponent,
    CustomReportsComponent,
    MobileuserComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    TabsModule, 
    HttpModule,
    routing,
    AlertModule.forRoot(),
    DataTableModule,
    FileUploadModule,
    ModalModule.forRoot(),
    DatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc'
    // }),
    ChartsModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc' }),
    // DatePickerModule
    // TooltipModule, DatepickerModule

  ],
  providers: [LoignValidator],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private http: Http) {

    Observable.interval(15000).subscribe(response => {
      //  this.sessionCheck();
     
    });

  }

  sessionCheck(): any {

    this.http.get("./session.json").subscribe(response => {

    }, error => {
      console.log("error..")
    })
  }

}

export const AppParams = Object.freeze({
  BASE_PATH: "http://35.154.0.116:8080/APIGateway-1.0/",
  // BASE_PATH: "http://192.0.0.48:8080/APIGateway-1.0/",
  //  MEDIA_SERVER : 'http://192.0.0.48:8080/',
 MEDIA_SERVER : 'http://35.154.0.116:8080/'
});
