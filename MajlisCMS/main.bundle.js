webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/alert/alert.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/alert/alert.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n\n  <div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n    aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-md\">\n\n      <div class=\"modal-content\">\n\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n          <h4 class=\"modal-title\">Operation Completed</h4>\n        </div>\n        <div class=\"modal-body\">\n          <p>{{successMsg}}</p>\n\n        </div>\n\n        <div class=\"modal-footer\">\n\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n  "

/***/ }),

/***/ "../../../../../src/app/alert/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AlertComponent = (function () {
    function AlertComponent() {
    }
    AlertComponent.prototype.ngOnInit = function () {
    };
    AlertComponent.prototype.showSuccessMessage = function (successMsg) {
        this.successMsg = successMsg;
        this.success.show();
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], AlertComponent.prototype, "success", void 0);
    AlertComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__("../../../../../src/app/alert/alert.component.html"),
            styles: [__webpack_require__("../../../../../src/app/alert/alert.component.css")]
        }), 
        __metadata('design:paramtypes', [])
    ], AlertComponent);
    return AlertComponent;
}());
//# sourceMappingURL=alert.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\r\n * Specific styles of signin component\r\n */\r\n/*\r\n * General styles\r\n */\r\nbody, html {\r\n    height: 100%;\r\n    background-repeat: no-repeat;\r\n    background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));\r\n}\r\n\r\n.card-container.card {\r\n    max-width: 350px;\r\n    padding: 40px 40px;\r\n}\r\n\r\n.btn {\r\n    font-weight: 700;\r\n    height: 36px;\r\n    -moz-user-select: none;\r\n    -webkit-user-select: none;\r\n    -ms-user-select: none;\r\n        user-select: none;\r\n    cursor: default;\r\n}\r\n\r\n/*\r\n * Card component\r\n */\r\n.card {\r\n    background-color: #F7F7F7;\r\n    /* just in case there no content*/\r\n    padding: 20px 25px 30px;\r\n    margin: 0 auto 25px;\r\n    margin-top: 50px;\r\n    /* shadows and rounded borders */\r\n    border-radius: 2px;\r\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n}\r\n\r\n.profile-img-card {\r\n    width: 96px;\r\n    height: 96px;\r\n    margin: 0 auto 10px;\r\n    display: block;\r\n    border-radius: 50%;\r\n}\r\n\r\n/*\r\n * Form styles\r\n */\r\n.profile-name-card {\r\n    font-size: 16px;\r\n    font-weight: bold;\r\n    text-align: center;\r\n    margin: 10px 0 0;\r\n    min-height: 1em;\r\n}\r\n\r\n.reauth-email {\r\n    display: block;\r\n    color: #404040;\r\n    line-height: 2;\r\n    margin-bottom: 10px;\r\n    font-size: 14px;\r\n    text-align: center;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    white-space: nowrap;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.form-signin #inputEmail,\r\n.form-signin #inputPassword {\r\n    direction: ltr;\r\n    height: 44px;\r\n    font-size: 16px;\r\n}\r\n\r\n.form-signin input[type=email],\r\n.form-signin input[type=password],\r\n.form-signin input[type=text],\r\n.form-signin button {\r\n    width: 100%;\r\n    display: block;\r\n    margin-bottom: 10px;\r\n    z-index: 1;\r\n    position: relative;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.form-signin .form-control:focus {\r\n    border-color: rgb(104, 145, 162);\r\n    outline: 0;\r\n    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);\r\n}\r\n\r\n.btn.btn-signin {\r\n    /*background-color: #4d90fe; */\r\n    background-color: rgb(104, 145, 162);\r\n    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/\r\n    padding: 0px;\r\n    font-weight: 700;\r\n    font-size: 14px;\r\n    height: 36px;\r\n    border-radius: 3px;\r\n    border: none;\r\n    transition: all 0.218s;\r\n}\r\n\r\n.btn.btn-signin:hover,\r\n.btn.btn-signin:active,\r\n.btn.btn-signin:focus {\r\n    background-color: rgb(12, 97, 33);\r\n    cursor : pointer;\r\n}\r\n\r\n.forgot-password {\r\n    color: rgb(104, 145, 162);\r\n}\r\n\r\n.forgot-password:hover,\r\n.forgot-password:active,\r\n.forgot-password:focus{\r\n    color: rgb(12, 97, 33);\r\n    cursor : pointer;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_loggedInUserModel__ = __webpack_require__("../../../../../src/app/model/loggedInUserModel.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(userData, webservice) {
        this.userData = userData;
        this.webservice = webservice;
        this.btnSignin = true;
        this.signinMask = false;
        this.loginFailMsg = "";
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            //templateUrl: './app.component.html',
            template: '<router-outlet></router-outlet>',
            styles: [__webpack_require__("../../../../../src/app/app.component.css"), __webpack_require__("../../../../../src/app/styles/loaders.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__model_loggedInUserModel__["a" /* LoggedInUserModel */], __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__model_loggedInUserModel__["a" /* LoggedInUserModel */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__model_loggedInUserModel__["a" /* LoggedInUserModel */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _b) || Object])
    ], AppComponent);
    return AppComponent;
    var _a, _b;
}());
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__("../../../../ng2-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_data_table__ = __webpack_require__("../../../../ng2-data-table/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pagetop_pagetop_component__ = __webpack_require__("../../../../../src/app/pagetop/pagetop.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__side_bar_side_bar_component__ = __webpack_require__("../../../../../src/app/side-bar/side-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__usermanagement_usermanagement_component__ = __webpack_require__("../../../../../src/app/usermanagement/usermanagement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__grpmangmnt_grpmangmnt_component__ = __webpack_require__("../../../../../src/app/grpmangmnt/grpmangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__categorymangmnt_categorymangmnt_component__ = __webpack_require__("../../../../../src/app/categorymangmnt/categorymangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__eventmangmnt_eventmangmnt_component__ = __webpack_require__("../../../../../src/app/eventmangmnt/eventmangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__templatemangmnt_templatemangmnt_component__ = __webpack_require__("../../../../../src/app/templatemangmnt/templatemangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__userprompt_userprompt_component__ = __webpack_require__("../../../../../src/app/userprompt/userprompt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ngx_bootstrap_pagination__ = __webpack_require__("../../../../ngx-bootstrap/pagination/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ng2_charts__ = __webpack_require__("../../../../ng2-charts/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_20_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__google_chart_google_chart_component__ = __webpack_require__("../../../../../src/app/google-chart/google-chart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ngui_map__ = __webpack_require__("../../../../@ngui/map/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ngui_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22__ngui_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__notification_notification_component__ = __webpack_require__("../../../../../src/app/notification/notification.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__custom_reports_custom_reports_component__ = __webpack_require__("../../../../../src/app/custom-reports/custom-reports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__mobileuser_mobileuser_component__ = __webpack_require__("../../../../../src/app/mobileuser/mobileuser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__alert_alert_component__ = __webpack_require__("../../../../../src/app/alert/alert.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AppParams; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





























// import { PagerService } from './_services/index';
var AppModule = (function () {
    function AppModule(http) {
        this.http = http;
        __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].interval(15000).subscribe(function (response) {
            //  this.sessionCheck();
        });
    }
    AppModule.prototype.sessionCheck = function () {
        this.http.get("./session.json").subscribe(function (response) {
        }, function (error) {
            console.log("error..");
        });
    };
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_10__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_11__pagetop_pagetop_component__["a" /* PagetopComponent */],
                __WEBPACK_IMPORTED_MODULE_12__side_bar_side_bar_component__["a" /* SideBarComponent */],
                __WEBPACK_IMPORTED_MODULE_13__usermanagement_usermanagement_component__["a" /* UsermanagementComponent */],
                __WEBPACK_IMPORTED_MODULE_14__grpmangmnt_grpmangmnt_component__["a" /* GrpmangmntComponent */],
                __WEBPACK_IMPORTED_MODULE_15__categorymangmnt_categorymangmnt_component__["a" /* CategorymangmntComponent */],
                __WEBPACK_IMPORTED_MODULE_16__eventmangmnt_eventmangmnt_component__["a" /* EventmangmntComponent */],
                __WEBPACK_IMPORTED_MODULE_17__templatemangmnt_templatemangmnt_component__["a" /* TemplatemangmntComponent */],
                __WEBPACK_IMPORTED_MODULE_18__userprompt_userprompt_component__["a" /* UserpromptComponent */],
                __WEBPACK_IMPORTED_MODULE_21__google_chart_google_chart_component__["a" /* GoogleChartComponent */],
                __WEBPACK_IMPORTED_MODULE_23__notification_notification_component__["a" /* NotificationComponent */],
                __WEBPACK_IMPORTED_MODULE_24__custom_reports_custom_reports_component__["a" /* CustomReportsComponent */],
                __WEBPACK_IMPORTED_MODULE_26__mobileuser_mobileuser_component__["a" /* MobileuserComponent */],
                __WEBPACK_IMPORTED_MODULE_28__alert_alert_component__["a" /* AlertComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["a" /* TabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9__app_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["b" /* AlertModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ng2_data_table__["a" /* DataTableModule */],
                __WEBPACK_IMPORTED_MODULE_25_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["c" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["d" /* DatepickerModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_19_ngx_bootstrap_pagination__["a" /* PaginationModule */].forRoot(),
                // AgmCoreModule.forRoot({
                //   apiKey: 'AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc'
                // }),
                __WEBPACK_IMPORTED_MODULE_20_ng2_charts__["ChartsModule"],
                __WEBPACK_IMPORTED_MODULE_22__ngui_map__["NguiMapModule"].forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc' }),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_27__service_login_validator__["a" /* LoignValidator */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */]) === 'function' && _a) || Object])
    ], AppModule);
    return AppModule;
    var _a;
}());
var AppParams = Object.freeze({
    BASE_PATH: "http://35.154.0.116:8080/APIGateway-1.0/",
    // BASE_PATH: "http://192.0.0.48:8080/APIGateway-1.0/",
    //  MEDIA_SERVER : 'http://192.0.0.48:8080/',
    MEDIA_SERVER: 'http://35.154.0.116:8080/'
});
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__usermanagement_usermanagement_component__ = __webpack_require__("../../../../../src/app/usermanagement/usermanagement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mobileuser_mobileuser_component__ = __webpack_require__("../../../../../src/app/mobileuser/mobileuser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__grpmangmnt_grpmangmnt_component__ = __webpack_require__("../../../../../src/app/grpmangmnt/grpmangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categorymangmnt_categorymangmnt_component__ = __webpack_require__("../../../../../src/app/categorymangmnt/categorymangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__eventmangmnt_eventmangmnt_component__ = __webpack_require__("../../../../../src/app/eventmangmnt/eventmangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__templatemangmnt_templatemangmnt_component__ = __webpack_require__("../../../../../src/app/templatemangmnt/templatemangmnt.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__notification_notification_component__ = __webpack_require__("../../../../../src/app/notification/notification.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__custom_reports_custom_reports_component__ = __webpack_require__("../../../../../src/app/custom-reports/custom-reports.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });











var appRoutes = [
    {
        path: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_1__dashboard_dashboard_component__["a" /* DashboardComponent */]
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */]
    },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */]
    },
    {
        path: 'user-management',
        component: __WEBPACK_IMPORTED_MODULE_3__usermanagement_usermanagement_component__["a" /* UsermanagementComponent */]
    },
    {
        path: 'mobileuser',
        component: __WEBPACK_IMPORTED_MODULE_4__mobileuser_mobileuser_component__["a" /* MobileuserComponent */]
    },
    {
        path: 'group-management',
        component: __WEBPACK_IMPORTED_MODULE_5__grpmangmnt_grpmangmnt_component__["a" /* GrpmangmntComponent */]
    },
    {
        path: 'category-management',
        component: __WEBPACK_IMPORTED_MODULE_6__categorymangmnt_categorymangmnt_component__["a" /* CategorymangmntComponent */]
    },
    {
        path: 'event-management',
        component: __WEBPACK_IMPORTED_MODULE_7__eventmangmnt_eventmangmnt_component__["a" /* EventmangmntComponent */]
    },
    {
        path: 'template-management',
        component: __WEBPACK_IMPORTED_MODULE_8__templatemangmnt_templatemangmnt_component__["a" /* TemplatemangmntComponent */]
    },
    {
        path: 'notification',
        component: __WEBPACK_IMPORTED_MODULE_9__notification_notification_component__["a" /* NotificationComponent */]
    },
    {
        path: 'custom-reports',
        component: __WEBPACK_IMPORTED_MODULE_10__custom_reports_custom_reports_component__["a" /* CustomReportsComponent */]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "../../../../../src/app/categorymangmnt/categorymangmnt.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*.panel-heading{\r\n    background-color: #009688;\r\n    margin-top: 40px;\r\n}*/\r\n\r\n\r\n/*###############################################*/\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n.cat-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.cat-desc{\r\n    color: #6f7982;\r\n}\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:10px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/categorymangmnt/categorymangmnt.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop>\r\n</app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<div class=\"container\">\r\n    <div class=\"page-title\">\r\n        <h2>Category Management</h2>\r\n    </div>\r\n    <div class=\"pull-right\"><button class=\"btn btn-success\" (click)=\"openCategoryForm('insert','')\">New Category</button></div><br><br><br>\r\n    <div class=\"panel panel-default\">\r\n        <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n        <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n        <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n            <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n                <tr>\r\n                    <th></th>\r\n                    <th>Category</th>\r\n                    <th>Status</th>\r\n                    <th></th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n                <tr *ngFor=\"let c of categoryModel\">\r\n                    <td>\r\n                        <img class=\"circle-image pull-left\" [src]=\"c.categoryIconPath\" />\r\n                    </td>\r\n                    <td>\r\n                        <div class=\"col-sm-12 cat-name\">\r\n                            {{c.categoryTitle}}\r\n                        </div>\r\n                        <div class=\"col-sm-12 cat-desc\">\r\n                            {{c.categoryDescription}}\r\n                        </div>\r\n                    </td>\r\n                    <td>\r\n                        <span class=\"glyphicon glyphicon-ok\" *ngIf=\"getStatusOfUser(c.categoryStatus.codeSeq).codeSeq==1\"></span>\r\n                        <span class=\"glyphicon glyphicon-remove\" *ngIf=\"getStatusOfUser(c.categoryStatus.codeSeq).codeSeq==2\"></span>\r\n                    </td>\r\n                    <td>\r\n                        <!--<button class=\"btn btn-primary\" (click)=\"editCategory.show()\">-->\r\n                        <button class=\"btn btn-primary\" (click)=\"openCategoryForm('update',c.categoryId)\">\r\n                            Edit\r\n                        </button>\r\n                    </td>\r\n                </tr>\r\n\r\n\r\n            </tbody>\r\n        </table>\r\n\r\n    </div>\r\n\r\n\r\n    <!--############## Add new Category ################-->\r\n    <div bsModal #addCategory=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-md\">\r\n\r\n            <div class=\"modal-content\">\r\n\r\n                <form #newCategoryForm=\"ngForm\" (ngSubmit)=\"fireFormAction($e)\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\">\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"addCategory.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                        <h4 class=\"modal-title\">{{formTitle}}</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Title</label>\r\n                            <input type=\"text\" [(ngModel)]=\"categoryModelDeatail.categoryTitle\" name=\"categoryModelDetail.categoryTitle\" class=\"form-control\"\r\n                                required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Description</label>\r\n                            <textarea [(ngModel)]=\"categoryModelDeatail.categoryDescription\" name=\"categoryModelDetail.categoryDescription\" class=\"form-control\"\r\n                                required></textarea>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Status</label>\r\n                            <select name=\"categoryModelDeatail.categoryStatus\" class=\"form-control\" (change)=\"changeCategoryStatus($event.target)\">\r\n                                <option *ngFor=\"let m of arrMdStatus\" [selected]=\"m.codeSeq == categoryModelDeatail.categoryStatus.codeSeq\" [ngValue]=\"m\">{{m.codeMessage}}</option>\r\n          \r\n                            </select>\r\n                        </div>\r\n\r\n                        <label class=\"control-label\">Select Icon Select Icon (Support file type - JPG)</label>\r\n                        <!--<input id=\"input-1\" type=\"file\" class=\"file\" >-->\r\n                        <input type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n\r\n                        <div class=\"col-md-9\" style=\"margin-bottom: 40px;width: 100%\">\r\n\r\n                            <table class=\"table\">\r\n                                <thead>\r\n                                    <tr>\r\n\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr *ngFor=\"let item of uploader.queue\">\r\n                                        <!-- <td><strong>{{ item?.file?.name }}</strong></td> -->\r\n                                        <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                                        <td *ngIf=\"uploader.isHTML5\">\r\n                                            <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                                                <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                                            </div>\r\n                                        </td>\r\n                                        <td style=\"text-align: right\">\r\n                                            <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                                            <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                                            <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                                        </td>\r\n                                        <td nowrap style=\"text-align: right; width: 150px;\">\r\n                                            <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                                            <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n\r\n                                        </td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <br/><br/>\r\n                    <div class=\"modal-footer\">\r\n                        <img *ngIf=\"serviceProgress\" src=\"assets/inlineLoader.gif\" style=\"width:20px;\" />\r\n                        <button type=\"reset\" class=\"btn btn-default\" (click)=\"addCategory.hide()\">Close</button>\r\n                        <button type=\"submit\" *ngIf=\"!isSuccess && operationType == 'insert'\" [disabled]=\"serviceProgress\" class=\"btn btn-success\">Create</button>\r\n                        <button type=\"submit\" *ngIf=\"!serviceProgress && operationType == 'update'\" class=\"btn btn-success\">Save</button>\r\n\r\n\r\n                    </div>\r\n                </form>\r\n                <!--############### end form ############-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!--############## Edit Category ################-->\r\n    <div bsModal #editCategory=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-md\">\r\n\r\n            <div class=\"modal-content\">\r\n\r\n                <form #editCategoryForm=\"ngForm\" (ngSubmit)=\"submitForm($e)\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\">\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"editCategory.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                        <h4 class=\"modal-title\">Edit Category</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Title</label>\r\n                            <input type=\"text\" [(ngModel)]=\"categoryModelDeatail.categoryTitle\" name=\"categoryModelDetail.categoryTitle\" class=\"form-control\"\r\n                                required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Description</label>\r\n                            <input type=\"text\" [(ngModel)]=\"categoryModelDeatail.categoryDescription\" name=\"categoryModelDetail.categoryDescription\"\r\n                                class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <!--<label class=\"radio-inline\">\r\n                        <input type=\"radio\" name=\"optradio\" checked>Active\r\n                    </label>\r\n                    <label class=\"radio-inline\">\r\n                        <input type=\"radio\" name=\"optradio\">Inactive\r\n                    </label><br><br>-->\r\n\r\n                        <label class=\"control-label\">Select Icon</label>\r\n                        <input id=\"input-1\" type=\"file\" class=\"file\">\r\n                        <!--<input id=\"input-1\" type=\"file\" class=\"file\" required>-->\r\n\r\n                    </div>\r\n\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"reset\" class=\"btn btn-default\" (click)=\"editCategory.hide()\">Close</button>\r\n                        <button type=\"button\" class=\"btn btn-success\" (click)=\"saveCategoryChanges(event)\">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>\r\n                    </div>\r\n                </form>\r\n                <!--############### end form ############-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"modal-header\" style=\"background-color: #449D44; color: azure\">\r\n                <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                <h4 class=\"modal-title\">Operation Completed</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <p>{{successMessage}}</p>\r\n\r\n            </div>\r\n\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div bsModal #fail=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"modal-header\" style=\"background-color: chocolate;color: azure\">\r\n                <button type=\"button\" class=\"close pull-right\" (click)=\"fail.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                <h4 class=\"modal-title\">Operation Failed!</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <p>{{failMessage}}</p>\r\n\r\n            </div>\r\n\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-default\" (click)=\"fail.hide()\">Close</button>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/categorymangmnt/categorymangmnt.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_Category__ = __webpack_require__("../../../../../src/app/model/Category.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategorymangmntComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CategorymangmntComponent = (function () {
    function CategorymangmntComponent(http, commonService, validator) {
        this.commonService = commonService;
        this.validator = validator;
        this.fakeArray = new Array(12);
        this.categoryModel = new Array();
        this.categoryModelDeatail = new __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */];
        // public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", });
        this.uploader = new __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-CategoryManagement-1.0/service/categories/saveCategoryIcon", queueLimit: 1 });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.showLoader = true;
        this.serviceProgress = false;
        // private doctors = [];
        this.arrMdStatus = new Array();
        // public arrGroupActiveStatus = new Array<MDCodeModel>();
        this.categoryStatus = new Array();
        this.validator.validateUser("category-management");
    }
    CategorymangmntComponent.prototype.changeCategoryStatus = function (taeget) {
        console.log("cat status ", taeget.value);
        console.log("arrMdStatus ", this.arrMdStatus);
        this.categoryModelDeatail.categoryStatus = this.arrMdStatus.filter(function (val) {
            return val.codeMessage === taeget.value;
        })[0];
        console.log("category details ", this.categoryModelDeatail);
    };
    CategorymangmntComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadGroupsToGrid();
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onBeforeUploadItem = (function (e) {
            _this.serviceProgress = true;
        });
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.categoryModelDeatail.categoryIconPath = responsePath.responseData;
        };
        // Category Status
        var urlActiveStatus = __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].BASE_PATH + "categories/getStatus?type=CATEGORY&subType=CATEGORY_STATUS";
        this.commonService.processGet(urlActiveStatus).subscribe(function (res) {
            var response = res.getStatus;
            if (response.responseCode == 1) {
                _this.categoryStatus = response.responseData;
            }
        }, function (error) {
            console.log("console ", error);
        });
    };
    CategorymangmntComponent.prototype.getStatusOfCategory = function (statusId) {
        var m = this.categoryStatus.filter(function (res) {
            return res.codeSeq == statusId;
        })[0];
        return m;
    };
    CategorymangmntComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    CategorymangmntComponent.prototype.fileOverAnother = function (e) {
        this.hasAnotherDropZoneOver = e;
    };
    CategorymangmntComponent.prototype.setSelectedEntities = function ($event) {
        this.selectedEntities = $event;
    };
    CategorymangmntComponent.prototype.loadGroupsToGrid = function () {
        var _this = this;
        this.showLoader = true;
        var k = this;
        var url = __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].BASE_PATH + "categories/getAllCategoryById";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res.allCategories);
            _this.arrMdStatus = res.categorystatus.responseData;
            _this.categoryModel = res.allCategories.responseData;
            console.log("response api ", _this.arrMdStatus);
            _this.showLoader = false;
        }, function (error) {
            _this.showLoader = false;
            console.log("error ", error);
        });
    };
    CategorymangmntComponent.prototype.fireFormAction = function (event) {
        if (this.operationType == 'insert') {
            this.createCategory(event);
        }
        else if (this.operationType == 'update') {
            this.saveCategoryChanges(event);
        }
    };
    CategorymangmntComponent.prototype.openCategoryForm = function (type, categoryId) {
        var _this = this;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-CategoryManagement-1.0/service/categories/saveCategoryIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onBeforeUploadItem = (function (e) {
            _this.serviceProgress = true;
        });
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.serviceProgress = false;
            console.log("aaa", responsePath);
            _this.categoryModelDeatail.categoryIconPath = responsePath.responseData;
        };
        if (type == "insert") {
            this.operationType = "insert";
            this.categoryModelDeatail = new __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */];
            this.formTitle = "New Category";
            this.categoryModelDeatail.categoryStatus.codeId = 1;
            this.addCategory.show();
        }
        else if (type == "update") {
            this.operationType = "update";
            this.formTitle = "Edit Category";
            this.getCategoryDetailsById(categoryId);
        }
    };
    CategorymangmntComponent.prototype.createCategory = function (evt) {
        var _this = this;
        console.log("Add Category ");
        var url = __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].BASE_PATH + "categories/createCategory";
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        this.commonService.processPostWithHeaders(url, this.categoryModelDeatail, option).subscribe(function (res) {
            _this.loadGroupsToGrid();
            _this.addCategory.hide();
        }, function (error) {
            console.log("error >>> ", error);
        });
    };
    CategorymangmntComponent.prototype.getStatusOfUser = function (statusId) {
        var m = this.arrMdStatus.filter(function (res) {
            return res.codeSeq == statusId;
        })[0];
        return m;
    };
    CategorymangmntComponent.prototype.saveCategoryChanges = function (evt) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        console.log("saveGroupChanges ", this.categoryModelDeatail);
        var url = __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].BASE_PATH + "categories/updatecategory";
        this.commonService.processPostWithHeaders(url, this.categoryModelDeatail, option).subscribe(function (res) {
            var response = res.updateCategory;
            if (response.responseCode == 1) {
                _this.addCategory.hide();
                _this.successMessage = "Category has been updated successfully!";
                _this.success.show();
            }
            else {
                _this.failMessage = "The Category cannot be updated at this time. Please try again later!";
                _this.fail.show();
            }
            _this.loadGroupsToGrid();
        }, function (error) {
            _this.failMessage = "The Category cannot be updated at this time. Please try again later!";
            _this.fail.show();
        });
    };
    CategorymangmntComponent.prototype.getCategoryDetailsById = function (categoryId) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_5__app_module__["b" /* AppParams */].BASE_PATH + "categories/getCategoryById?categoryId=" + categoryId;
        this.commonService.processGet(url).subscribe(function (res) {
            _this.categoryModelDeatail = res.getCategoryById.responseData[0];
            _this.addCategory.show();
        }, function (error) {
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('childModal'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */]) === 'function' && _a) || Object)
    ], CategorymangmntComponent.prototype, "childModal", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("editCategory"), 
        __metadata('design:type', Object)
    ], CategorymangmntComponent.prototype, "editCategory", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("addCategory"), 
        __metadata('design:type', Object)
    ], CategorymangmntComponent.prototype, "addCategory", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], CategorymangmntComponent.prototype, "success", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fail"), 
        __metadata('design:type', Object)
    ], CategorymangmntComponent.prototype, "fail", void 0);
    CategorymangmntComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-categorymangmnt',
            template: __webpack_require__("../../../../../src/app/categorymangmnt/categorymangmnt.component.html"),
            styles: [__webpack_require__("../../../../../src/app/categorymangmnt/categorymangmnt.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */]) === 'function' && _d) || Object])
    ], CategorymangmntComponent);
    return CategorymangmntComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=categorymangmnt.component.js.map

/***/ }),

/***/ "../../../../../src/app/custom-reports/custom-reports.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n/*###############################################*/\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n.repo-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:10px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\ntextarea {\r\n    resize: none;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/custom-reports/custom-reports.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop></app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<div class=\"container\">\r\n  <div class=\"page-title\">\r\n    <h2>Custom Reports</h2>\r\n  </div>\r\n  <div class=\"pull-right\"><button class=\"btn btn-success\" (click)=\"newReport.show()\">New Report</button></div><br><br><br>\r\n  <div class=\"panel panel-default\">\r\n\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n<img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n\r\n    <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n      <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n        <tr>\r\n          <th>Report Name</th>\r\n          <th>Status</th>\r\n          <th></th>\r\n          <th></th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let c of CustomReportModelArr;\">\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{c.reportName}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <span class=\"glyphicon glyphicon-ok\" *ngIf=\"c.status== 1\"></span>\r\n            <span class=\"glyphicon glyphicon-remove\" *ngIf=\"c.status== 2\"></span>\r\n          </td>\r\n          <td>\r\n            <button class=\"btn btn-primary pull-right\" (click)=\"openEditReportForm(c.reportId)\">\r\n                            Edit\r\n                        </button>\r\n          </td>\r\n          <td>\r\n            <button class=\"btn btn-primary pull-right\" (click)=\"generateReport(c.query)\">\r\n                            Generate\r\n                        </button>\r\n          </td>\r\n        </tr>\r\n\r\n\r\n      </tbody>\r\n    </table>\r\n\r\n\r\n\r\n\r\n\r\n  </div>\r\n\r\n  <!--######################Edit Report Modal######################-->\r\n  <div bsModal #editReport=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n        <form>\r\n          <!--################ start edit form ################-->\r\n          <div class=\"modal-header\">\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"editReport.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\">Edit Report</h4>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Report Name</label>\r\n              <input type=\"text\" [(ngModel)]=\"customReportModelEdit.reportName\" name=\"customReportModelEdit.reportName\"  class=\"form-control\" placeholder=\"Report Name\" required>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"comment\">Query</label>\r\n              <textarea class=\"form-control\" [(ngModel)]=\"customReportModelEdit.query\" name=\"customReportModelEdit.query\"  rows=\"5\" placeholder=\"SELECT example FROM example\" required></textarea>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Report Status</label>\r\n              <select [(ngModel)]=\"customReportModelEdit.status\" name=\"customReportModelEdit.status\" class=\"form-control\">\r\n                <option value =1>Active</option>\r\n                <option value =2>Inactive</option>\r\n              </select>\r\n            </div>\r\n\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"editReport.hide()\">Close</button>\r\n            <button type=\"submit\" class=\"btn btn-success\" (click)=\"editReportDetails()\">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>\r\n          </div>\r\n        </form>\r\n        <!--################ end edit form ################-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!--######################New Report Modal######################-->\r\n  <div bsModal #newReport=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n        <form>\r\n          <!--################ start new report form ################-->\r\n          <div class=\"modal-header\">\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"newReport.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\">New Report</h4>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Report Name</label>\r\n              <input type=\"text\" [(ngModel)]=\"customReportModel.reportName\" name=\"customReportModel.reportName\" class=\"form-control\" placeholder=\"Report Name\" required>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"comment\">Query</label>\r\n              <textarea class=\"form-control\" [(ngModel)]=\"customReportModel.query\" name=\"customReportModel.query\" rows=\"5\" placeholder=\"SELECT example FROM example\" required></textarea>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Report Status</label>\r\n              <select [(ngModel)]=\"customReportModel.status\" name=\"status\" class=\"form-control\">\r\n                <option value =1>Active</option>\r\n                <option value =2>Inactive</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"newReport.hide()\">Close</button>\r\n            <button type=\"submit\" class=\"btn btn-success\" (click)=\"saveNewReport()\">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>\r\n          </div>\r\n        </form>\r\n        <!--################ end new report form ################-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!--######################Report Format Modal######################-->\r\n  <!--<div bsModal #reportFormat=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-sm\">\r\n\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"reportFormat.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Report Format</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <div class=\"col-sm-6 text-center\">\r\n            <button type=\"button\" class=\"btn btn-success\">PDF</button>\r\n          </div>\r\n          <div class=\"col-sm-6 text-center\">\r\n            <button type=\"button\" class=\"btn btn-success\">EXCEL</button>\r\n          </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <br><br>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>-->\r\n\r\n\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/custom-reports/custom-reports.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_CustomReport__ = __webpack_require__("../../../../../src/app/model/CustomReport.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomReportsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CustomReportsComponent = (function () {
    function CustomReportsComponent(http, commonService, validator) {
        this.http = http;
        this.commonService = commonService;
        this.validator = validator;
        this.fakeArray = new Array(12);
        this.CustomReportModelArr = new Array();
        this.customReportModel = new __WEBPACK_IMPORTED_MODULE_3__model_CustomReport__["a" /* CustomReportModel */]();
        this.customReportModelEdit = new __WEBPACK_IMPORTED_MODULE_3__model_CustomReport__["a" /* CustomReportModel */]();
        this.showLoader = true;
        this.validator.validateUser("custom-reports");
    }
    CustomReportsComponent.prototype.ngOnInit = function () {
        this.loadToGrid();
    };
    CustomReportsComponent.prototype.loadToGrid = function () {
        var _this = this;
        var urlPublic = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "reports/getReportList";
        //let urlPublic = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/getReportList";
        this.commonService.processGet(urlPublic).subscribe(function (res) {
            console.log("res", res);
            var response = res.getReportList;
            if (response.responseCode != 999) {
                _this.CustomReportModelArr = response.responseData;
                _this.showLoader = false;
                console.log("this.CustomReportModelArr", _this.CustomReportModelArr);
            }
        }, function (error) {
            console.log("console ", error);
        });
    };
    CustomReportsComponent.prototype.openEditReportForm = function (reportId) {
        var _this = this;
        console.log("reportId", reportId);
        this.reportId = reportId;
        var urlPublic = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "reports/getReportDetail?reportId=" + reportId;
        //let urlPublic = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/getReportDetail?reportId=" + reportId;
        this.commonService.processGet(urlPublic).subscribe(function (res) {
            console.log("res", res);
            var response = res.getReportDetail;
            if (response.responseCode != 999) {
                _this.customReportModelEdit = response.responseData[0];
                // this.customReportModelEdit = res.responseData[0];
                console.log("this.customReportModelEdit", _this.customReportModelEdit);
            }
        });
        this.editReportForm.show();
    };
    CustomReportsComponent.prototype.saveNewReport = function (evt) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "reports/createReport";
        //let url = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/createReport";
        this.commonService.processPostWithHeaders(url, this.customReportModel, option).subscribe(function (res) {
            var response = res.createReport;
            if (response.responseCode == 1) {
                //if (res.responseCode == 1) {
                _this.loadToGrid();
                _this.successMessage = "Report Added!";
                _this.customReportModel.query = "";
                _this.customReportModel.reportName = "";
            }
            else {
                _this.successMessage = "Error in Report creation!";
            }
        }, function (error) {
            console.log("error >>> ", error);
        });
    };
    CustomReportsComponent.prototype.editReportDetails = function () {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        console.log("inside edit report..");
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "reports/updateReport";
        //let url = "http://192.0.0.59:8080/Majlis-CustomReports-1.0/service/reports/updateReport";
        this.commonService.processPostWithHeaders(url, this.customReportModelEdit, option).subscribe(function (res) {
            var response = res.updateReport;
            if (response.responseCode == 1) {
                // if (res.responseCode == 1) {
                _this.successMessage = "Report Edited!";
            }
            else {
                _this.successMessage = "Error in Report Edit!";
            }
        }, function (error) {
            console.log("error >>> ", error);
        });
        console.log("this.successMessage", this.successMessage);
    };
    CustomReportsComponent.prototype.generateReport = function (query) {
        console.log("query", query);
        var urlPublic = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-CustomReports-1.0/service/reports/getReport?query=" + query;
        console.log("url ", urlPublic);
        window.open(urlPublic, "_blank");
        console.log("report not genereated");
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("editReport"), 
        __metadata('design:type', Object)
    ], CustomReportsComponent.prototype, "editReportForm", void 0);
    CustomReportsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-custom-reports',
            template: __webpack_require__("../../../../../src/app/custom-reports/custom-reports.component.html"),
            styles: [__webpack_require__("../../../../../src/app/custom-reports/custom-reports.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__service_login_validator__["a" /* LoignValidator */]) === 'function' && _c) || Object])
    ], CustomReportsComponent);
    return CustomReportsComponent;
    var _a, _b, _c;
}());
// setPage(page: number) {
//       if (page < 1 || page > this.pager.totalPages) {
//           return;
//       }
//       // get pager object from service
//       this.pager = this.pagerService.getPager(this.allItems.length, page);
//       // get current page of items
//       this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
//   }
//# sourceMappingURL=custom-reports.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.sebm-google-map-container {\r\n  height: 300px;\r\n}\r\n.main{\r\n      padding: 10px 10px 0 10px;\r\n      position: absolute;\r\n\t\t\twidth: calc(100% - 51px); \r\n\t\t\tmargin-left: 50px;\r\n\t\t\tfloat: right;\r\n      overflow-x:hidden;\r\n}\r\n\r\n\r\n.dash-panel{\r\nbackground: #009688;\r\n}\r\n\r\n.dash-panel-count {\r\n  font-size: 40px;\r\n  font-weight: bold;\r\n  color: #fff;\r\n}\r\n\r\n.dash-panel-footer{\r\n    background: #019d8e;\r\n}\r\n.panel-footer{\r\nborder: 0;\r\n}\r\n.page-title{\r\n  background: #fff;\r\n}\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\nngui-map{\r\n  height: 400px;\r\n  margin-top: -5px;\r\n}\r\n#pie_chart{\r\n  display: block; \r\n   margin: 0 auto;\r\n}\r\n\r\n.modal-dialog{\r\n  width: 50%;\r\n  top: 20%;\r\n}\r\n\r\n\r\n\r\n/*toggle styles*/\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\r\n}\r\n\r\n.switch input {display:none;}\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked + .slider {\r\n  background-color: #2196F3;\r\n}\r\n\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #2196F3;\r\n}\r\n\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}\r\n\r\n.modal {\r\n   overflow: scroll;\r\n  }\r\n\r\nul {\r\n  display: inline-block;\r\n  list-style: square;\r\n  padding:0;\r\n  margin-top:10px;\r\n  font-size: 12px;\r\n  text-align: left;\r\n}\r\n\r\nli { \r\n  padding-left: 1em; \r\n  text-indent: -.7em;\r\n }\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop></app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<br><br>\r\n<div class=\"main\">\r\n  <div class=\"page-title\">\r\n    <h2>Dashboard</h2>\r\n    \r\n\r\n  </div>\r\n\r\n  <ngui-map zoom=\"3\" center=\"24.3340655, 51.6955822\">\r\n    <div id=\"floating-panel\">\r\n    </div>\r\n    <heatmap-layer [data]=\"points\"></heatmap-layer>\r\n  </ngui-map>\r\n\r\n  <br><br>\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-3 col-md-6\">\r\n      <div class=\"dash-panel\" style=\"cursor: pointer\" (click)=\"openGroupDetails()\">\r\n        <div class=\"panel-heading\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xs-3\">\r\n              <img src=\"assets/img/icons/MajlisUserGroup.png\" height=\"50px\" width=\"50px\" />\r\n            </div>\r\n            <div class=\"col-xs-9 text-right\">\r\n              <div class=\"dash-panel-count\">{{(dashboardItem.group_active)}}</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"panel-footer dash-panel-footer\">\r\n          <span class=\"pull-left\">Total Groups Count</span>\r\n          <span class=\"pull-right\">\r\n          <img src=\"assets/img/icons/MajlisOk.png\" height=\"20px\" width=\"20px\"/>\r\n        </span>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-3 col-md-6\">\r\n      <div class=\"dash-panel\">\r\n        <div class=\"panel-heading\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xs-3\">\r\n              <img src=\"assets/img/icons/MajlisGroup.png\" height=\"50px\" width=\"50px\" />\r\n            </div>\r\n            <div class=\"col-xs-9 text-right\">\r\n              <div class=\"dash-panel-count\">{{dashboardItem.group_pending}}</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"panel-footer dash-panel-footer\">\r\n          <span class=\"pull-left\">Pending Group Requests</span>\r\n          <span class=\"pull-right\">\r\n          <img src=\"assets/img/icons/MajlisPending.png\" height=\"20px\" width=\"20px\"/>\r\n        </span>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-3 col-md-6\" style=\"cursor: pointer\"  (click)=\"openEventPage()\">\r\n      <div class=\"dash-panel\">\r\n        <div class=\"panel-heading\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xs-3\">\r\n              <img src=\"assets/img/icons/MajlisEvent.png\" height=\"50px\" width=\"50px\" />\r\n            </div>\r\n            <div class=\"col-xs-9 text-right\">\r\n              <div class=\"dash-panel-count\">{{dashboardItem.event_active}}</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"panel-footer dash-panel-footer\">\r\n          <span class=\"pull-left\">Active Events</span>\r\n          <span class=\"pull-right\">\r\n          <img src=\"assets/img/icons/MajlisOk.png\" height=\"20px\" width=\"20px\"/>\r\n        </span>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-3 col-md-6\">\r\n      <div class=\"dash-panel\">\r\n        <div class=\"panel-heading\">\r\n          <div class=\"row\">\r\n            <div class=\"col-xs-3\">\r\n              <img src=\"assets/img/icons/MajlisEventPending.png\" height=\"50px\" width=\"50px\" />\r\n            </div>\r\n            <div class=\"col-xs-9 text-right\">\r\n              <div class=\"dash-panel-count\">{{dashboardItem.tot_notification}}/{{dashboardItem.tot_notification_limit}}</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"panel-footer dash-panel-footer\">\r\n          <span class=\"pull-left\">Remain Notification Count</span>\r\n          <span class=\"pull-right\">\r\n          <img src=\"assets/img/icons/MajlisPending.png\" height=\"20px\" width=\"20px\"/>\r\n        </span>\r\n          <div class=\"clearfix\"></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <br><br>\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-5 pull-left\" style=\"margin-left: 15px; padding-top:10px; padding-bottom:10px; border-radius: 5px; border: 2px solid #cccccc;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\">\r\n      <div style=\" border-bottom: 1px solid; border-color:#ccc;\">Category</div>\r\n      <div align=\"right\">\r\n        <ul>\r\n          <li *ngFor=\"let c of chartCategoryModel;\" style='color:\"{{c.color}}\"'>{{c.categoryName}}</li>\r\n          <!--id=\"c.id\" style='color:\"{{c.color}}\"'-->\r\n          <!--<li style=\"color: #6eba8c;\">Category 1</li>\r\n          <li style=\"color: #b9f2a1;\">Category 1</li>\r\n          <li style=\"color: #10c4b5;\">Category 1</li>\r\n          <li style=\"color: #005562;\">Category 1</li>-->\r\n        </ul>\r\n      </div>\r\n      <div id=\"pie_chart\" [chartData]=\"pie_ChartData\" [chartOptions]=\"pie_ChartOptions\" chartType=\"PieChart\" GoogleChart align=\"center\"></div>\r\n\r\n    </div>\r\n    <div class=\"col-sm-1\"></div>\r\n    <div class=\"col-sm-5 pull-right\" style=\"margin-right: 15px; padding-top:10px; padding-bottom:10px; border-radius: 5px; border: 2px solid #cccccc;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\">\r\n      <div style=\" border-bottom: 1px solid; border-color:#ccc;\">Top Group</div>\r\n\r\n      <div align=\"right\">\r\n        <ul>\r\n          <li style=\"color: #009688;\" *ngFor=\"let g of chartGroupModel;\">{{g.groupName}}</li><br>\r\n          <!--<li style=\"color: #6eba8c;\">Category 1</li>\r\n          <li style=\"color: #b9f2a1;\">Category 1</li>\r\n          <li style=\"color: #10c4b5;\">Category 1</li>\r\n          <li style=\"color: #005562;\">Category 1</li>-->\r\n        </ul>\r\n      </div>\r\n      <div id=\"bar_chart\" [chartData]=\"bar_ChartData\" [chartOptions]=\"bar_ChartOptions\" chartType=\"BarChart\" GoogleChart></div>\r\n    </div>\r\n  </div>\r\n  <br><br><br>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngui_map__ = __webpack_require__("../../../../@ngui/map/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngui_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__ngui_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_DashboardItems__ = __webpack_require__("../../../../../src/app/model/DashboardItems.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_ChartCategory__ = __webpack_require__("../../../../../src/app/model/ChartCategory.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_service__ = __webpack_require__("../../../../../src/app/shared-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DashboardComponent = (function () {
    function DashboardComponent(commonService, sharedService, route, validator, router) {
        this.commonService = commonService;
        this.sharedService = sharedService;
        this.route = route;
        this.validator = validator;
        this.router = router;
        this.arrEventModelForMap = new Array();
        this.points = [];
        this.dashboardItem = new __WEBPACK_IMPORTED_MODULE_4__model_DashboardItems__["a" /* DashBoardItemModel */]();
        this.chartGroupModel = new Array();
        this.chartCategoryModel = new Array();
        this.pie_ChartData = [];
        this.pie_ChartOptions = {
            // title: 'User involvement for category',
            width: '70%',
            height: 300,
            pieHole: 0.6,
            colors: ['#009688', '#6eba8c', '#b9f2a1', '#10c4b5', '#005562'],
            legend: { position: 'none' },
            chartArea: { left: 10, top: 10, right: 10, bottom: 10 },
            animation: {
                duration: 1000,
                easing: 'out',
                startup: true
            }
        };
        this.bar_ChartData = [];
        this.bar_ChartOptions = {
            title: 'Top Groups',
            width: '70%',
            height: 300,
            colors: ['#009688', '#6eba8c', '#b9f2a1', '#10c4b5', '#005562', '#0e8174', '#639a4b', '#2d6b64'],
            legend: { position: 'none' },
            chartArea: { left: 10, top: 10, right: 10, bottom: 10 },
            animation: {
                duration: 2000,
                easing: 'out',
                startup: true
            }
        };
        this.validator.validateUser("dashboard");
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadMapData();
        this.heatmapLayer['initialized$'].subscribe(function (heatmap) {
            _this.heatmap = heatmap;
            _this.map = _this.heatmap.getMap();
            _this.heatmap.set('radius', _this.heatmap.get('radius') ? null : 20);
        });
        this.loadGroupsToList();
        this.loadBarChartGroupData();
        this.loadPieChartCategoryData();
    };
    DashboardComponent.prototype.openGroupDetails = function () {
        console.log("open ldap");
        this.router.navigate(['./group-management'], { queryParams: { dashview: true } });
    };
    DashboardComponent.prototype.openEventPage = function () {
        this.router.navigate(['./event-management']);
    };
    DashboardComponent.prototype.loadMapData = function () {
        var _this = this;
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventLocationsForMap";
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventLocationsForMap?userId=" + this.validator.getUserId();
        }
        this.commonService.processGet(url).subscribe(function (response) {
            console.log(">>> ", response);
            var res = response.getEventLocationsForMap;
            if (res.responseCode == 1) {
                _this.arrEventModelForMap = res.responseData;
                console.log("response data ", _this.arrEventModelForMap);
                _this.arrEventModelForMap.forEach(function (e) {
                    _this.points.push(new google.maps.LatLng(e.eventLocationLat, e.eventLocationLont));
                });
            }
        }, function (error) {
            console.log("err", error);
        });
    };
    DashboardComponent.prototype.toggleHeatmap = function () {
        this.heatmap.setMap(this.heatmap.getMap() ? null : this.map);
    };
    DashboardComponent.prototype.changeGradient = function () {
        var gradient = [
            'rgba(0, 255, 255, 0)',
            'rgba(0, 255, 255, 1)',
            'rgba(0, 191, 255, 1)',
            'rgba(0, 127, 255, 1)',
            'rgba(0, 63, 255, 1)',
            'rgba(0, 0, 255, 1)',
            'rgba(0, 0, 223, 1)',
            'rgba(0, 0, 191, 1)',
            'rgba(0, 0, 159, 1)',
            'rgba(0, 0, 127, 1)',
            'rgba(63, 0, 91, 1)',
            'rgba(127, 0, 63, 1)',
            'rgba(191, 0, 31, 1)',
            'rgba(255, 0, 0, 1)'
        ];
        this.heatmap.set('gradient', this.heatmap.get('gradient') ? null : gradient);
    };
    DashboardComponent.prototype.changeRadius = function () {
        this.heatmap.set('radius', this.heatmap.get('radius') ? null : 20);
    };
    DashboardComponent.prototype.changeOpacity = function () {
        this.heatmap.set('opacity', this.heatmap.get('opacity') ? null : 0.2);
    };
    DashboardComponent.prototype.loadRandomPoints = function () {
        this.points = [];
        for (var i = 0; i < 9; i++) {
            this.addPoint();
        }
    };
    DashboardComponent.prototype.addPoint = function () {
        var randomLat = Math.random() * 0.0099 + 37.782551;
        var randomLng = Math.random() * 0.0099 + -122.445368;
        var latlng = new google.maps.LatLng(randomLat, randomLng);
        this.points.push(latlng);
    };
    DashboardComponent.prototype.loadGroupsToList = function () {
        var _this = this;
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/getCount";
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/getCount?userId=" + this.validator.getUserId();
        }
        // let url = AppParams.BASE_PATH + "groups/getCount";
        // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getCount";
        this.commonService.processGet(url).subscribe(function (res) {
            var response = res.getCount;
            if (res.responseCode != 999) {
                _this.dashboardItem = response.responseData;
                if (_this.dashboardItem.tot_notification_limit == -1) {
                    _this.dashboardItem.tot_notification_limit = "Unlimited";
                }
            }
        }, function (error) {
            console.log("error ", error);
        });
    };
    DashboardComponent.prototype.loadBarChartGroupData = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/getTopGroups";
        //let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getTopGroups";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            var response = res.getTopGroups;
            if (res.responseCode != 999) {
                _this.chartGroupModel = response.responseData;
            }
            console.log("response api ", _this.chartGroupModel);
            _this.bar_ChartData = [];
        }, function (error) {
            console.log("error ", error);
        });
    };
    DashboardComponent.prototype.loadPieChartCategoryData = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "categories/getTopCategories";
        //let url = "http://192.0.0.59:8080/Majlis-CategoryManagement-1.0/service/categories/getTopCategories";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            var response = res.getTopCategories;
            if (res.responseCode != 999) {
                _this.chartCategoryModel = response.responseData;
            }
            console.log("response api ", _this.chartCategoryModel);
            // this.pie_ChartData = [['Task', 'User count'],
            // [this.chartCategoryModel[0].categoryName, this.chartCategoryModel[0].count],
            // [this.chartCategoryModel[1].categoryName, this.chartCategoryModel[1].count],
            // [this.chartCategoryModel[2].categoryName, this.chartCategoryModel[2].count],
            // [this.chartCategoryModel[3].categoryName, this.chartCategoryModel[3].count],
            // [this.chartCategoryModel[4].categoryName, this.chartCategoryModel[4].count],
            // ];
            _this.chartCategoryModel.forEach(function (model) {
                console.log("model");
                var chartCategoryModelTemp = new __WEBPACK_IMPORTED_MODULE_5__model_ChartCategory__["a" /* ChartCategoryModel */]();
                if (model.id == 1) {
                    model.color = '#009688';
                }
                else if (model.id == 2) {
                    model.color = '#6eba8c';
                }
                else if (model.id == 3) {
                    model.color = '#b9f2a1';
                }
                else if (model.id == 4) {
                    model.color = '#10c4b5';
                }
                else {
                    model.color = '#005562';
                }
            });
        }, function (error) {
            console.log("error ", error);
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__ngui_map__["HeatmapLayer"]), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ngui_map__["HeatmapLayer"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__ngui_map__["HeatmapLayer"]) === 'function' && _a) || Object)
    ], DashboardComponent.prototype, "heatmapLayer", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("showGroupDetails"), 
        __metadata('design:type', Object)
    ], DashboardComponent.prototype, "showGroupDetails", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('chartz'), 
        __metadata('design:type', Object)
    ], DashboardComponent.prototype, "Chartzz", void 0);
    DashboardComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/dashboard.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_4__model_DashboardItems__["a" /* DashBoardItemModel */], __WEBPACK_IMPORTED_MODULE_6__shared_service__["a" /* SharedService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__shared_service__["a" /* SharedService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__shared_service__["a" /* SharedService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_7__angular_router__["c" /* ActivatedRoute */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_7__angular_router__["a" /* Router */]) === 'function' && _f) || Object])
    ], DashboardComponent);
    return DashboardComponent;
    var _a, _b, _c, _d, _e, _f;
}());
//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/eventmangmnt/eventmangmnt.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n/*###############################################*/\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n.event-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:10px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n.modal-body {\r\n    max-height: calc(100vh - 210px);\r\n    overflow-y: auto;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\ntextarea {\r\n    resize: none;\r\n}\r\nvideo{\r\n    cursor: pointer;    \r\n}\r\n.show-video{\r\n    margin-left: 55%;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/eventmangmnt/eventmangmnt.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop>\r\n</app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<div class=\"container\">\r\n  <div class=\"page-title\">\r\n    <h2>Event Management</h2>\r\n  </div>\r\n  <div class=\"pull-right\"><button class=\"btn btn-success\" (click)=\"addNewEvent()\">New Event</button></div><br><br><br>\r\n  <div class=\"panel panel-default\">\r\n\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n\r\n    <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n      <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n        <tr>\r\n          <th></th>\r\n          <th>Caption</th>\r\n          <th>Message</th>\r\n          <th>Location</th>\r\n          <th>Date</th>\r\n          <!-- <th>Time</th> -->\r\n          <th>Category</th>\r\n          <th>Recipients</th>\r\n          <!-- <th>Images</th> -->\r\n          <th style=\"text-align: center\">Info</th>\r\n          <th style=\"text-align: center\">Edit</th>\r\n          <th style=\"text-align: center\">Status</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let a of eventModelList;\">\r\n          <td><img class=\"circle-image pull-left\" src=\"{{a.eventIconPath}}\" /></td>\r\n          <td>\r\n            <div class=\"col-sm-12 event-name pull-left\">\r\n              {{a.eventCaption}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.eventMessage}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.eventLocation}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.eventDate | date : \"fullDate\"}}\r\n            </div>\r\n          </td>\r\n          <!-- <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.eventTime}}\r\n            </div>\r\n          </td> -->\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.eventCategory.categoryTitle}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              {{a.manualCount}}\r\n            </div>\r\n          </td>\r\n          <!-- <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              <button class=\"btn btn-primary\" (click)=\"viewImages.show()\">View Images</button>\r\n            </div>\r\n          </td> -->\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              <button class=\"btn btn-primary \" (click)=\"viewInfomation(a)\">View</button>\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 repo-name pull-left\">\r\n              <button class=\"btn btn-primary\" (click)=\"editEventOpen(a)\">Edit</button>\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <span class=\"glyphicon glyphicon-ok\" *ngIf=\"a.eventStatus.codeId ==15\"></span>\r\n            <span class=\"glyphicon glyphicon-remove\" *ngIf=\"a.eventStatus.codeId ==16\"></span>\r\n          </td>\r\n        </tr>\r\n\r\n      </tbody>\r\n    </table>\r\n  </div>\r\n\r\n\r\n  <!--######################New Event Modal######################-->\r\n  <div bsModal #newEvent=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n        <form #newEventForm=\"ngForm\">\r\n          <!--################ start new report form ################-->\r\n          <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"newEvent.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\">Add Event</h4>\r\n      \r\n            <h4 class=\"modal-title\"></h4>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Caption</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"eventModel.eventCaption\" class=\"form-control\" name=\"eventModel.eventCaption\"\r\n                required>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"comment\">Message</label>\r\n              <textarea class=\"form-control\" rows=\"3\" [(ngModel)]=\"eventModel.eventMessage\" class=\"form-control\" name=\"eventModel.eventMessage\"\r\n                required></textarea>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Address</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"eventModel.eventLocation\" class=\"form-control\" name=\"eventModel.eventLocation\"\r\n                required>\r\n            </div>\r\n\r\n            <label for=\"template\">Location</label>\r\n            <div class=\"row\">\r\n\r\n              <ngui-map (mapClick)=\"onMapClick($event)\" zoom=\"11\" mapTypeId=\"terrain\" style=\"height: 200px;\">\r\n                <marker [position]=\"position\"></marker>\r\n              </ngui-map>\r\n            </div>\r\n            <br/>\r\n            <div class=\"row\">\r\n              <div class=\"form-group col-xs-6\">\r\n                <label class=\"sr-only\">Latitude</label>\r\n                <input readonly=\"true\" class=\"form-control input-group-lg\" type=\"text\" title=\"Enter Latitude\" [(ngModel)]=\"eventModel.eventLocationLat\"\r\n                  class=\"form-control\" name=\"eventModel.eventLocationLat\" placeholder=\"Latitude\" />\r\n              </div>\r\n\r\n              <div class=\"form-group col-xs-6\">\r\n                <label class=\"sr-only\">Longitude</label>\r\n                <input readonly=\"true\" class=\"form-control input-group-lg\" type=\"text\" title=\"Enter Longitude\" [(ngModel)]=\"eventModel.eventLocationLont\"\r\n                  class=\"form-control\" name=\"eventModel.eventLocationLont\" placeholder=\"Longitude\" />\r\n              </div>\r\n\r\n            </div>\r\n\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Date</label>\r\n              <input type=\"date\" class=\"form-control\" [(ngModel)]=\"eventModel.eventDate\" class=\"form-control\" name=\"eventModel.eventDate\"\r\n                required>\r\n              <!--<input type=\"date\" ng-model=\"date\" value=\"{{ date | date: 'yyyy-MM-dd' }}\" />-->\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Time (Format should be HH:MM:AM/PM)</label>\r\n              <input type=\"time\" class=\"form-control\" [(ngModel)]=\"eventModel.eventTime\" class=\"form-control\" name=\"eventModel.eventTime\"\r\n                required>\r\n              <!--<input type=\"date\" ng-model=\"date\" value=\"{{ date | date: 'yyyy-MM-dd' }}\" />-->\r\n            </div>\r\n\r\n\r\n            <!--Category Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Category</label>\r\n              <select [(ngModel)]=\"eventModel.eventCategory.categoryId\" name=\"eventModel.eventCategory.categoryId\" class=\"form-control\">\r\n                <option *ngFor=\"let x of categoryModelList\"  [ngValue]=\"x.categoryId\">{{x.categoryTitle}}</option>\r\n            </select>\r\n            </div>\r\n\r\n            <!--Group Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Group</label>\r\n\r\n              <select [(ngModel)]=\"eventModel.groupId.groupId\" name=\"eventModel.groupId\" class=\"form-control\">\r\n                <option *ngFor=\"let y of groupList\" [ngValue]=\"y.groupId\">{{y.groupTitle}}</option>\r\n            </select>\r\n            </div>\r\n\r\n            <!--Event Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Event Status</label>\r\n              <select [(ngModel)]=\"eventModel.eventStatus.codeId\" name=\"eventModel.eventStatus\" class=\"form-control\">\r\n                <option *ngFor=\"let m of statusList\" [ngValue]=\"m.codeId\">{{m.codeMessage}}</option>\r\n              </select>\r\n            </div>\r\n\r\n            <div class=\"form-group\" style=\"display: none\">\r\n              <label for=\"template\">Group Public Levels</label>\r\n              <select [(ngModel)]=\"eventModel.groupId.groupPublicLevel\" name=\"eventModel.groupId.groupPublicLevel\" class=\"form-control\">\r\n                <option *ngFor=\"let n of levelsList\" [ngValue]=\"n\">{{n.codeMessage}}</option>\r\n              </select>\r\n            </div>\r\n\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Recipients</label>\r\n              <input type=\"number\" class=\"form-control\" [(ngModel)]=\"eventModel.manualCount\" class=\"form-control\" name=\"eventModel.manualCount\"\r\n                required>\r\n            </div>\r\n\r\n            <label class=\"control-label\">Select Banner Image (Support file type - JPG)</label>\r\n            <input type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n\r\n            <table class=\"table\">\r\n              <thead>\r\n\r\n              </thead>\r\n              <tbody>\r\n                <tr *ngFor=\"let item of uploader.queue\" style=\"text-align: right\">\r\n\r\n                  <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                  <td *ngIf=\"uploader.isHTML5\">\r\n                    <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                      <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"text-center\">\r\n                    <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                    <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                    <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                  </td>\r\n                  <td nowrap>\r\n                    <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                    <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n                    <button type=\"button\" class=\"btn btn-danger btn-xs\" (click)=\"item.remove()\">\r\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                        </button>\r\n                  </td>\r\n\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div style=\"display: none\">\r\n              <label class=\"control-label\">Select Event Images</label>\r\n              <input id=\"input-1\" type=\"file\" class=\"file\" required><br>\r\n\r\n              <label class=\"control-label\">Select Event Video</label>\r\n              <input id=\"input-1\" type=\"file\" class=\"file\" required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"newEvent.hide()\">Close</button>\r\n            <button type=\"submit\" *ngIf=\"mode=='edit'\" (click)=saveChangesEvent() class=\"btn btn-success\">&nbsp;&nbsp;Save Changes&nbsp;&nbsp;</button>\r\n\r\n            <button type=\"submit\" *ngIf=\"mode=='create'\" (click)=createEvent() class=\"btn btn-success\">&nbsp;&nbsp;Create&nbsp;&nbsp;</button>\r\n          </div>\r\n        </form>\r\n        <!--################ end new report form ################-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div bsModal #editEventModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n        <form #newEventForm=\"ngForm\">\r\n          <!--################ start new report form ################-->\r\n          <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"editEventModal.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\">Edit Event</h4>\r\n\r\n            <h4 class=\"modal-title\"></h4>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Caption</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"eventEdit.eventCaption\" class=\"form-control\" name=\"eventModel.eventCaption\"\r\n                required>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"comment\">Message</label>\r\n              <textarea class=\"form-control\" rows=\"3\" [(ngModel)]=\"eventEdit.eventMessage\" class=\"form-control\" name=\"eventModel.eventMessage\"\r\n                required></textarea>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Address</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"eventEdit.eventLocation\" class=\"form-control\" name=\"eventModel.eventLocation\"\r\n                required>\r\n            </div>\r\n\r\n            <label for=\"template\">Location</label>\r\n\r\n            <div class=\"row\">\r\n\r\n              <ngui-map (mapClick)=\"onMapClickEdit($event)\" zoom=\"11\" [center]=\"position\" mapTypeId=\"terrain\" style=\"height: 200px;\">\r\n                <marker [position]=\"position\"></marker>\r\n              </ngui-map>\r\n\r\n            </div>\r\n            <br/>\r\n            <div class=\"row\">\r\n\r\n              <div class=\"form-group col-xs-6\">\r\n                <label class=\"sr-only\">Latitude</label>\r\n                <input readonly=\"true\" class=\"form-control input-group-lg\" type=\"text\" title=\"Enter Latitude\" [(ngModel)]=\"eventEdit.eventLocationLat\"\r\n                  class=\"form-control\" name=\"eventModel.eventLocationLat\" placeholder=\"Latitude\" />\r\n              </div>\r\n              <div class=\"form-group col-xs-6\">\r\n                <label class=\"sr-only\">Longitude</label>\r\n                <input readonly=\"true\" class=\"form-control input-group-lg\" type=\"text\" title=\"Enter Longitude\" [(ngModel)]=\"eventEdit.eventLocationLont\"\r\n                  class=\"form-control\" name=\"eventModel.eventLocationLont\" placeholder=\"Longitude\" />\r\n              </div>\r\n\r\n            </div>\r\n\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Date</label>\r\n              <input type=\"date\" class=\"form-control\" [(ngModel)]=\"eventEdit.eventDate\" class=\"form-control\" name=\"eventModel.eventDate\"\r\n                required>\r\n              <!--<input type=\"date\" ng-model=\"date\" value=\"{{ date | date: 'yyyy-MM-dd' }}\" />-->\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Time (Format should be HH:MM:AM/PM)</label>\r\n              <input type=\"time\" class=\"form-control\" [(ngModel)]=\"eventEdit.eventTime\" class=\"form-control\" name=\"eventModel.eventTime\"\r\n                required>\r\n              <!--<input type=\"date\" ng-model=\"date\" value=\"{{ date | date: 'yyyy-MM-dd' }}\" />-->\r\n            </div>\r\n\r\n\r\n            <!--Category Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Category</label>\r\n              <select [(ngModel)]=\"eventEdit.eventCategory.categoryId\" name=\"eventModel.eventCategory.categoryId\" class=\"form-control\">\r\n                <option *ngFor=\"let x of categoryModelList\"  [ngValue]=\"x.categoryId\">{{x.categoryTitle}}</option>\r\n            </select>\r\n            </div>\r\n\r\n            <!--Group Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Group</label>\r\n\r\n              <select [(ngModel)]=\"eventEdit.groupId.groupId\" name=\"eventModel.groupId\" class=\"form-control\">\r\n                <option *ngFor=\"let y of groupList\" [ngValue]=\"y.groupId\">{{y.groupTitle}}</option>\r\n            </select>\r\n            </div>\r\n\r\n            <!--Event Drop Down List-->\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Event Status</label>\r\n              <select [(ngModel)]=\"eventEdit.eventStatus.codeId\" name=\"eventModel.eventStatus\" class=\"form-control\">\r\n                <option *ngFor=\"let m of statusList\" [ngValue]=\"m.codeId\">{{m.codeMessage}}</option>\r\n              </select>\r\n            </div>\r\n\r\n\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Recipients</label>\r\n              <input type=\"number\" class=\"form-control\" [(ngModel)]=\"eventEdit.manualCount\" class=\"form-control\" name=\"eventModel.manualCount\"\r\n                required>\r\n            </div>\r\n\r\n            <label class=\"control-label\">Select Banner Image (Support file type - JPG)</label>\r\n            <input type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n\r\n            <table class=\"table\">\r\n              <thead>\r\n\r\n              </thead>\r\n              <tbody>\r\n                <tr *ngFor=\"let item of uploader.queue\" style=\"text-align: right\">\r\n\r\n                  <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                  <td *ngIf=\"uploader.isHTML5\">\r\n                    <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                      <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                    </div>\r\n                  </td>\r\n                  <td class=\"text-center\">\r\n                    <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                    <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                    <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                  </td>\r\n                  <td nowrap>\r\n                    <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                    <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n                    <button type=\"button\" class=\"btn btn-danger btn-xs\" (click)=\"item.remove()\">\r\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                        </button>\r\n                  </td>\r\n\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div style=\"display: none\">\r\n              <label class=\"control-label\">Select Event Images</label>\r\n              <input id=\"input-1\" type=\"file\" class=\"file\" required><br>\r\n\r\n              <label class=\"control-label\">Select Event Video</label>\r\n              <input id=\"input-1\" type=\"file\" class=\"file\" required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"editOpertionProcess\" style=\"width: 25px\" /> </span>\r\n            <button type=\"submit\" *ngIf=\"mode=='edit'\" [disabled]=\"editOpertionProcess\" (click)=saveChangesEvent() class=\"btn btn-success\">&nbsp;&nbsp;Save Changes&nbsp;&nbsp;</button>\r\n\r\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"editEventModal.hide()\">Close</button>\r\n\r\n          </div>\r\n        </form>\r\n        <!--################ end new report form ################-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <!--###################### View Images Modal######################-->\r\n  <div bsModal #viewImages=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\">\r\n\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"viewImages.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Event Images</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-3 col-md-4 col-xs-6 thumb\" *ngFor=\"let a of eventModelList;\">\r\n              <a class=\"thumbnail\" href=\"#\">\r\n                    <img class=\"img-responsive\" src={{a.eventIconPath}} alt=\"\">\r\n                </a>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"viewImages.hide()\">Close</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n  <div bsModal #viewInfo=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\">\r\n\r\n      <div class=\"modal-content\" style=\"background-image: \">\r\n        <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n\r\n\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"viewInfo.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n\r\n          <h4 class=\"modal-title\">About the Event</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n\r\n\r\n          <tabset>\r\n            <tab>\r\n              <span *tabHeading> \r\n                <span style=\"color: #02685E\">Event Information </span>\r\n              </span>\r\n              <div style=\"font-family: 'Josefin Sans', sans-serif;\">\r\n                <span class=\"pull-right\" style=\"color: #02685E\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCategory.categoryTitle}} </span>\r\n\r\n                <h3 style=\"text-align: center\">{{eventEdit.eventCaption}}</h3>\r\n\r\n                <h4 style=\"text-align: center\">{{eventEdit.eventMessage}}</h4>\r\n                <ngui-map (mapClick)=\"onMapClick($event)\" zoom=\"11\" [center]=\"position\" mapTypeId=\"terrain\" style=\"height: 200px;\">\r\n                  <marker [position]=\"position\"></marker>\r\n                </ngui-map>\r\n\r\n                <h3 style=\"text-align: center\">At {{eventEdit.eventLocation}}</h3>\r\n\r\n                <h4 style=\"text-align: center\">On {{eventEdit.eventDate}} | {{eventEdit.eventTime}}</h4>\r\n\r\n                <h3 style=\"text-align: center\"> {{eventEdit.manualCount}} Participants</h3>\r\n\r\n                <!-- <div class=\"center-block\" style=\"border-radius: 5%; width: 75%;height: 200px; background-image: url(http://192.168.1.135:8080/Majlis_images/events/804405.jpg)\"> -->\r\n\r\n                <!-- </div> -->\r\n\r\n              </div>\r\n            </tab>\r\n            <tab>\r\n              <span *tabHeading>\r\n              <span style=\"color: #02685E\">  Event Comments </span>\r\n              </span>\r\n              <div style=\"font-family: 'Josefin Sans', sans-serif;\">\r\n                <span class=\"pull-right\" style=\"color: #02685E\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCategory.categoryTitle}} </span>\r\n                <span class=\"pull-right\" style=\"color: #02685E;margin-right: 10px\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCaption}} </span>\r\n                <span><img class=\"pull-right\" *ngIf=\"commentsLoader\" src=\"assets/inlineLoader.gif\" style=\"width:15px;margin-right: 10px;margin-top: 2px\" /> </span>\r\n                <br/><br/>\r\n\r\n                <div class=\"row\" *ngFor=\"let c of commentList\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"col-md-1\">\r\n                      <div class=\"row\" style=\"text-align: center;margin-bottom: 5px\">\r\n                        <i class=\"fa fa-heart\" aria-hidden=\"true\"></i>\r\n                      </div>\r\n                      <div class=\"row\" style=\"text-align: center\">\r\n                        {{c.commentLike}}\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"col-md-10\">\r\n                      <div class=\"row\">\r\n                        <h4 style=\"margin-top: 0\">{{c.userInserted}}</h4>\r\n                      </div>\r\n                      <div class=\"row\">\r\n                        <h5 style=\"margin-top: 0\">{{c.commentMessage}}</h5>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-1\">\r\n                      <div class=\"row\" style=\"text-align: center;margin-top: 18%;\">\r\n                        <i style=\"font-size: 20px\" *ngIf=\"c.commentId!=deletedCommentId\" class=\"btn fa fa-trash-o\" aria-hidden=\"true\" (click)=\"deleteComment(c.commentEventId,c.commentId)\"></i>\r\n                        <i style=\"font-size: 15px\" class=\"fa fa-check\" *ngIf=\"c.commentEventId==deleteEventId && c.commentId==deletedCommentId\" aria-hidden=\"true\"></i>\r\n\r\n                      </div>\r\n                    </div>\r\n\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n\r\n            </tab>\r\n            <tab>\r\n              <span *tabHeading>\r\n              <span style=\"color: #02685E\"> Event Photos </span>\r\n              </span>\r\n              <div style=\"font-family: 'Josefin Sans', sans-serif;\">\r\n                <span class=\"pull-right\" style=\"color: #02685E\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCategory.categoryTitle}} </span>\r\n                <span class=\"pull-right\" style=\"color: #02685E;margin-right: 10px\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCaption}} </span>\r\n                <span><img class=\"pull-right\" *ngIf=\"eventImageLoader\" src=\"assets/inlineLoader.gif\" style=\"width:15px;margin-right: 10px;margin-top: 2px\" /> </span>\r\n                <br/><br/>\r\n\r\n                <div class=\"col-md-12\">\r\n\r\n                  <div class=\"col-md-3\" *ngFor=\"let m of imgArray\" style=\"cursor: pointer\">\r\n                    <img class=\"img-thumbnail\" [src]=\"m.eventImagePath\" (click)=\"openImage(m.eventId.eventId,m.eventImageId,m.eventImagePath)\"\r\n                    />\r\n                  </div>\r\n\r\n                </div>\r\n\r\n              </div>\r\n\r\n\r\n            </tab>\r\n            <tab>\r\n              <span *tabHeading>\r\n               <span style=\"color: #02685E\">  Event Videos </span>\r\n              </span>\r\n\r\n              <div style=\"font-family: 'Josefin Sans', sans-serif;\">\r\n                <span class=\"pull-right\" style=\"color: #02685E\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCategory.categoryTitle}} </span>\r\n                <span class=\"pull-right\" style=\"color: #02685E;margin-right: 10px\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCaption}} </span>\r\n                <span><img class=\"pull-right\" *ngIf=\"eventVideoLoader\" src=\"assets/inlineLoader.gif\" style=\"width:15px;margin-right: 10px;margin-top: 2px\" /> </span>\r\n                <br/><br/>\r\n\r\n                <div class=\"col-md-12\">\r\n\r\n                  <div class=\"col-md-6\" style=\"cursor: pointer\" *ngFor=\"let m of videoList\">\r\n                    <button type=\"button\" class=\"close pull-right\" (click)=\"deleteVideo(m.eventId,m.videoId)\" aria-label=\"Close\">\r\n                      <span style=\"color: #000\" aria-hidden=\"true\">&times;</span>\r\n                    </button>\r\n                    <video controls class=\"img-thumbnail\">\r\n                      <source [src]=\"m.videoFilePath\" type=\"video/mp4\">\r\n                    </video>\r\n\r\n                  </div>\r\n\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </tab>\r\n            <!-- <tab>\r\n              <span *tabHeading>\r\n              <span style=\"color: #02685E\">  Event Participants </span>\r\n              </span>\r\n              <div style=\"font-family: 'Josefin Sans', sans-serif;\">\r\n                <span class=\"pull-right\" style=\"color: #02685E\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCategory.categoryTitle}} </span>\r\n                <span class=\"pull-right\" style=\"color: #02685E;margin-right: 10px\"> <i class=\"fa fa-hashtag\" aria-hidden=\"true\"></i> {{eventEdit.eventCaption}} </span>\r\n                <span><img class=\"pull-right\" *ngIf=\"participantsLoader\" src=\"assets/inlineLoader.gif\" style=\"width:15px;margin-right: 10px;margin-top: 2px\" /> </span>\r\n                <br/><br/>\r\n\r\n                <div class=\"row\" *ngFor=\"let c of eventParticipantsList\">\r\n                  <div class=\"col-md-12\">\r\n                    <div class=\"col-md-1\">\r\n                      <div class=\"row\" style=\"text-align: center;margin-bottom: 5px\">\r\n                        <i style=\"font-size: 20px\" class=\"fa fa-user\" aria-hidden=\"true\"></i>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"col-md-6\">\r\n                      <div class=\"row\">\r\n                        <h4 style=\"margin-top: 0\">{{(c.mobileUser == null)? \"-\" : c.mobileUser }}</h4>\r\n                      </div>\r\n                      <div class=\"row\">\r\n                        <h5 style=\"margin-top: 0\">{{c.mobileUserNo}}</h5>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"col-md-5\">\r\n                      <button [ngClass]=\"{'btn btn-default ' : true,'active' : ((c.mobileUserId == statusUserId) &&  (statusAction == 'attend') )}\" (click)=\"changeUserEventStatus($event,c.mobileUserId,'attend')\" >Attending</button>\r\n                      <button [ngClass]=\"{'btn btn-default ' : true,'active' : ((c.mobileUserId == statusUserId) &&  (statusAction == 'notattend'))}\" (click)=\"changeUserEventStatus($event,c.mobileUserId,'notattend')\" >Not Attending</button>\r\n                      <button [ngClass]=\"{'btn btn-default ' : true,'active' : ((c.mobileUserId == statusUserId) &&  (statusAction == 'maybe'))}\" (click)=\"changeUserEventStatus($event,c.mobileUserId,'maybe')\" >Maybe</button>\r\n                   \r\n                    </div>\r\n\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n\r\n            </tab> -->\r\n          </tabset>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"viewInfo.hide()\">Close</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div bsModal #showImages=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\">\r\n\r\n      <div class=\"modal-content\" style=\"background-color: #02685E;color: azure\">\r\n        <div class=\"modal-header\" style=\"border: 0\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"showImages.hide()\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n          <h4 class=\"modal-title\"></h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <img [src]=\"selectedImageUrl\" class=\"img-responsive\" />\r\n        </div>\r\n\r\n        <div class=\"modal-footer\" style=\"border: 0\">\r\n          <img *ngIf=\"eventImageLoader\" src=\"assets/inlineLoader.gif\" style=\"width:20px;\" />\r\n          <button type=\"button\" class=\"btn btn-info\" style=\"background: #02685E\" (click)=\"deleteSelectedImage()\">Delete</button>\r\n\r\n          <button type=\"button\" class=\"btn btn-info\" style=\"background: #02685E\" (click)=\"showImages.hide()\">Close</button>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <div class=\"modal-header\" style=\"background-color: #449D44; color: azure\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Operation Completed</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <p>{{successMessage}}</p>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div bsModal #fail=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <div class=\"modal-header\" style=\"background-color: chocolate;color: azure\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"fail.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Operation Failed!</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <p>{{failMessage}}</p>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"fail.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div bsModal #confirmation=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <div class=\"modal-header\" style=\"background-color: #02685E; color: azure\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"sendNotification.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Delete Operation</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <p>Are you sure that you want to delete this {{deleteType}}?</p>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"confirmLoader\" style=\"width: 25px\" /> </span>\r\n\r\n          <button type=\"button\" class=\"btn btn-success\" style=\"background-color: #02685E; color: azure\" [disabled]=\"confirmLoader\"\r\n            (click)=\"confirmDelete()\">Sure</button>\r\n\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"confirmation.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/eventmangmnt/eventmangmnt.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_Event__ = __webpack_require__("../../../../../src/app/model/Event.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_Category__ = __webpack_require__("../../../../../src/app/model/Category.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__model_Comment__ = __webpack_require__("../../../../../src/app/model/Comment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_bootstrap__ = __webpack_require__("../../../../ng2-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__model_DeleteEventData__ = __webpack_require__("../../../../../src/app/model/DeleteEventData.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__model_EventImages__ = __webpack_require__("../../../../../src/app/model/EventImages.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__model_EventParticipants__ = __webpack_require__("../../../../../src/app/model/EventParticipants.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventmangmntComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var EventmangmntComponent = (function () {
    function EventmangmntComponent(commonService, eventModel, categoryModel, groupModel, validator) {
        this.commonService = commonService;
        this.eventModel = eventModel;
        this.categoryModel = categoryModel;
        this.groupModel = groupModel;
        this.validator = validator;
        this.participantsLoader = false;
        this.imgArray = new Array();
        this.fakeArray = new Array(12);
        this.fakeArrayImages = new Array(20);
        this.commentList = new Array();
        this.mode = "create";
        this.editOpertionProcess = false;
        this.eventModelList = new Array(); // Create an event model list
        this.eventModelObj = new __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]();
        this.imageList = new Array();
        this.uploader = new __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.categoryModelObj = new __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */]();
        this.categoryModelList = new Array();
        this.statusList = new Array();
        this.levelsList = new Array();
        this.groupList = new Array();
        this.eventEdit = new __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]();
        this.mdCode = new __WEBPACK_IMPORTED_MODULE_4__model_MDCode__["a" /* MDCodeModel */]();
        this.isSuccess = false;
        this.showLoader = true;
        this.successMessage = "";
        this.validator.validateUser("event-management");
    }
    EventmangmntComponent.prototype.toggleVideo = function (event) {
        this.videoplayer.nativeElement.play();
    };
    EventmangmntComponent.prototype.changeUserEventStatus = function ($event, userId, action) {
        console.log(">>>>>>> ", $event);
        this.statusUserId = userId;
        this.statusAction = action;
    };
    EventmangmntComponent.prototype.viewInfomation = function (m) {
        this.position = new google.maps.LatLng(m.eventLocationLat, m.eventLocationLont);
        this.eventEdit = new __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]();
        this.eventEdit = m;
        this.viewInfo.show();
        this.loadEventComments();
        this.loadEventImages();
        this.loadEventVideo();
        this.loadEventParticipants();
    };
    EventmangmntComponent.prototype.editEventOpen = function (event) {
        var _this = this;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.eventModel.eventIconPath = responsePath.responseData;
        };
        this.mode = 'edit';
        this.editEventModal.show();
        this.eventEdit = new __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]();
        this.eventEdit = event;
        console.log("event id ", this.eventEdit);
        this.position = new google.maps.LatLng(this.eventEdit.eventLocationLat, this.eventEdit.eventLocationLont);
    };
    EventmangmntComponent.prototype.loadEventParticipants = function () {
        var _this = this;
        console.log(">>> getEventParticipants");
        this.participantsLoader = true;
        this.eventParticipantsList = new Array();
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventParticipants?eventId=" + this.eventEdit.eventId;
        this.commonService.processGet(url).subscribe(function (response) {
            var res = response.getEventParticipants;
            if (res.responseCode == 1) {
                _this.eventParticipantsList = res.responseData;
            }
            _this.participantsLoader = false;
        }, function (error) {
            _this.participantsLoader = false;
        });
    };
    EventmangmntComponent.prototype.deleteVideo = function (eventId, videoId) {
        this.deleteType = "video";
        this.deleteObjId = videoId;
        this.deleteEventId = this.eventEdit.eventId;
        this.confirmation.show();
    };
    EventmangmntComponent.prototype.loadEventVideo = function () {
        var _this = this;
        this.videoList = new Array();
        this.eventVideoLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventVideos?eventId=" + this.eventEdit.eventId;
        this.commonService.processGet(url).subscribe(function (response) {
            var res = response.getEventVideos;
            if (res.responseCode == 1) {
                _this.videoList = res.responseData;
            }
            else {
            }
            _this.eventVideoLoader = false;
        }, function (error) {
            _this.eventVideoLoader = false;
        });
    };
    EventmangmntComponent.prototype.loadEventComments = function () {
        var _this = this;
        this.commentList = [];
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventComments?eventId=" + this.eventEdit.eventId;
        this.commentsLoader = true;
        this.commonService.processGet(url).subscribe(function (response) {
            console.log("esponse ", response);
            var res = response.getEventComments;
            if (res.responseCode == 1) {
                _this.commentList = res.responseData;
                console.log("coomments ", _this.commentList);
            }
            else {
            }
            _this.commentsLoader = false;
        }, function (error) {
            _this.commentsLoader = false;
        });
    };
    EventmangmntComponent.prototype.deleteSelectedImage = function () {
        this.deleteType = "image";
        this.deleteObjId = this.selectedImageId;
        this.deleteEventId = this.eventEdit.eventId;
        this.confirmation.show();
    };
    EventmangmntComponent.prototype.openImage = function (eventId, imgId, path) {
        this.selectedImageId = imgId;
        this.selectedImageUrl = path;
        this.showImages.show();
    };
    EventmangmntComponent.prototype.loadEventImages = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventImagesCMS?eventId=" + this.eventEdit.eventId;
        this.eventImageLoader = true;
        this.commonService.processGet(url).subscribe(function (response) {
            _this.imgArray = [];
            var res = response.getEventImagesCMS;
            if (res.responseCode == 1) {
                _this.imgArray = res.responseData;
                console.log("console ", _this.imgArray);
            }
            else {
            }
            _this.eventImageLoader = false;
        }, function (error) {
            _this.eventImageLoader = false;
        });
    };
    EventmangmntComponent.prototype.deleteComment = function (eventId, commentId) {
        this.deleteType = "comment";
        this.deleteObjId = commentId;
        this.deleteEventId = eventId;
        this.confirmation.show();
    };
    EventmangmntComponent.prototype.confirmDelete = function () {
        var _this = this;
        var me = this;
        this.confirmation.hide();
        this.deleteEventData = new __WEBPACK_IMPORTED_MODULE_12__model_DeleteEventData__["a" /* DeleteEventDataModel */]();
        this.deleteEventData.eventId = this.deleteEventId;
        this.deleteEventData.deletedObjId = this.deleteObjId;
        this.commentsLoader = true;
        if (this.deleteType == "comment") {
            this.deletedCommentId = this.deleteObjId;
            this.deleteEventData.operaton = this.deleteType;
            var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/deleteEventData";
            this.commonService.processPost(url, this.deleteEventData).subscribe(function (response) {
                var res = response.deleteEventData;
                _this.commentsLoader = false;
                if (res.responseCode == 1) {
                    _this.deletedCommentId = _this.deleteObjId;
                    _this.loadEventComments();
                }
                else {
                    _this.failMessage = "The comment cannot delete at this time. Try again!";
                    _this.fail.show();
                }
                _this.deleteEventData.deletedObjId = _this.deleteObjId;
            }, function (error) {
                _this.commentsLoader = false;
                _this.failMessage = "Cannot connect to the server. Try again!";
                _this.fail.show();
            });
        }
        else if (this.deleteType == "image") {
            this.deleteEventData.operaton = this.deleteType;
            var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/deleteEventData";
            this.eventImageLoader = true;
            this.commonService.processPost(url, this.deleteEventData).subscribe(function (response) {
                var res = response.deleteEventData;
                _this.eventImageLoader = false;
                if (res.responseCode == 1) {
                    _this.showImages.hide();
                    _this.loadEventImages();
                }
                else {
                    _this.failMessage = "The image cannot delete at this time. Try again!";
                    _this.fail.show();
                }
                _this.deleteEventData.deletedObjId = _this.deleteObjId;
            }, function (error) {
                _this.eventImageLoader = false;
                _this.failMessage = "Cannot connect to the server. Try again!";
                _this.fail.show();
            });
        }
        else if (this.deleteType == "video") {
            this.deleteEventData.operaton = this.deleteType;
            this.eventVideoLoader = true;
            var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/deleteEventData";
            this.commonService.processPost(url, this.deleteEventData).subscribe(function (response) {
                var res = response.deleteEventData;
                _this.eventVideoLoader = false;
                if (res.responseCode == 1) {
                    _this.loadEventVideo();
                }
                else {
                    _this.failMessage = "The image cannot delete at this time. Try again!";
                    _this.fail.show();
                }
                _this.deleteEventData.deletedObjId = _this.deleteObjId;
            }, function (error) {
                _this.eventVideoLoader = false;
                _this.failMessage = "Cannot connect to the server. Try again!";
                _this.fail.show();
            });
        }
    };
    EventmangmntComponent.prototype.saveChangesEvent = function () {
        var _this = this;
        console.log("save chnages ", this.eventEdit);
        this.eventEdit.eventIconPath = this.eventModel.eventIconPath;
        this.editOpertionProcess = true;
        var header = new __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_10__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        option.headers = header;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/updateEventDetails";
        this.commonService.processPostWithHeaders(url, this.eventEdit, option).subscribe(function (response) {
            console.log("response ", response);
            var res = response.updateEventDetails;
            if (res.responseCode == 1) {
                _this.editEventModal.hide();
                _this.successMessage = "Event has been updated successfully!";
                _this.success.show();
                _this.loadEventsToGrid(0, 5000);
            }
            else {
                _this.failMessage = "The event cannot be updated at this time. Please try again later!";
                _this.fail.show();
            }
            _this.editOpertionProcess = false;
        }, function (error) {
            _this.failMessage = "Cannot connect to the server.";
            _this.fail.show();
            _this.editOpertionProcess = false;
        });
    };
    EventmangmntComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadEventsToGrid(0, 100);
        this.loadCategories();
        this.loadGroups();
        this.loadStatus("EVENTS");
        this.loadGroupPublicLevels("GROUP_LEVELS");
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.eventModel.eventIconPath = responsePath.responseData;
        };
    };
    EventmangmntComponent.prototype.submitForm = function (form) {
        console.log('Form Data: ');
        console.log(form);
        console.log();
    };
    EventmangmntComponent.prototype.loadEventsToGrid = function (start, limit) {
        //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getEventList?start=" + start + "&limit=" + limit;
        var _this = this;
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventsList?start=" + start + "&limit=" + limit;
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventsList?userId=" + this.validator.getUserId() + "&start=" + start + "&limit=" + limit;
        }
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.eventModelList = res.getEventsList.responseData;
            _this.showLoader = false;
            console.log("event model list", _this.eventModelList);
        }, function (error) {
            console.log("error ", error);
        });
    };
    EventmangmntComponent.prototype.addNewEvent = function () {
        var _this = this;
        this.position = new google.maps.LatLng(null, null);
        ;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-EventManagement_merge-1.0/service/events/saveEventIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.eventModel.eventIconPath = responsePath.responseData;
        };
        this.mode = 'create';
        this.eventModel = new __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]();
        this.newEvent.show();
    };
    EventmangmntComponent.prototype.createEvent = function () {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_10__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/createEvent"
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/createEvent";
        console.log("date parsing...", this.eventModel.eventDate);
        console.log("event time", this.eventModel.eventTime);
        var time = this.eventModel.eventTime;
        this.eventModel.groupId.groupCountry = null;
        this.commonService.processPost(url, this.eventModel).subscribe(function (res) {
            _this.isSuccess = true;
            var response = res.createEvent;
            if (response.responseCode == 1) {
                _this.successMessage = "Event has created!";
                _this.loadEventsToGrid(0, 1000);
                _this.showLoader = false;
                _this.resetFields(_this.eventModel);
                _this.newEvent.hide();
                _this.successMessage = "The Event has been created";
                _this.success.show();
            }
            else {
                _this.failMessage = "Cannot connect to the server";
                _this.fail.show();
            }
        }, function (error) {
            _this.failMessage = "Cannot connect to the server";
            _this.fail.show();
        });
    };
    EventmangmntComponent.prototype.resetFields = function (mod) {
        this.showLoader = true;
        this.isSuccess = false;
        mod.eventCaption = "";
        mod.eventMessage = "";
        mod.eventLocation = "";
        mod.eventLocationLat = "";
        mod.eventLocationLont = "";
        mod.manualCount = "";
    };
    EventmangmntComponent.prototype.loadCategories = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "category/getCategoriesCMS";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("category list ", res);
            _this.categoryModelList = res.getCategoriesCMS.responseData;
        }, function (error) {
            console.log("error ", error);
        });
    };
    EventmangmntComponent.prototype.loadGroups = function () {
        var _this = this;
        //  let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getGroupList"
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupList";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.groupList = res.getGroupList.responseData;
            console.log("response api ", _this.groupList);
        }, function (error) {
            console.log("error ", error);
        });
    };
    EventmangmntComponent.prototype.loadStatus = function (type) {
        // let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getStatus?type="+type
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getStatus?type=" + type;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.statusList = res.getStatus.responseData;
        }, function (error) {
            console.log("error ", error);
        });
    };
    EventmangmntComponent.prototype.loadGroupPublicLevels = function (subType) {
        //  let url = "http://192.0.0.69:9090/Majlis-EventManagement-1.0/service/events/getPublicLevels?subType="+subType
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_8__app_module__["b" /* AppParams */].BASE_PATH + "events/getPublicLevels?subType=" + subType;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.levelsList = res.getPublicLevels.responseData;
        }, function (error) {
            console.log("error ", error);
        });
    };
    // onMapReady(map) {
    //   console.log('map', map);
    //   console.log('markers', map.markers);  // to get all markers as an array 
    // }
    EventmangmntComponent.prototype.onIdle = function (event) {
        console.log('map', event.target);
    };
    EventmangmntComponent.prototype.onMarkerInit = function (marker) {
        console.log('marker', marker);
    };
    EventmangmntComponent.prototype.onMapClick = function (event) {
        this.eventModel.eventLocationLat = event.latLng.lat();
        this.eventModel.eventLocationLont = event.latLng.lng();
        this.position = event.latLng;
        event.target.panTo(event.latLng);
    };
    EventmangmntComponent.prototype.onMapClickEdit = function (event) {
        this.eventEdit.eventLocationLat = event.latLng.lat();
        this.eventEdit.eventLocationLont = event.latLng.lng();
        this.position = event.latLng;
        event.target.panTo(event.latLng);
    };
    EventmangmntComponent.prototype.function = function () {
        console.log("######### show video #########");
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('videoPlayer'), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "videoplayer", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("newEvent"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "newEvent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("viewInfo"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "viewInfo", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "success", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("confirmation"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "confirmation", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("showImages"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "showImages", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fail"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "fail", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("editEventModal"), 
        __metadata('design:type', Object)
    ], EventmangmntComponent.prototype, "editEventModal", void 0);
    EventmangmntComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-eventmangmnt',
            template: __webpack_require__("../../../../../src/app/eventmangmnt/eventmangmnt.component.html"),
            styles: [__webpack_require__("../../../../../src/app/eventmangmnt/eventmangmnt.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */], __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */], __WEBPACK_IMPORTED_MODULE_5__model_Group__["a" /* GroupModel */], __WEBPACK_IMPORTED_MODULE_14__model_EventParticipants__["a" /* EventParticipantsModel */], __WEBPACK_IMPORTED_MODULE_11_ng2_bootstrap__["e" /* TabsetConfig */], __WEBPACK_IMPORTED_MODULE_6__model_Comment__["a" /* CommentModel */], __WEBPACK_IMPORTED_MODULE_13__model_EventImages__["a" /* EventImagesModel */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__model_Event__["a" /* EventModel */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__model_Category__["a" /* CategoryModel */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__model_Group__["a" /* GroupModel */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__model_Group__["a" /* GroupModel */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_9__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_9__service_login_validator__["a" /* LoignValidator */]) === 'function' && _e) || Object])
    ], EventmangmntComponent);
    return EventmangmntComponent;
    var _a, _b, _c, _d, _e;
}());
//# sourceMappingURL=eventmangmnt.component.js.map

/***/ }),

/***/ "../../../../../src/app/google-chart/google-chart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleChartComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GoogleChartComponent = (function () {
    function GoogleChartComponent(element) {
        this.element = element;
        this._element = this.element.nativeElement;
    }
    GoogleChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            google.charts.load('current', { 'packages': ['corechart'] });
            setTimeout(function () {
                _this.drawGraph(_this.chartOptions, _this.chartType, _this.chartData, _this._element);
            }, 1000);
        }, 1000);
    };
    GoogleChartComponent.prototype.drawGraph = function (chartOptions, chartType, chartData, ele) {
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var wrapper;
            wrapper = new google.visualization.ChartWrapper({
                chartType: chartType,
                dataTable: chartData,
                options: chartOptions || {},
                containerId: ele.id
            });
            wrapper.draw();
        }
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('chartType'), 
        __metadata('design:type', String)
    ], GoogleChartComponent.prototype, "chartType", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('chartOptions'), 
        __metadata('design:type', Object)
    ], GoogleChartComponent.prototype, "chartOptions", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('chartData'), 
        __metadata('design:type', Object)
    ], GoogleChartComponent.prototype, "chartData", void 0);
    GoogleChartComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[GoogleChart]'
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === 'function' && _a) || Object])
    ], GoogleChartComponent);
    return GoogleChartComponent;
    var _a;
}());
//# sourceMappingURL=google-chart.component.js.map

/***/ }),

/***/ "../../../../../src/app/grpmangmnt/grpmangmnt.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n.group-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.group-desc{\r\n    color: #6f7982;\r\n}\r\n\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/grpmangmnt/grpmangmnt.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop>\r\n</app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<div class=\"container\">\r\n  <div class=\"page-title\">\r\n    <h2>Group Management\r\n      <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"userAcceptLoader\" style=\"width: 25px\" /> </span>\r\n    </h2>\r\n  </div>\r\n  <div class=\"pull-right\"><button class=\"btn btn-success\" *ngIf=\"!dashview\" (click)=\"openNewGroup()\">New Group</button></div><br><br><br>\r\n  <div class=\"panel panel-default\">\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n    <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n      <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n        <tr>\r\n          <th></th>\r\n          <th>Group</th>\r\n          <!-- <th>Location</th> -->\r\n          <th>Public Level</th>\r\n          <th>Group Requests</th>\r\n          <th>View</th>\r\n          <th>Status</th>\r\n          <th *ngIf=\"!dashview\"></th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let a of groupModel;\">\r\n          <td>\r\n            <img class=\"circle-image pull-left\" [src]=\"a.groupIconPath\" />\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 group-name\">\r\n              {{a.groupTitle}}\r\n            </div>\r\n            <div class=\"col-sm-12 group-desc\">\r\n              {{a.groupDescription}}\r\n            </div>\r\n          </td>\r\n         \r\n          <td>\r\n            {{a.groupPublicLevel.codeMessage}}\r\n          </td>\r\n          <td>\r\n            <button [disabled]=\"a.groupPublicLevel.codeId!=5\" class=\"btn btn-primary\" (click)=\"getGroupAcceptData(a.groupId)\">\r\n            Accept\r\n          </button>\r\n          </td>\r\n          <td>\r\n            <button class=\"btn btn-primary\" (click)=\"viewGroupDetail(a.groupId)\">\r\n            View\r\n          </button>\r\n          </td>\r\n          <td>\r\n            <span *ngIf=\"a.groupStatus.codeId==3\">Active</span>\r\n            <span *ngIf=\"a.groupStatus.codeId==2\">Inactive</span>\r\n          </td>\r\n          <td *ngIf=\"!dashview\">\r\n            <button class=\"btn btn-primary\" (click)=\"getGroupDetailsById(a.groupId)\">\r\n            Edit\r\n          </button>\r\n          </td>\r\n\r\n        </tr>\r\n\r\n\r\n      </tbody>\r\n    </table>\r\n\r\n  </div>\r\n\r\n\r\n  <!--###################### Edit Group Modal######################-->\r\n  <div bsModal #editGroup=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <form #editGroupForm=\"ngForm\" (ngSubmit)=\"fireFormAction(evt)\">\r\n          <!--####### start form #######-->\r\n\r\n          <div class=\"modal-header\">\r\n\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"editGroup.hide()\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\" *ngIf=\"!viewMode\" style=\"display: inline\">{{modalTitle}}</h4>\r\n             <h4 class=\"modal-title\" *ngIf=\"viewMode\" style=\"display: inline\">Group Information</h4>\r\n            <img src=\"assets/img/loader3.gif\" *ngIf=\"formloaded\" style=\"height: 35px;\" />\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div *ngIf=\"!isSuccess\">\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">Title</label>\r\n                <input [readonly]=\"viewMode\" required type=\"text\" [(ngModel)]=\"groupModelDetail.groupTitle\" name=\"groupModelDetail.groupTitle\" class=\"form-control\"\r\n                  required>\r\n              </div>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"comment\">Description</label>\r\n                <textarea [readonly]=\"viewMode\"  required [(ngModel)]=\"groupModelDetail.groupDescription\" name=\"groupModelDetail.groupDescription\" class=\"form-control\"\r\n                  rows=\"5\" required></textarea>\r\n              </div>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">Country</label>\r\n                <select [disabled]=\"viewMode\" (change)=\"getDistrictList()\" required [(ngModel)]=\"groupModelDetail.groupCountry\" name=\"groupModelDetail.groupCountry\" class=\"form-control\">\r\n                <option *ngFor=\"let k of arrCountries\" [value]=\"k.alpha2Code\">{{k.name}}</option>\r\n            </select>\r\n              </div>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">District</label>\r\n                <select [disabled]=\"viewMode\" (change)=\"getCityList()\" required [(ngModel)]=\"groupModelDetail.groupDistrict\" name=\"groupModelDetail.groupDistrict\" class=\"form-control\"\r\n                  required>\r\n                  <option *ngFor=\"let m of provinces\" >{{m.provinceName}}</option>\r\n                </select>\r\n              </div>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">City</label>\r\n                <select [disabled]=\"viewMode\"  required [(ngModel)]=\"groupModelDetail.groupCity\" name=\"groupModelDetail.groupCity\" class=\"form-control\"\r\n                  required>\r\n                   <option *ngFor=\"let m of cities\" >{{m.cityName}}</option>\r\n                </select>\r\n              </div>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">Public Level</label>\r\n                <select [disabled]=\"viewMode\"  required class=\"form-control\" (change)=\"changePublicLevel($event.target)\" name=\"groupModelDetail.groupPublicLevel\">\r\n                <option *ngFor=\"let m of arrGroupPublic\" [selected]=\"m.codeSeq==groupModelDetail.groupPublicLevel.codeSeq\" [ngValue]=\"m\">{{m.codeMessage}}</option>\r\n                <!--<option value=\"1\" selected>val 1</option>\r\n                  <option value=\"2\">val 2</option>-->\r\n              </select>\r\n              </div>\r\n              <br><br>\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">Group Status</label>\r\n                <select [disabled]=\"viewMode\"  required class=\"form-control\" name=\"groupModelDetail.groupStatus\" (change)=\"changeGroupStatus($event.target)\">\r\n                <option *ngFor=\"let m of arrGroupActiveStatus\" [selected]=\"m.codeSeq == groupModelDetail.groupStatus.codeSeq\" [ngValue]=\"m\">{{m.codeMessage}}</option>\r\n              </select>\r\n              </div>\r\n              <br><br>\r\n\r\n\r\n              <div class=\"form-group\">\r\n                <label for=\"template\">Expiry date</label>\r\n                <input [readonly]=\"viewMode\" required type=\"date\" [(ngModel)]=\"groupModelDetail.expiryDate\" name=\"groupModelDetail.expiryDate\" class=\"form-control\"\r\n                  required>\r\n              </div>\r\n\r\n              <label *ngIf=\"!viewMode\" class=\"control-label\">Select Icon (Support file type - JPG)</label>\r\n              <input *ngIf=\"!viewMode\" type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n\r\n              <table class=\"table\" *ngIf=\"!viewMode\">\r\n                <thead>\r\n                  <tr>\r\n                    <!--<th width=\"50%\">Name</th>\r\n                    <th>Size</th>\r\n                    <th>Progress</th>\r\n                    <th>Status</th>\r\n                    <th>Actions</th>-->\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr *ngFor=\"let item of uploader.queue\">\r\n                    <!-- <td><strong>{{ item?.file?.name }}</strong></td> -->\r\n                    <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                    <td *ngIf=\"uploader.isHTML5\">\r\n                      <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                      </div>\r\n                    </td>\r\n                    <td style=\"text-align: right\">\r\n                      <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                      <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                      <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                    </td>\r\n                    <td nowrap style=\"text-align: right; width: 150px\">\r\n                      <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                      <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n                      <!-- <button type=\"button\" class=\"btn btn-danger btn-xs\" (click)=\"item.remove()\">\r\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                        </button> -->\r\n                    </td>\r\n                    <!--<td>\r\n                      <!--<input type=\"file\" (change)=\"fileChangeEvent($event)\" placeholder=\"Upload file...\" />-->\r\n                    <!--<input type=\"file\" onchange=\"previewFile()\"><br>-->\r\n                    <!--<img src=\"\" height=\"200\" alt=\"Image preview...\">-->\r\n                    <!--</td>-->\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n\r\n            <div *ngIf=\"isSuccess\">\r\n\r\n              <div style=\"text-align: center\">{{successMessage}}</div>\r\n\r\n            </div>\r\n            <!--<input type=\"hidden\" name=\"\"-->\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <img class=\"pull-left\" *ngIf=\"serviceProgress\" src=\"assets/img/loader2.gif\" />\r\n            <button  class=\"btn btn-default\" type=\"button\" (click)=\"editGroup.hide()\">Close</button>\r\n            <button type=\"submit\" *ngIf=\"!isSuccess && operationType == 'insert' && !viewMode\" [disabled]=\"serviceProgress\" class=\"btn btn-success\">&nbsp;&nbsp;Create&nbsp;&nbsp;</button>\r\n            <button type=\"submit\" *ngIf=\"!isSuccess && operationType == 'update' && !viewMode\" [disabled]=\"serviceProgress\" class=\"btn btn-success\">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>\r\n          </div>\r\n\r\n        </form>\r\n        <!--############ end of form #############-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <!-- ================================Accept user Modal -->\r\n\r\n\r\n\r\n  <div bsModal #acceptUsers=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n\r\n        <div class=\"modal-header\" style=\"background-color: #449D44;color: azure\">\r\n\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"acceptUsers.hide()\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\" style=\"display: inline\">Accept Pending Requests</h4>\r\n\r\n\r\n        </div>\r\n        <div class=\"modal-body\">\r\n\r\n          <table style=\"width: 100%\">\r\n            <thead>\r\n              <th>Name</th>\r\n              <th>Date of Birth</th>\r\n              <th>Country</th>\r\n              <th>Province</th>\r\n              <th>City</th>\r\n              <th></th>\r\n            </thead>\r\n            <tbody>\r\n\r\n              <tr *ngFor=\"let m of arrMobile\">\r\n                <td>{{(m.mobileUserName == null)?\"-\":m.mobileUserName }}</td>\r\n                <td>{{(m.mobileUserDob == null)?\"-\":m.mobileUserDob }}</td>\r\n                <td>{{getCountry(m.mobileUserCountry)}}</td>\r\n                <td>{{(m.mobileUserProvince == null) ? \"-\" : m.mobileUserProvince}}</td>\r\n                <td>{{(m.mobileUserCity==null) ? \"-\" : m.mobileUserCity}}</td>\r\n                <td>\r\n                  <button class=\"btn btn-default\" *ngIf=\"!addUserLoader\" type=\"button\" (click)=\"addUserToGroup(m.mobileUserId)\">Accept</button>\r\n                  <img src=\"assets/inlineLoader.gif\" *ngIf=\"addUserLoader && m.mobileUserMobileNo== selectedUser\" style=\"width: 25px\" />\r\n\r\n                </td>\r\n              </tr>\r\n\r\n            </tbody>\r\n          </table>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <button class=\"btn btn-default\" type=\"button\" (click)=\"acceptUsers.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n</div>\r\n\r\n\r\n<div bsModal #noUsers=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-md\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-header\" style=\"background-color: #286090; color: azure\">\r\n        <button type=\"button\" class=\"close pull-right\" (click)=\"noUsers.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n        <h4 class=\"modal-title\">Information</h4>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>This group does not have any pending requests</p>\r\n\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"noUsers.hide()\">Close</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-md\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-header\" style=\"background-color: #449D44; color: azure\">\r\n        <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n        <h4 class=\"modal-title\">Operation Completed</h4>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>{{successMessage}}</p>\r\n\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<div bsModal #fail=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-md\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-header\" style=\"background-color: chocolate;color: azure\">\r\n        <button type=\"button\" class=\"close pull-right\" (click)=\"fail.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n        <h4 class=\"modal-title\">Operation Failed!</h4>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>{{failMessage}}</p>\r\n\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"fail.hide()\">Close</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/grpmangmnt/grpmangmnt.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__model_country__ = __webpack_require__("../../../../../src/app/model/country.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__model_EventUserGroup__ = __webpack_require__("../../../../../src/app/model/EventUserGroup.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GrpmangmntComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HEROES = [
    { id: 1, name: 'Sanga' },
    { id: 2, name: 'Mahela' },
    { id: 3, name: 'Dilshan' },
];
var GrpmangmntComponent = (function () {
    function GrpmangmntComponent(commonService, validator, route) {
        this.commonService = commonService;
        this.validator = validator;
        this.route = route;
        this.fakeArray = new Array(12);
        this.isSuccess = false;
        this.successMessage = "";
        this.showLoader = true;
        this.viewMode = false;
        this.serviceProgress = false;
        this.formloaded = false;
        this.modalTitle = "";
        this.arrMobile = new Array();
        this.arrGroupPublic = new Array();
        this.arrGroupActiveStatus = new Array();
        this.arrMdStatus = new Array();
        this.arrCountries = [];
        this.arrProvince = new Array();
        this.provinces = new Array();
        // public uploader: FileUploader = new FileUploader({ url: AppParams.MEDIA_SERVER+"Majlis-GroupManagement-1.0/service/groups/saveGroupIcon"});
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        /////////////////////////////////////////////
        this.groupModelObj = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        //////////////////////////////////////////
        this.groupModel = new Array();
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        this.validator.validateUser("group-management");
    }
    GrpmangmntComponent.prototype.getDistrictList = function ($event) {
        var country = this.groupModelDetail.groupCountry;
        console.log("country ", country);
        var provinces = this.arrProvince.filter(function (e) {
            return e.countryCode == country;
        });
        console.log("avil province ", provinces.length);
        if (provinces.length > 0) {
            console.log("inside province");
            this.provinces = provinces[0].provinces;
        }
        else {
            this.provinces = [];
        }
        console.log("province list ", this.provinces);
    };
    GrpmangmntComponent.prototype.getCityList = function () {
        var _this = this;
        console.log("district ", this.groupModelDetail.groupDistrict);
        this.cities = this.provinces.filter(function (e) {
            return e.provinceName == _this.groupModelDetail.groupDistrict;
        })[0].cities;
    };
    GrpmangmntComponent.prototype.viewGroupDetail = function (groupId) {
        console.log("group details ", groupId);
        this.viewMode = true;
        this.editGroup.show();
        this.getGroupDetailsById2(groupId);
    };
    GrpmangmntComponent.prototype.addUserToGroup = function (userId) {
        var _this = this;
        console.log("user is ", userId);
        this.addUserLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/approveUser";
        var user = new __WEBPACK_IMPORTED_MODULE_8__model_EventUserGroup__["a" /* EventUserGroupModel */]();
        user.groupId = this.groupId;
        user.userId = userId;
        user.operation = null;
        this.selectedUser = userId;
        console.log("user ", user);
        this.commonService.processPost(url, user).subscribe(function (response) {
            _this.completedAddGroupUser = true;
            _this.addUserLoader = false;
            _this.successMessage = "User has been added to the group";
            _this.acceptUsers.hide();
            _this.success.show();
            _this.getGroupAcceptData(_this.groupId);
        }, function (error) {
            _this.failMessage = "Operation cannot complete at this time. Please try agian later";
            _this.fail.show();
        });
    };
    GrpmangmntComponent.prototype.changePublicLevel = function (taeget) {
        this.groupModelDetail.groupPublicLevel = this.arrGroupPublic.filter(function (val) {
            return val.codeMessage === taeget.value;
        })[0];
    };
    GrpmangmntComponent.prototype.changeGroupStatus = function (taeget) {
        this.groupModelDetail.groupStatus = this.arrGroupActiveStatus.filter(function (val) {
            return val.codeMessage === taeget.value;
        })[0];
    };
    GrpmangmntComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (e) {
            _this.dashview = e['dashview'] || false;
            console.log("eee ", _this.dashview);
        });
        this.commonService.processGet("/assets/countries.json").subscribe(function (res) {
            _this.arrCountries = res;
            console.log("country list ", _this.arrCountries);
        }, function (error) {
        });
        this.commonService.processGet("/assets/provinces.json").subscribe(function (res) {
            _this.arrProvince = res;
        }, function (error) {
        });
        this.loadGroupsToGrid(0, 1025);
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.groupModelDetail.groupIconPath = responsePath.responseData;
        };
        var urlPubic = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getStatus?type=GROUPS&subType=GROUP_LEVELS";
        this.commonService.processGet(urlPubic).subscribe(function (res) {
            console.log("res", res);
            var response = res.getStatus;
            if (response.responseCode == 1) {
                _this.arrGroupPublic = response.responseData;
                console.log("public group ", _this.arrGroupPublic);
            }
        }, function (error) {
            console.log("console ", error);
        });
        var urlActiveStatus = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getStatus?type=GROUPS&subType=ACTIVE_STATUS";
        this.commonService.processGet(urlActiveStatus).subscribe(function (res) {
            var response = res.getStatus;
            if (response.responseCode == 1) {
                _this.arrGroupActiveStatus = response.responseData;
            }
        }, function (error) {
            console.log("console ", error);
        });
    };
    GrpmangmntComponent.prototype.getStatusOfUser = function (statusId) {
        var m = this.arrGroupActiveStatus.filter(function (res) {
            return res.codeSeq == statusId;
        })[0];
        return m;
    };
    GrpmangmntComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    GrpmangmntComponent.prototype.fileOverAnother = function (e) {
        this.hasAnotherDropZoneOver = e;
    };
    GrpmangmntComponent.prototype.performSearch = function (searchTerm) {
        console.log("User entered: " + searchTerm.value);
    };
    GrpmangmntComponent.prototype.openNewGroup = function () {
        var _this = this;
        this.viewMode = false;
        this.serviceProgress = false;
        this.isSuccess = false;
        this.modalTitle = "Create Group";
        this.operationType = "insert";
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onBeforeUploadItem = function (e) {
            _this.serviceProgress = true;
        };
        this.uploader.onCompleteItem = function (m, v) {
            console.log("completed! 1");
            _this.serviceProgress = false;
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.groupModelDetail.groupIconPath = responsePath.responseData;
        };
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            var responsePath = JSON.parse(v);
            _this.serviceProgress = false;
            console.log("aaa", responsePath);
            _this.groupModelDetail.groupIconPath = responsePath.responseData;
        };
        this.editGroup.show();
    };
    GrpmangmntComponent.prototype.getCountry = function (country) {
        var ct = this.arrCountries.filter(function (e) {
            return e.alpha2Code == country;
        })[0];
        return ct.name;
    };
    GrpmangmntComponent.prototype.getGroupAcceptData = function (groupId) {
        var _this = this;
        this.groupId = groupId;
        this.userAcceptLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getAllAcceptUsers?groupId=" + groupId;
        this.commonService.processGet(url).subscribe(function (response) {
            console.log("getAccept ", response);
            var res = response.getAllAcceptUsers;
            if (res.responseCode == 1) {
                _this.arrMobile = res.responseData;
                if (_this.arrMobile.length == 0) {
                    _this.noUsers.show();
                }
                else {
                    _this.acceptUsers.show();
                }
            }
            else {
                _this.fail.show();
            }
            _this.userAcceptLoader = false;
        }, function (error) {
            _this.userAcceptLoader = false;
            console.log("error ", error);
        });
    };
    GrpmangmntComponent.prototype.loadGroupsToGrid = function (start, limit) {
        var _this = this;
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupList?start=" + start + "&limit=" + limit;
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupList?start=" + start + "&limit=" + limit + "&userId=" + this.validator.getUserId();
        }
        // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/group/getGroupList?start=" + start + "&limit=" + limit;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.groupModel = res.getGroupList.responseData;
            console.log("response api ", _this.groupModel);
            _this.showLoader = false;
        }, function (error) {
            console.log("error ", error);
        });
    };
    GrpmangmntComponent.prototype.saveGroupChanges = function (evt) {
        var _this = this;
        this.serviceProgress = true;
        //  let reader=new FileReader();
        var header = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        console.log("saveGroupChanges ", this.groupModelDetail);
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/updateGroup";
        // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/group/updateGroup";
        this.commonService.processPostWithHeaders(url, this.groupModelDetail, option).subscribe(function (res) {
            _this.isSuccess = true;
            _this.serviceProgress = false;
            var response = res.updateGroup;
            if (response.responseCode == 1) {
                _this.successMessage = "Group Details have been updated!";
            }
            else {
                _this.successMessage = "Error in Group Details updated!";
            }
            _this.loadGroupsToGrid(0, 5000);
        }, function (error) {
            console.log("error >>> ", error);
        });
    };
    GrpmangmntComponent.prototype.getGroupDetailsById2 = function (groupid) {
        var _this = this;
        this.serviceProgress = true;
        this.viewMode = true;
        this.formloaded = true;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onBeforeUploadItem = function (e) {
            _this.serviceProgress = true;
        };
        this.uploader.onCompleteItem = function (m, v) {
            var responsePath = JSON.parse(v);
            _this.serviceProgress = false;
            ;
            _this.groupModelDetail.groupIconPath = responsePath.responseData;
        };
        this.modalTitle = "Edit Group";
        this.operationType = "update";
        this.isSuccess = false;
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        console.log(">>>>>>>>>>>", groupid);
        this.serviceProgress = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupDetail?groupid=" + groupid;
        // let url = "http://192.0.0.48:9090/Majlis-GroupManagement-1.0/service/group/getGroupDetail?groupid=" + groupid;
        this.commonService.processGet(url).subscribe(function (res) {
            _this.groupModelDetail = res.getGroupDetail.responseData[0];
            console.log("res >>> ", _this.groupModelDetail);
            _this.formloaded = false;
            _this.serviceProgress = false;
        }, function (error) {
            _this.formloaded = false;
            console.log("error >>> ", error);
            _this.serviceProgress = false;
        });
        this.editGroup.show();
    };
    GrpmangmntComponent.prototype.getGroupDetailsById = function (groupid) {
        var _this = this;
        this.serviceProgress = true;
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        this.viewMode = false;
        this.formloaded = true;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-GroupManagement-1.0/service/groups/saveGroupIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onBeforeUploadItem = function (e) {
            _this.serviceProgress = true;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.serviceProgress = false;
            console.log("aaa", responsePath);
            _this.groupModelDetail.groupIconPath = responsePath.responseData;
        };
        this.modalTitle = "Edit Group";
        this.operationType = "update";
        this.isSuccess = false;
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        console.log(">>>>>>>>>>>", groupid);
        this.serviceProgress = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupDetail?groupid=" + groupid;
        // let url = "http://192.0.0.48:9090/Majlis-GroupManagement-1.0/service/group/getGroupDetail?groupid=" + groupid;
        this.commonService.processGet(url).subscribe(function (res) {
            _this.groupModelDetail = res.getGroupDetail.responseData[0];
            console.log("res >>> ", _this.groupModelDetail);
            _this.getDistrictList(_this);
            _this.groupModelDetail.groupDistrict = _this.groupModelDetail.groupDistrict;
            _this.getCityList();
            _this.formloaded = false;
            _this.serviceProgress = false;
        }, function (error) {
            _this.formloaded = false;
            console.log("error >>> ", error);
            _this.serviceProgress = false;
        });
        this.editGroup.show();
    };
    GrpmangmntComponent.prototype.fireFormAction = function (event) {
        if (this.operationType == 'insert') {
            this.createGroup(event);
        }
        else if (this.operationType == 'update') {
            this.saveGroupChanges(event);
        }
    };
    GrpmangmntComponent.prototype.createGroup = function (evt) {
        var _this = this;
        var me = this;
        this.serviceProgress = true;
        this.groupModelObj = new __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */]();
        if (this.groupModelDetail.groupPublicLevel.codeId == null) {
            this.groupModelDetail.groupPublicLevel = this.arrGroupPublic.filter(function (val) {
                return val.codeSeq === 1;
            })[0];
        }
        if (this.groupModelDetail.groupStatus.codeId == null) {
            this.groupModelDetail.groupStatus = this.arrGroupActiveStatus.filter(function (val) {
                return val.codeSeq === 1;
            })[0];
        }
        console.log("Create Group", this.groupModelDetail);
        console.log("saveGroupChanges ", this.groupModelDetail);
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/createGroup";
        var header = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        this.groupModelObj.groupStatus.codeSeq;
        this.groupModelDetail.groupCountry = this.groupModelDetail.groupCountry;
        this.commonService.processPostWithHeaders(url, this.groupModelDetail, option).subscribe(function (res) {
            _this.isSuccess = true;
            if (res.createGroup.responseCode == 1) {
                _this.successMessage = "Group created successfully!";
                _this.groupModelObj = res.createGroup.responseData;
                _this.loadGroupsToGrid(0, 5000);
            }
            else {
                _this.successMessage = "Error in Group created unsuccessfully!";
            }
            _this.serviceProgress = false;
        }, function (error) {
            console.log("error >>> ", error);
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("groupPublicLevel"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "groupPublicLevel", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("editGroupForm"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "editGroupForm", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("acceptUsers"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "acceptUsers", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fail"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "fail", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "success", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("noUsers"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "noUsers", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("editGroup"), 
        __metadata('design:type', Object)
    ], GrpmangmntComponent.prototype, "editGroup", void 0);
    GrpmangmntComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-grpmangmnt',
            template: __webpack_require__("../../../../../src/app/grpmangmnt/grpmangmnt.component.html"),
            // template:'<table><tboby></tboby></table>',  
            styles: [__webpack_require__("../../../../../src/app/grpmangmnt/grpmangmnt.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_2__model_Group__["a" /* GroupModel */], __WEBPACK_IMPORTED_MODULE_7__model_country__["a" /* CountryModel */]]
        }),
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({ selector: '[ng2FileSelect]' }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_9__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_9__angular_router__["c" /* ActivatedRoute */]) === 'function' && _c) || Object])
    ], GrpmangmntComponent);
    return GrpmangmntComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=grpmangmnt.component.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\r\n * Specific styles of signin component\r\n */\r\n/*\r\n * General styles\r\n */\r\nhtml, body {\r\n    height: 100%;\r\n}\r\n\r\n\r\n/*\r\n\r\n.reauth-email {\r\n    display: block;\r\n    color: #404040;\r\n    line-height: 2;\r\n    margin-bottom: 10px;\r\n    font-size: 14px;\r\n    text-align: center;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    white-space: nowrap;\r\n    -moz-box-sizing: border-box;\r\n    -webkit-box-sizing: border-box;\r\n    box-sizing: border-box;\r\n}\r\n\r\n\r\n\r\n.navbar-fixed-left {\r\n  width: 140px;\r\n  position: fixed;\r\n  border-radius: 0;\r\n  height: 100%;\r\n}\r\n\r\n.navbar-fixed-left .navbar-nav > li {\r\n  float: none; \r\n  width: 139px;\r\n}\r\n\r\n.navbar-fixed-left + .container {\r\n  padding-left: 160px;\r\n}*/\r\n\r\n.fill { \r\n    min-height: 100%;\r\n    height: 100%;\r\n    min-width: 100%;\r\n    width: 100%;\r\n    background: #2d2d31;\r\n    position: absolute;\r\n    margin-top: -71px;\r\n    padding-bottom: 1000px;\r\n}\r\n\r\nh1{\r\n     font-weight: 200;\r\n}\r\n.login_box{\r\n    background: #009688;    \r\n    width:100%;\r\n    /*position:absolute;*/\r\n    /*top:15%;*/\r\n    /*left:32.5%;*/\r\n}\r\n.login_box{\r\n    margin-top: 40%;\r\n}\r\n\r\n\r\n\r\n.img-circle{\r\n    border-radius: 50%;\r\n    width: 175px;\r\n    height: 175px;\r\n    border: 4px solid #FFF;\r\n    margin: 10px;\r\n}\r\n\r\n.login_control{\r\n    background-color:#FFF;\r\n    padding:10px;\r\n    \r\n}\r\n\r\n.control {\r\n    color:#000;\r\n    margin:10px;\r\n}\r\n.label {\r\n    color: #009688;\r\n    font-size: 18px;\r\n    font-weight: 500;\r\n}\r\n.form-control{\r\n    color: #000000 !important;\r\n    font-size: 25px;\r\n    border: none;\r\n    padding: 25px;\r\n    padding-left: 10px;\r\n    border-bottom: 1px solid #CCC;\r\n    margin-bottom: 10px;\r\n    outline: none;\r\n    box-shadow: none !important;\r\n}\r\n\r\n.form-control:focus{\r\n    border-radius: 0px;\r\n    border-bottom: 1px solid #009688;\r\n    margin-bottom: 10px;\r\n    outline: none;\r\n    box-shadow: none !important;\r\n}\r\n.btn-green{\r\n    background-color: #009688;\r\n    border-radius: 0px;\r\n    margin: 5px;\r\n    padding: 5px;\r\n    width: 150px;\r\n    font-size: 20px;\r\n    font-weight: inherit;\r\n}\r\n\r\n.btn-orange:hover {\r\n    background-color: #009688;\r\n    border-radius: 0px;\r\n    margin: 5px;\r\n    padding: 5px;\r\n    width: 150px;\r\n    font-size: 20px;\r\n    font-weight: inherit;\r\n    color:#FFF !important;\r\n}\r\n.wrapper{\r\n    padding: 0px;\r\n    border: 1px solid rgba(255, 255, 255, 0.29);\r\n    border-radius: 50%;\r\n    width: 200px;\r\n    height: 200px;\r\n    margin-top: 20px;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n\r\n\r\n<!--<app-side-bar></app-side-bar>-->\r\n\r\n<div class=\"container-fluid fill\">\r\n\r\n  <div class=\"col-md-4 col-xs-2\"></div>\r\n  <div class=\"col-md-4 col-xs-8 center-block\">\r\n\r\n    <div class=\"main_box\">\r\n      <div class=\"col-md-12 col-xs-12 login_box\" align=\"center\">\r\n        <div class=\"wrapper\"><img src=\"https://cyber-education.az/resources/site/design/icons/admin.svg\" class=\"img-circle\" /></div>\r\n        <h1>Welcome to Majlis</h1>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 col-xs-12 login_control\">\r\n        <form (ngSubmit)=\"login($event)\">\r\n\r\n          <div class=\"control\">\r\n            <span id=\"reauth-email\" class=\"reauth-email\"></span>\r\n            <input type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\" placeholder=\"Username\" required autofocus>\r\n\r\n          </div>\r\n\r\n          <div class=\"control\">\r\n            <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\r\n\r\n          </div>\r\n          <div class=\"text-center\">\r\n            <button class=\"btn btn-green\" *ngIf=\"btnSignin\" type=\"submit\">LOGIN</button>\r\n          </div>\r\n          <div class=\"cssload-container\" *ngIf=\"signinMask\" style=\"margin-top: 40px\">\r\n            <div class=\"cssload-whirlpool\"></div>\r\n          </div>\r\n\r\n          <alert type=\"md-local\" type=\"success\" *ngIf=\"loginFailMsg !=''\">\r\n            <strong>Login Failed!</strong> {{loginFailMsg}}.\r\n          </alert>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-4 col-xs-2\"></div>\r\n\r\n  \r\n\r\n\r\n\r\n\r\n\r\n\r\n  <!--  <div class=\"card card-container\">\r\n\r\n    <img id=\"profile-img\" class=\"profile-img-card\" src=\"//ssl.gstatic.com/accounts/ui/avatar_2x.png\" style=\"vertical-align: middle;\"\r\n    />\r\n    <p id=\"profile-name\" class=\"profile-name-card\"></p>\r\n\r\n    <form class=\"form-signin\" (ngSubmit)=\"login($event)\">\r\n\r\n      <span id=\"reauth-email\" class=\"reauth-email\"></span>\r\n      <input type=\"text\" [(ngModel)]=\"username\" name=\"username\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Username\" required\r\n        autofocus>\r\n      <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Password\"\r\n        required>\r\n      <div id=\"remember\" class=\"checkbox\">\r\n\r\n      </div>\r\n\r\n      <button class=\"btn btn-lg btn-primary btn-block btn-signin\" *ngIf=\"btnSignin\" type=\"submit\">Sign in</button>\r\n      <div class=\"cssload-container\" *ngIf=\"signinMask\" style=\"margin-top: 40px\">\r\n        <div class=\"cssload-whirlpool\"></div>\r\n      </div>\r\n\r\n      <alert type=\"md-local\" type=\"success\" *ngIf=\"loginFailMsg !=''\">\r\n        <strong>Login Failed!</strong> {{loginFailMsg}}.\r\n      </alert>\r\n\r\n    </form>\r\n  </div> -->\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_loggedInUserModel__ = __webpack_require__("../../../../../src/app/model/loggedInUserModel.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_service__ = __webpack_require__("../../../../../src/app/shared-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginComponent = (function () {
    function LoginComponent(userData, sharedService, webservice, router) {
        this.userData = userData;
        this.sharedService = sharedService;
        this.webservice = webservice;
        this.router = router;
        this.btnSignin = true;
        this.signinMask = false;
        this.loginFailMsg = "";
        localStorage.clear();
        sessionStorage.clear();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function ($event) {
        var _this = this;
        $event.preventDefault();
        console.log("login.....");
        this.loginFailMsg = '';
        this.btnSignin = false;
        this.signinMask = true;
        this.userData.userId = this.username;
        this.userData.password = this.password;
        var header = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        header.set("system", "MajlisCMS");
        option.headers = header;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "user/validateCmsUser";
        this.webservice.processPostWithHeaders(url, this.userData, option).subscribe(function (response) {
            console.log("response", response.validateCmsUser.data[0]);
            var responseData = response.validateCmsUser;
            if (responseData != null) {
                var flag = responseData['flag'];
                if (flag == 1000) {
                    console.log("success zzz", responseData.data[0]);
                    var user = responseData.data[0];
                    _this.sharedService.setLoggedInUserst(user);
                    sessionStorage.setItem('id_token', btoa(JSON.stringify(user)));
                    _this.router.navigate(['./dashboard']);
                }
                else {
                    _this.loginFailMsg = responseData['exceptionMessages'][0];
                }
            }
            else {
                _this.loginFailMsg = "Cannot connect with the authentication server";
            }
            _this.signinMask = false;
            _this.btnSignin = true;
        }, function (error) {
            _this.signinMask = false;
            _this.btnSignin = true;
        });
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/login/login.component.html"),
            // template:'<router-outlet></router-outlet>',
            styles: [__webpack_require__("../../../../../src/app/login/login.component.css"), __webpack_require__("../../../../../src/app/styles/loaders.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__model_loggedInUserModel__["a" /* LoggedInUserModel */], __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_5__shared_service__["a" /* SharedService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__model_loggedInUserModel__["a" /* LoggedInUserModel */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__model_loggedInUserModel__["a" /* LoggedInUserModel */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__shared_service__["a" /* SharedService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__shared_service__["a" /* SharedService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* Router */]) === 'function' && _d) || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/mobileuser/mobileuser.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".panel-heading{\r\n    background-color: #009688;\r\n    margin-top: 40px;\r\n}\r\n\r\n.input-group.input-group-unstyled input.form-control {\r\n    border-radius: 4px;\r\n}\r\n.input-group-unstyled .input-group-addon {\r\n    border-radius: 4px;\r\n    border: 0px;\r\n    background-color: transparent;\r\n}\r\n\r\n#btnUser{\r\n    margin-left: 170px;\r\n}\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n.circle-user{\r\n width: 40px;\r\n  height: 40px;\r\n  border-radius: 50%;\r\n  font-size: 20px;\r\n  color: #fff;\r\n  line-height: 40px;\r\n  text-align: center;\r\n  background: #009486;\r\n  margin-left: 20px;\r\n}\r\n\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:10px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n\r\n/*Model styles*/\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n/*toggle styles*/\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\r\n  margin-left: 60px;\r\n}\r\n\r\n.switch input {display:none;}\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked + .slider {\r\n  background-color: #009486;\r\n}\r\n\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #009486;\r\n}\r\n\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}\r\n\r\n\r\n/*modal*/\r\n.modal-body {\r\n    max-height: calc(100vh - 210px);\r\n    overflow-y: auto;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\n.user-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.user-email{\r\n    color: #6f7982;\r\n}\r\n\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n\r\n.cat-list{\r\n    color: #30363a;\r\n    font-size: 16px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/mobileuser/mobileuser.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop>\n</app-pagetop>\n<app-side-bar></app-side-bar>\n<div class=\"container\">\n    <div class=\"page-title\">\n        <h2>Mobile User Management</h2>\n    </div>\n\n\n    <div class=\"panel panel-default\">\n\n\n        <table class=\"table table-hover\">\n            <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\n                <tr>\n                    <th></th>\n                    <th>User</th>\n                    <th>Mobile No</th>\n                    <th>Date of Birth</th>\n                    <th>Date of Registration</th>\n                    <th>Assign Groups</th>\n                    <!--<th>Status</th>-->\n                </tr>\n            </thead>\n            <tbody>\n\n                <tr *ngFor=\"let a of mobileUserModel;\">\n                    <td>\n                        <!--<div class=\"circle-user col-sm-1\">{{myName}}</div>-->\n                    </td>\n                    <td>\n                        <div class=\"col-sm-12 user-id\">{{a.mobileUserId}}</div>\n                        <div class=\"col-sm-12 user-name\">{{a.mobileUserName}}</div>\n                    </td>\n                    <td>\n                        <div class=\"col-sm-12 mobile-no\">{{a.mobileUserMobileNo}}</div>\n                    </td>\n                    <td>\n                        <div class=\"col-sm-12 mobile-no\">{{a.mobileUserDob}}</div>\n                    </td>\n                    <td>\n                        <div class=\"col-sm-12 mobile-no\">{{a.mobileUserDateRegistration}}</div>\n                    </td>\n                    <td>\n                            <button class=\"btn btn-primary\" (click)=\"getMobileGroupsById(a)\">\n                            Assign\n                        </button>\n                    </td>\n                </tr>\n\n\n            </tbody>\n        </table>\n\n    </div>\n\n\n\n\n\n    <div bsModal #assignToGroup=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog modal-md\">\n\n            <div class=\"modal-content\">\n                <form #assignGroupForm=\"ngForm\" (ngSubmit)=\"submitForm($e)\">\n\n                    <div class=\"modal-header\">\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"assignToGroup.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n                        <h4 class=\"modal-title\">Assign Groups</h4>\n                    </div>\n                    <div class=\"modal-body\">\n\n                        <div class=\"row\" *ngFor=\"let a of groupModel;\">\n                            <div class=\"col-sm-12\"><br></div>\n                            <div class=\"col-sm-1\"><img class=\"circle-image pull-left\" [src]=\"a.groupIconPath\"></div>\n\n                            <div class=\"col-sm-8\">\n                                <div class=\"col-sm-12\">\n                                    {{a.groupTitle}} </div>\n                        \n                            </div>\n                            <div class=\"col-sm-3 pull-right\">\n                          \n                             <label class=\"switch\">\n                            <input type=\"checkbox\" (change)=\"assignGroup(a.groupId,$event)\" [checked]=\"!getUserGroupStatus(a.groupId)\" name=\"template-status\">\n                            <div class=\"slider round\"></div>\n                        </label>\n                            </div>\n                        </div>\n                    </div>\n\n                </form>\n\n            </div>\n        </div>\n    </div>\n\n    <!--#################Make Group Admin Modal###############-->\n    <div bsModal #makeGroupAdmin=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog modal-lg\">\n\n            <div class=\"modal-content\">\n                <form #makeAdminForm=\"ngForm\" (ngSubmit)=\"submitForm($e)\">\n                    <!--########## start form ########-->\n                    <div class=\"modal-header\">\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"makeGroupAdmin.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n                        <h4 class=\"modal-title\">Assign Group Admin</h4>\n                    </div>\n                    <div class=\"modal-body\">\n\n                        <div class=\"row\" *ngFor=\"let a of fakeArray1;\">\n                            <div class=\"col-sm-12\"><br></div>\n                            <div class=\"col-sm-1\">\n                                <div class=\"circle-user col-sm-1 pull-left\">{{myName}}</div>\n                            </div>\n\n                            <div class=\"col-sm-7\">\n                                <div class=\"col-sm-12\">\n                                    This is the title of group - This is the title of group </div>\n                            </div>\n                            <div class=\"col-sm-2\">\n                                <label class=\"switch\">\n                                <input type=\"checkbox\">\n                                <div class=\"slider round\"></div>\n                            </label>\n                            </div>\n                            <div class=\"col-sm-2\">\n                                <!--<a class=\"btn pull-right\" (click)=\"makeGroupAdminSetting.show()\">Setting</a>-->\n\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"makeGroupAdmin.hide()\">Close</button>\n                        <button type=\"button\" class=\"btn btn-success\">Save changes</button>\n                    </div>\n                </form>\n                <!--################ end form ################-->\n            </div>\n        </div>\n    </div>\n\n    <!--#################Make Group Admin Setting Modal###############-->\n    <div bsModal #makeGroupAdminSetting=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog modal-md\">\n\n            <div class=\"modal-content\">\n                <form #adminSettingForm=\"ngForm\" (ngSubmit)=\"btnUSubmit()\">\n                    <!--########## start form ########-->\n                    <div class=\"modal-header\">\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"makeGroupAdminSetting.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n                        <h4 class=\"modal-title\">Setting</h4>\n                    </div>\n                    <div class=\"modal-body\">\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">First Name</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">Last Name</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">Telephone</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">Email</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">User Status</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">Address</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">Expiry Date</label>\n                            <input type=\"date\" class=\"form-control\" required>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"template\">No. of allowed notifications (\"-1\" if unlimited)</label>\n                            <input type=\"text\" class=\"form-control\" required>\n                        </div>\n\n\n                    </div>\n\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"makeGroupAdminSetting.hide()\">Close</button>\n                        <button type=\"submit\" class=\"btn btn-success\">Save changes</button>\n                    </div>\n                </form>\n                <!--################ end form ################-->\n            </div>\n        </div>\n    </div>\n\n\n\n    <!--###################Show Groups Modal#######################-->\n    <div bsModal #showGroups=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n        <div class=\"modal-dialog modal-md\">\n\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close pull-right\" (click)=\"showGroups.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n                    <h4 class=\"modal-title\">Groups</h4>\n                </div>\n                <div class=\"modal-body\">\n\n\n\n                    <div class=\"row\" *ngFor=\"let a of fakeArray1;\">\n                        <div class=\"col-sm-12\"><br></div>\n                        <div class=\"col-sm-1\"><img class=\"circle-image pull-left\" src=\"https://i.ytimg.com/vi/AZj9EaZRGV8/maxresdefault.jpg\" /></div>\n\n                        <div class=\"col-sm-8\">\n                            <div class=\"col-sm-12\">\n                                This is the title of group - This is the title of group </div>\n                            <div class=\"col-sm-12\">\n                                This is the description of group\n                            </div>\n                        </div>\n                        <div class=\"col-sm-3 pull-right\">\n                            Group Admin\n                        </div>\n                    </div>\n\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-default\" (click)=\"showGroups.hide()\">Close</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <!--####################Show Category Modal#######################-->\n    <div bsModal #showCategory=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog modal-sm\">\n\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close pull-right\" (click)=\"showCategory.hide()\" aria-label=\"Close\">\n                                    <span aria-hidden=\"true\">&times;</span>\n                                </button>\n                    <h4 class=\"modal-title\">Categories</h4>\n                </div>\n                <div class=\"modal-body\">\n\n\n\n                    <div class=\"row\" *ngFor=\"let a of fakeArray1;\">\n                        <div class=\"col-sm-12\"><br></div>\n\n                        <div class=\"col-sm-12\">\n                            <div class=\"cat-list col-sm-12 text-center \">\n                                Birthday\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-default\" (click)=\"showCategory.hide()\">Close</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/mobileuser/mobileuser.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_MobileUser__ = __webpack_require__("../../../../../src/app/model/MobileUser.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MobileuserComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { DatepickerModule } from 'ngx-bootstrap/datepicker';
// import { DialogService } from "ng2-bootstrap-modal";
var MobileuserComponent = (function () {
    function MobileuserComponent(commonService, validator) {
        this.commonService = commonService;
        this.validator = validator;
        this.myName = "Kushan";
        this.fakeArray = new Array(12);
        this.fakeArray1 = new Array(10);
        this.signinMask = false;
        this.adminUser = "super";
        this.mobileUserModel = new Array();
        this.mobileUserModelDetail = new __WEBPACK_IMPORTED_MODULE_3__model_MobileUser__["a" /* MobileUserModel */]();
        this.mobileUserModel2 = new __WEBPACK_IMPORTED_MODULE_3__model_MobileUser__["a" /* MobileUserModel */]();
        this.groupModel = new Array();
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_5__model_Group__["a" /* GroupModel */]();
        this.totalItems = 64;
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.validator.validateUser("mobileuser");
    }
    MobileuserComponent.prototype.ngOnInit = function () {
        this.myName = this.myName.substring(0, 1);
        this.loadMobileUserToGrid();
    };
    MobileuserComponent.prototype.showChildModal = function () {
        this.childModal.show();
    };
    MobileuserComponent.prototype.hideChildModal = function () {
        this.childModal.hide();
    };
    // btnSearch = function(){
    //   console.log('User entered: ${searchByUserId.value}');
    //   // this.router.navigate(['./templatemngmnt'])
    // }
    MobileuserComponent.prototype.performSearch = function (searchTerm) {
        console.log("User entered: " + searchTerm.value);
    };
    MobileuserComponent.prototype.loadMobileUserToGrid = function () {
        var _this = this;
        this.signinMask = true;
        console.log("USER zz");
        // let url = AppParams.BASE_PATH + "users/getAllUsers?start=" + start + "&limit=" + limit;
        // let url = AppParams.BASE_PATH + "users/getAllMobileUsers";
        var url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "users/getAllMobileUsers?adminUserId=" + this.adminUser;
        this.commonService.processGet(url).subscribe(function (res) {
            // console.log("response", res.getAllMobileUsers.responseData);
            // this.userModel = res.users.responseData;
            // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
            if (res.getAllMobileUsers.responseCode == 1) {
                _this.signinMask = true;
                _this.mobileUserModel = res.getAllMobileUsers.responseData;
                console.log("response api ", _this.mobileUserModel);
            }
        }, function (error) {
            console.log("error ", error);
        });
    };
    MobileuserComponent.prototype.getUserGroupStatus = function (groupId) {
        var obj = this.mobileUserModel2.majlisGroupList.filter(function (e) {
            return e.groupId == groupId;
        });
        return obj.length == 0;
        // console.log("groupObj "+groupId,groupObj)
    };
    MobileuserComponent.prototype.getMobileGroupsById = function (a) {
        var _this = this;
        console.log("aaa ", a);
        this.mobileUserModel2 = a;
        console.log("logged user ", this.validator.user.groups[0].groupId);
        this.userIdVal = a.mobileUserId;
        this.signinMask = true;
        console.log("GroupsById>>", a.mobileUserId);
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupList";
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/getGroupList?userId=" + this.validator.getUserId();
        }
        this.commonService.processGet(url).subscribe(function (res) {
            res = res.getGroupList;
            if (res.responseCode == 1) {
                _this.signinMask = false;
                _this.groupModel = res.responseData;
                console.log("groupModel >>> ", _this.groupModel);
            }
        }, function (error) {
            console.log("error >>> ", error);
            _this.signinMask = false;
        });
        // this.showGroups.show()
        this.assignToGroup.show();
    };
    MobileuserComponent.prototype.assignGroup = function (groupId, $event) {
        var _this = this;
        console.log("check box", $event.target.checked);
        var url = __WEBPACK_IMPORTED_MODULE_2__app_module__["b" /* AppParams */].BASE_PATH + "groups/updateGroupsForUser";
        var obj = {
            userId: this.userIdVal,
            groupId: groupId,
            operation: $event.target.checked
        };
        this.commonService.processPost(url, obj).subscribe(function (res) {
            console.log("response ", res);
            _this.loadMobileUserToGrid();
        }, function (error) {
        });
    };
    MobileuserComponent.prototype.showPrompt = function () {
        console.log("aDD nEW uSER");
    };
    MobileuserComponent.prototype.btnUSubmit = function () {
        // $('')
        console.log("Submit form");
        // console.log(form);
    };
    // submitForm($e) {
    //   // $e.preventDefault();
    //   console.log("Submit formzzz");
    // }
    // btnSesarch(searchByUserId): void {
    //   console.log('User entered: ${searchByUserId.value}');
    // }form.value
    MobileuserComponent.prototype.pageChanged = function (event) {
        console.log('Page changed to: ' + event.page);
        console.log('Number items per page: ' + event.itemsPerPage);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('childModal'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */]) === 'function' && _a) || Object)
    ], MobileuserComponent.prototype, "childModal", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("assignToGroup"), 
        __metadata('design:type', Object)
    ], MobileuserComponent.prototype, "assignToGroup", void 0);
    MobileuserComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-mobileuser',
            template: __webpack_require__("../../../../../src/app/mobileuser/mobileuser.component.html"),
            styles: [__webpack_require__("../../../../../src/app/mobileuser/mobileuser.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */]) === 'function' && _c) || Object])
    ], MobileuserComponent);
    return MobileuserComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=mobileuser.component.js.map

/***/ }),

/***/ "../../../../../src/app/model/BirthdayTemplate.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BirthdayTemplateModel; });

var BirthdayTemplateModel = (function () {
    function BirthdayTemplateModel() {
        this.templateId = null;
        this.templateSystemId = null;
        this.templateTitle = null;
        this.templateIconPath = null;
        this.templateMessage = null;
        this.dateInserted = null;
        this.userInserted = null;
        this.dateModified = null;
        this.userModified = null;
        this.templateStatus = new __WEBPACK_IMPORTED_MODULE_0__MDCode__["a" /* MDCodeModel */]();
    }
    return BirthdayTemplateModel;
}());
//# sourceMappingURL=BirthdayTemplate.js.map

/***/ }),

/***/ "../../../../../src/app/model/CMSUsers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CMSUsersModel; });

var CMSUsersModel = (function () {
    function CMSUsersModel() {
        this.userId = null;
        this.userFullName = null;
        this.emailAddress = null;
        this.expiryDate = null;
        this.noAllowedNotifications = null;
        this.userInserted = null;
        this.dateInserted = null;
        this.userModified = null;
        this.dateModified = null;
        this.status = new __WEBPACK_IMPORTED_MODULE_0__MDCode__["a" /* MDCodeModel */]();
    }
    return CMSUsersModel;
}());
//# sourceMappingURL=CMSUsers.js.map

/***/ }),

/***/ "../../../../../src/app/model/Category.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CategoryStatus__ = __webpack_require__("../../../../../src/app/model/CategoryStatus.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryModel; });

var CategoryModel = (function () {
    function CategoryModel() {
        this.categoryId = null;
        this.categorySystemId = null;
        this.categoryTitle = null;
        this.categoryIconPath = null;
        this.categoryDescription = null;
        this.userInserted = null;
        this.dateInserted = null;
        this.userModified = null;
        this.dateModified = null;
        this.categoryStatus = new __WEBPACK_IMPORTED_MODULE_0__CategoryStatus__["a" /* CategoryStatusModel */]();
    }
    return CategoryModel;
}());
//# sourceMappingURL=Category.js.map

/***/ }),

/***/ "../../../../../src/app/model/CategoryStatus.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryStatusModel; });
var CategoryStatusModel = (function () {
    function CategoryStatusModel() {
        this.codeId = null;
        this.codeType = null;
        this.codeSubType = null;
        this.codeLocale = null;
        this.codeSeq = null;
        this.codeMessage = null;
    }
    return CategoryStatusModel;
}());
//# sourceMappingURL=CategoryStatus.js.map

/***/ }),

/***/ "../../../../../src/app/model/ChartCategory.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartCategoryModel; });
var ChartCategoryModel = (function () {
    function ChartCategoryModel() {
        this.id = null;
        this.categoryName = null;
        this.count = null;
        this.percentage = null;
        this.color = null;
    }
    return ChartCategoryModel;
}());
//# sourceMappingURL=ChartCategory.js.map

/***/ }),

/***/ "../../../../../src/app/model/Comment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentModel; });
var CommentModel = (function () {
    function CommentModel() {
        this.commentEventId = null;
        this.commentId = null;
        this.commentLike = null;
        this.commentMessage = null;
        this.userInserted = null;
        this.dateInserted = null;
    }
    return CommentModel;
}());
//# sourceMappingURL=Comment.js.map

/***/ }),

/***/ "../../../../../src/app/model/CustomReport.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomReportModel; });
var CustomReportModel = (function () {
    function CustomReportModel() {
        this.reportId = null;
        this.reportName = null;
        this.query = null;
        this.status = null;
    }
    return CustomReportModel;
}());
//# sourceMappingURL=CustomReport.js.map

/***/ }),

/***/ "../../../../../src/app/model/DashboardItems.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashBoardItemModel; });
var DashBoardItemModel = (function () {
    function DashBoardItemModel() {
        this.event_active = null;
        this.group_active = null;
        this.event_inactive = null;
        this.group_inactive = null;
        this.tot_notification_limit = null;
        this.tot_notification = null;
    }
    return DashBoardItemModel;
}());
//# sourceMappingURL=DashboardItems.js.map

/***/ }),

/***/ "../../../../../src/app/model/DeleteEventData.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeleteEventDataModel; });
var DeleteEventDataModel = (function () {
    function DeleteEventDataModel() {
        this.eventId = null;
        this.deletedObjId = null;
        this.operaton = null;
    }
    return DeleteEventDataModel;
}());
//# sourceMappingURL=DeleteEventData.js.map

/***/ }),

/***/ "../../../../../src/app/model/Event.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Category__ = __webpack_require__("../../../../../src/app/model/Category.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventModel; });



var EventModel = (function () {
    function EventModel() {
        this.eventId = null;
        this.eventSystemId = null;
        this.eventCaption = null;
        this.eventIconPath = null;
        this.eventMessage = null;
        this.eventLocation = null;
        this.eventLocationLat = null;
        this.eventLocationLont = null;
        this.eventTime = null;
        this.eventDate = null;
        this.userInserted = null;
        this.dateInserted = null;
        this.manualCount = null;
        this.userModified = null;
        this.dateModified = null;
        this.eventCategory = new __WEBPACK_IMPORTED_MODULE_0__Category__["a" /* CategoryModel */]();
        this.eventStatus = new __WEBPACK_IMPORTED_MODULE_1__MDCode__["a" /* MDCodeModel */]();
        this.groupId = new __WEBPACK_IMPORTED_MODULE_2__Group__["a" /* GroupModel */]();
    }
    return EventModel;
}());
//# sourceMappingURL=Event.js.map

/***/ }),

/***/ "../../../../../src/app/model/EventImages.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventImagesModel; });
var EventImagesModel = (function () {
    function EventImagesModel() {
        this.eventImageId = null;
        this.eventImagePath = null;
        this.userInserted = null;
        this.dateInserted = null;
        this.eventId = null;
    }
    return EventImagesModel;
}());
//# sourceMappingURL=EventImages.js.map

/***/ }),

/***/ "../../../../../src/app/model/EventParticipants.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventParticipantsModel; });
var EventParticipantsModel = (function () {
    function EventParticipantsModel() {
        this.eventId = null;
        this.eventCaption = null;
        this.mobileUserId = null;
        this.mobileUserNo = null;
        this.mobileUser = null;
        this.userEventStatus = null;
    }
    return EventParticipantsModel;
}());
//# sourceMappingURL=EventParticipants.js.map

/***/ }),

/***/ "../../../../../src/app/model/EventUserGroup.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventUserGroupModel; });
var EventUserGroupModel = (function () {
    function EventUserGroupModel() {
        this.userId = null;
        this.groupId = null;
        this.operation = null;
    }
    return EventUserGroupModel;
}());
//# sourceMappingURL=EventUserGroup.js.map

/***/ }),

/***/ "../../../../../src/app/model/Group.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__CMSUsers__ = __webpack_require__("../../../../../src/app/model/CMSUsers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupModel; });


var GroupModel = (function () {
    function GroupModel() {
        this.groupId = null;
        this.groupSystemId = null;
        this.groupTitle = null;
        this.groupIconPath = null;
        this.groupDescription = null;
        this.groupCountry = null;
        this.groupDistrict = null;
        this.groupCity = null;
        this.userInserted = null;
        this.dateInserted = null;
        this.userModified = null;
        this.dateModified = null;
        this.expiryDate = null;
        this.groupAdmin = new __WEBPACK_IMPORTED_MODULE_0__CMSUsers__["a" /* CMSUsersModel */]();
        this.groupPublicLevel = new __WEBPACK_IMPORTED_MODULE_1__MDCode__["a" /* MDCodeModel */]();
        this.groupStatus = new __WEBPACK_IMPORTED_MODULE_1__MDCode__["a" /* MDCodeModel */]();
    }
    return GroupModel;
}());
//# sourceMappingURL=Group.js.map

/***/ }),

/***/ "../../../../../src/app/model/LDapGroup.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LdapGroupModel; });
var LdapGroupModel = (function () {
    function LdapGroupModel() {
        // groupId = null;
        // functions = null;
        // groupName = null;
        // dn= null;
        this.groupId = null;
        this.functions = new Array();
        this.groupName = null;
        this.dn = null;
    }
    return LdapGroupModel;
}());
//# sourceMappingURL=LDapGroup.js.map

/***/ }),

/***/ "../../../../../src/app/model/LoggedInUserModel.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggedInUserModel; });
var LoggedInUserModel = (function () {
    function LoggedInUserModel() {
        this.userId = null;
        this.firstName = null;
        this.lastName = null;
        this.commonName = null;
        this.designation = null;
        this.extraParams = null;
        this.nic = null;
        this.telephoneNumber = null;
        this.email = null;
        this.status = null;
        this.password = null;
        this.newPassword = null;
        this.createdOn = null;
        this.createdBy = null;
        this.modifiedOn = null;
        this.modifiedBy = null;
        this.inactivedOn = null;
        this.inactivedBy = null;
        this.remarks = null;
        this.address = null;
        this.loginStatus = null;
        this.loginFailAttempts = null;
        this.lastLoginAccessedOn = null;
        this.lastLoggedoutOn = null;
        this.lastLoggedIPAddress = null;
        this.lastLoggedInSystem = null;
        this.lastLoggedInOn = null;
        this.lastAccessedUri = null;
        this.activateBy = null;
        this.activateOn = null;
        this.lastGeneratedKey = null;
        this.pwdChangedBy = null;
        this.pwdChangedOn = null;
        this.pwdFailureAttemptsAll = null;
        this.pwdResetBy = null;
        this.pwdResetOn = null;
        this.securityAnswer = null;
        this.securityQuestion = null;
        this.accountUnlockedBy = null;
        this.accountUnlockedOn = null;
        this.accountUnlockTime = null;
        this.dn = null;
        this.organization = null;
        this.division = null;
        this.countryCode = null;
        this.branch = null;
        this.department = null;
        this.room = null;
        this.groups = [];
        this.functions = [];
    }
    return LoggedInUserModel;
}());
//# sourceMappingURL=LoggedInUserModel.js.map

/***/ }),

/***/ "../../../../../src/app/model/MDCode.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MDCodeModel; });
var MDCodeModel = (function () {
    function MDCodeModel() {
        this.codeId = null;
        this.codeType = null;
        this.codeSubType = null;
        this.codeLocale = null;
        this.codeSeq = null;
        this.codeMessage = null;
    }
    return MDCodeModel;
}());
//# sourceMappingURL=MDCode.js.map

/***/ }),

/***/ "../../../../../src/app/model/MobileUser.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MobileUserModel; });
var MobileUserModel = (function () {
    function MobileUserModel() {
        this.mobileUserId = null;
        this.mobileUserMobileNo = null;
        this.mobileUserName = null;
        this.mobileUserEmail = null;
        this.mobileUserDob = null;
        this.mobileUserCountry = null;
        this.mobileUserProvince = null;
        this.mobileUserCity = null;
        this.mobileUserDateRegistration = null;
        this.mobileUserDateModified = null;
        this.mobileUserPassCode = null;
        this.majlisCategoryList = new Array();
        this.majlisGroupList = new Array();
    }
    return MobileUserModel;
}());
//# sourceMappingURL=MobileUser.js.map

/***/ }),

/***/ "../../../../../src/app/model/NewUserModel.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__LDapGroup__ = __webpack_require__("../../../../../src/app/model/LDapGroup.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewUserModel; });

var NewUserModel = (function () {
    function NewUserModel() {
        this.userId = null;
        this.firstName = null;
        this.lastName = null;
        this.commonName = null;
        this.designation = null;
        this.extraParams = null;
        this.nic = null;
        this.telephoneNumber = null;
        this.email = null;
        this.status = null;
        this.password = null;
        this.newPassword = null;
        this.createdOn = null;
        this.createdBy = null;
        this.modifiedOn = null;
        this.modifiedBy = null;
        this.inactivedOn = null;
        this.inactivedBy = null;
        this.remarks = null;
        this.address = null;
        this.loginStatus = null;
        this.loginFailAttempts = null;
        this.lastLoginAccessedOn = null;
        this.lastLoggedoutOn = null;
        this.lastLoggedIPAddress = null;
        this.lastLoggedInSystem = null;
        this.lastLoggedInOn = null;
        this.lastAccessedUri = null;
        this.activateBy = null;
        this.activateOn = null;
        this.lastGeneratedKey = null;
        this.pwdChangedBy = null;
        this.pwdChangedOn = null;
        this.pwdFailureAttemptsAll = null;
        this.pwdResetBy = null;
        this.pwdResetOn = null;
        this.securityAnswer = null;
        this.securityQuestion = null;
        this.accountUnlockedBy = null;
        this.accountUnlockedOn = null;
        this.accountUnlockTime = null;
        this.dn = null;
        this.organization = null;
        this.division = null;
        this.countryCode = null;
        this.branch = null;
        this.department = null;
        this.room = null;
        //  groups : Array<any> = [];
        this.groups = new __WEBPACK_IMPORTED_MODULE_0__LDapGroup__["a" /* LdapGroupModel */]();
        this.functions = [];
    }
    return NewUserModel;
}());
//# sourceMappingURL=NewUserModel.js.map

/***/ }),

/***/ "../../../../../src/app/model/Notification.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Event__ = __webpack_require__("../../../../../src/app/model/Event.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationModel; });



var NotificationModel = (function () {
    function NotificationModel() {
        this.notificationId = null;
        this.notificationSystemId = null;
        this.notificationIconPath = null;
        this.notificationMessage = null;
        this.dateInserted = null;
        this.notificationEventId = new __WEBPACK_IMPORTED_MODULE_2__Event__["a" /* EventModel */]();
        this.notificationGroupId = new __WEBPACK_IMPORTED_MODULE_1__Group__["a" /* GroupModel */]();
        this.notificationUserId = null;
        this.notificationStatus = new __WEBPACK_IMPORTED_MODULE_0__MDCode__["a" /* MDCodeModel */]();
    }
    return NotificationModel;
}());
//# sourceMappingURL=Notification.js.map

/***/ }),

/***/ "../../../../../src/app/model/NotificationSend.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationSendModel; });
var NotificationSendModel = (function () {
    function NotificationSendModel() {
        this.notificationId = 0;
        this.notificationSendTo = 0;
        this.notifictionType = 0;
    }
    return NotificationSendModel;
}());
//# sourceMappingURL=NotificationSend.js.map

/***/ }),

/***/ "../../../../../src/app/model/User.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__LoggedInUserModel__ = __webpack_require__("../../../../../src/app/model/LoggedInUserModel.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });

var User = (function () {
    function User() {
        this.expiryDate = null;
        this.allowedNotificationCnt = null;
        this.user = new __WEBPACK_IMPORTED_MODULE_0__LoggedInUserModel__["a" /* LoggedInUserModel */]();
    }
    return User;
}());
//# sourceMappingURL=User.js.map

/***/ }),

/***/ "../../../../../src/app/model/country.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountryModel; });
var CountryModel = (function () {
    function CountryModel() {
    }
    return CountryModel;
}());
//# sourceMappingURL=country.js.map

/***/ }),

/***/ "../../../../../src/app/model/loggedInUserModel.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggedInUserModel; });
var LoggedInUserModel = (function () {
    function LoggedInUserModel() {
        this.userId = null;
        this.firstName = null;
        this.lastName = null;
        this.commonName = null;
        this.designation = null;
        this.extraParams = null;
        this.nic = null;
        this.telephoneNumber = null;
        this.email = null;
        this.status = null;
        this.password = null;
        this.newPassword = null;
        this.createdOn = null;
        this.createdBy = null;
        this.modifiedOn = null;
        this.modifiedBy = null;
        this.inactivedOn = null;
        this.inactivedBy = null;
        this.remarks = null;
        this.address = null;
        this.loginStatus = null;
        this.loginFailAttempts = null;
        this.lastLoginAccessedOn = null;
        this.lastLoggedoutOn = null;
        this.lastLoggedIPAddress = null;
        this.lastLoggedInSystem = null;
        this.lastLoggedInOn = null;
        this.lastAccessedUri = null;
        this.activateBy = null;
        this.activateOn = null;
        this.lastGeneratedKey = null;
        this.pwdChangedBy = null;
        this.pwdChangedOn = null;
        this.pwdFailureAttemptsAll = null;
        this.pwdResetBy = null;
        this.pwdResetOn = null;
        this.securityAnswer = null;
        this.securityQuestion = null;
        this.accountUnlockedBy = null;
        this.accountUnlockedOn = null;
        this.accountUnlockTime = null;
        this.dn = null;
        this.organization = null;
        this.division = null;
        this.countryCode = null;
        this.branch = null;
        this.department = null;
        this.room = null;
        this.groups = [];
        this.functions = [];
    }
    return LoggedInUserModel;
}());
//# sourceMappingURL=loggedInUserModel.js.map

/***/ }),

/***/ "../../../../../src/app/notification/notification.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*.panel-heading{\r\n    background-color: #009688;\r\n    margin-top: 40px;\r\n}*/\r\n\r\n\r\n/*###############################################*/\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:5px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n\r\n.notif-style{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n.glyphicon-pencil{\r\n    color: #206ada;\r\n}\r\n\r\n/*toggle styles*/\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\r\n  margin-left: 60px;\r\n}\r\n\r\n.switch input {display:none;}\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked + .slider {\r\n  background-color: #009486;\r\n}\r\n\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #009486;\r\n}\r\n\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/notification/notification.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop></app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n<div class=\"container\">\r\n  <div class=\"page-title\">\r\n    <h2>Notification Management <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"waitForNotiCount && !isSuperAdmin\" style=\"width: 25px\" /> </span>      <span *ngIf=\"!waitForNotiCount && !isSuperAdmin\"> (Available Notifications - {{sentNoti}}/{{avilNoti}})</span></h2>\r\n  </div>\r\n  <div class=\"pull-right\"><button class=\"btn btn-success\" (click)=\"openAddNew()\">New Notification</button></div><br><br><br>\r\n  <div class=\"panel panel-default\">\r\n\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n    <img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n    <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n      <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n        <tr>\r\n          <th></th>\r\n          <th>Group ID</th>\r\n          <th>Event ID</th>\r\n          <!--<th>User ID</th>-->\r\n          <!--<th>Caption</th>-->\r\n          <th>Message</th>\r\n          <th>Status</th>\r\n          <th>Send</th>\r\n          <th>Edit</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let n of NotificationModelArr;\">\r\n          <td>\r\n            <img class=\"circle-image pull-left\" src=\"{{n.notificationIconPath}}\" />\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 notif-style\">\r\n              {{n.notificationGroupId.groupTitle}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 notif-style\">\r\n              {{n.notificationEventId.eventCaption}}\r\n            </div>\r\n          </td>\r\n          <!--<td>\r\n            <div class=\"col-sm-12 notif-style\">\r\n             {{n.notificationUserId.userId}}\r\n            </div>\r\n          </td>-->\r\n          <!--<td>\r\n            <div class=\"col-sm-12 notif-style\">\r\n              caption\r\n            </div>\r\n          </td>-->\r\n          <td>\r\n            <div class=\"col-sm-12 notif-style\">\r\n              {{n.notificationMessage}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <span class=\"fa fa-floppy-o\" *ngIf=\"n.notificationStatus.codeId== 26\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Active\"></span>\r\n            <!-- <span class=\"glyphicon glyphicon-remove\"  *ngIf=\"n.notificationStatus.codeId== 25\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Inactive\"></span> -->\r\n            <span class=\"glyphicon glyphicon-ok\" *ngIf=\"n.notificationStatus.codeId== 24\" data-toggle=\"tooltip\" data-placement=\"top\"\r\n              title=\"Draft\"></span>\r\n          </td>\r\n          <td>\r\n            <button class=\"btn btn-primary\" [disabled]=\"n.notificationStatus.codeId== 26\" (click)=\"openSendNotificationForm(n)\">\r\n              <i class=\"fa fa-paper-plane\"></i>\r\n            </button>\r\n          </td>\r\n          <td>\r\n            <button class=\"btn btn-primary\" [disabled]=\"n.notificationStatus.codeId== 24\" (click)=\"openSendNotificationEdit(n.notificationId)\">\r\n              <i class=\"fa fa-pencil\"></i>\r\n            </button>\r\n          </td>\r\n        </tr>\r\n\r\n\r\n      </tbody>\r\n    </table>\r\n\r\n\r\n  </div>\r\n\r\n\r\n  <!--######################New Notification Modal######################-->\r\n  <div bsModal #newNotification=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-lg\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <form>\r\n          <!--################ start form ############-->\r\n          <div class=\"modal-header\">\r\n            <button type=\"button\" class=\"close pull-right\" (click)=\"newNotification.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n            <h4 class=\"modal-title\">{{notificationTitleBar}} </h4>\r\n          </div>\r\n          <div class=\"modal-body\">\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Group</label>\r\n              <!--<input type=\"number\" name=\"groupId\" [(ngModel)]=\"NotificationModel.notificationGroupId\" class=\"form-control\" required>-->\r\n              <select class=\"form-control\" name=\"groupId\" (change)=\"selectEventsForGroups($event.target)\">\r\n                <option value=\"0\">Please select group</option>\r\n                <option *ngFor=\"let g of groupModel\" [selected]=\"g.groupId ==NotificationModel.notificationGroupId.groupId\"   [ngValue]=g>{{g.groupTitle}}</option>\r\n              </select>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"template\">Event ID</label>\r\n              <!--<input type=\"number\"  name=\"eventId\"  [(ngModel)]=\"NotificationModel.notificationEventId\" class=\"form-control\" required>-->\r\n              <select class=\"form-control\" name=\"eventId\" (change)=\"selectEvents($event.target)\">\r\n                <option value=\"0\">Please select event</option>\r\n                <option *ngFor=\"let e of eventModel\" [selected]=\"e.eventId ==NotificationModel.notificationEventId.eventId\"  [ngValue]=e>{{e.eventCaption}}</option>\r\n              </select>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n              <label for=\"comment\">Message</label>\r\n              <textarea class=\"form-control\" name=\"notificationMessage\" [(ngModel)]=\"NotificationModel.notificationMessage\" rows=\"5\" required></textarea>\r\n            </div>\r\n            <div style=\"text-align: right\">\r\n              <label class=\"radio-inline\">\r\n            <input type=\"radio\" name=\"optradio\" [value]= \"26\"  [(ngModel)]=\"notificationStatus\" [checked]=\"NotificationModel.notificationStatus.codeId==26\">Draft\r\n          </label>\r\n              <label class=\"radio-inline\">\r\n            <input type=\"radio\" name=\"optradio\" [value]= \"24\" [(ngModel)]=\"notificationStatus\" [checked]=\"NotificationModel.notificationStatus.codeId==24\">Active\r\n          </label>\r\n              <label class=\"radio-inline\">\r\n            <!-- <input type=\"radio\" name=\"optradio\" value = \"26\" [(ngModel)]=\"status\">Inactive -->\r\n          </label>\r\n            </div>\r\n            <br><br>\r\n            <div *ngIf=\"!editMode\">\r\n              <label class=\"control-label\">Select Icon</label>\r\n              <input type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n              <div class=\"col-md-12\" >\r\n\r\n                <table class=\"table pull-right\" style=\"margin-bottom: 5px;width: 100%\">\r\n                  <thead>\r\n                    <tr>\r\n                    </tr>\r\n                  </thead>\r\n                  <tbody>\r\n                    <tr *ngFor=\"let item of uploader.queue\">\r\n\r\n                      <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                      <td *ngIf=\"uploader.isHTML5\">\r\n                        <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                          <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                        </div>\r\n                      </td>\r\n                      <td class=\"text-center\" style=\"text-align: right\">\r\n                        <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                        <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                        <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                      </td>\r\n                      <td nowrap>\r\n                        <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n                        <button type=\"button\" class=\"btn btn-danger btn-xs\" (click)=\"item.remove()\">\r\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                        </button>\r\n                      </td>\r\n                      <!--<td>\r\n                      <!--<input type=\"file\" (change)=\"fileChangeEvent($event)\" placeholder=\"Upload file...\" />-->\r\n                      <!--<input type=\"file\" onchange=\"previewFile()\"><br>-->\r\n                      <!--<img src=\"\" height=\"200\" alt=\"Image preview...\">-->\r\n                      <!--</td>-->\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n              <div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n\r\n\r\n          </div>\r\n\r\n          <div class=\"modal-footer\">\r\n            <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"showAddNewWindow\" style=\"width: 25px\" /> </span>\r\n            <button type=\"submit\" *ngIf=\"!editMode\" class=\"btn btn-success\" (click)=\"saveNewNotification()\">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>\r\n            <button type=\"submit\" *ngIf=\"editMode\" class=\"btn btn-success\" [disabled]=\"showAddNewWindow\" (click)=\"saveNewNotificationChanges()\">&nbsp;&nbsp;Update&nbsp;&nbsp;</button>\r\n\r\n            <button type=\"button\" class=\"btn btn-default\" (click)=\"newNotification.hide()\">Close</button>\r\n\r\n          </div>\r\n        </form>\r\n        <!--################## end form ###############-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <!--###########SendNotification modal#############-->\r\n  <div bsModal #sendNotification=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <div class=\"modal-header\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"sendNotification.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Send Notification</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <div align=\"center\">\r\n            <button class=\"btn btn-success\" (click)=\"sendTo(17)\" style=\"padding-top:20px;padding-bottom:20px\">Send to Group</button>\r\n            <button class=\"btn btn-success\" (click)=\"sendTo(18)\" style=\"padding-top:20px;padding-bottom:20px\">Send to Event</button>\r\n            <button class=\"btn btn-success\" (click)=\"sendTo(19)\" style=\"padding-top:20px;padding-bottom:20px\">Send to Attendees</button>\r\n            <button class=\"btn btn-success\" (click)=\"sendTo(20)\" style=\"padding-top:20px;padding-bottom:20px\">Send to non Attendees</button>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"sendNotification.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div bsModal #confirmation=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <div class=\"modal-header\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"sendNotification.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n          <h4 class=\"modal-title\">Send Notification</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <p>This may affect your total notification count. Are you sure?</p>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n          <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"confirmLoader\" style=\"width: 25px\" /> </span>\r\n\r\n          <button type=\"button\" class=\"btn btn-success\" [disabled]=\"confirmLoader\" (click)=\"sendToConfirm()\">Sure</button>\r\n\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"confirmation.hide()\">Close</button>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n<!-- ============================================================= -->\r\n\r\n<div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-md\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-header\" style=\"background-color: #449D44; color: azure\">\r\n        <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n        <h4 class=\"modal-title\">Operation Completed</h4>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>{{successMessage}}</p>\r\n\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<div bsModal #fail=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-md\">\r\n\r\n    <div class=\"modal-content\">\r\n\r\n      <div class=\"modal-header\" style=\"background-color: chocolate;color: azure\">\r\n        <button type=\"button\" class=\"close pull-right\" (click)=\"fail.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n        <h4 class=\"modal-title\">Operation Failed!</h4>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <p>{{failMessage}}</p>\r\n\r\n      </div>\r\n\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-default\" (click)=\"fail.hide()\">Close</button>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/notification/notification.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_Notification__ = __webpack_require__("../../../../../src/app/model/Notification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_CMSUsers__ = __webpack_require__("../../../../../src/app/model/CMSUsers.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__model_MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__model_NotificationSend__ = __webpack_require__("../../../../../src/app/model/NotificationSend.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__alert_alert_component__ = __webpack_require__("../../../../../src/app/alert/alert.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var NotificationComponent = (function () {
    function NotificationComponent(commonService, validator, alert) {
        this.commonService = commonService;
        this.validator = validator;
        this.alert = alert;
        this.fakeArray = new Array(12);
        this.NotificationModelArr = new Array();
        this.arrTemplates = [];
        this.NotificationModel = new __WEBPACK_IMPORTED_MODULE_2__model_Notification__["a" /* NotificationModel */]();
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-NotificationManagement-1.0/service/notification/saveNotificationIcon", queueLimit: 1 });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.showLoader = true;
        this.confirmLoader = false;
        this.successMessage = "";
        this.groupId2 = 0;
        this.showAddNewWindow = true;
        this.notificationTitleBar = "New Notification";
        this.editMode = false;
        this.selectedNoitificationObj = new __WEBPACK_IMPORTED_MODULE_2__model_Notification__["a" /* NotificationModel */]();
        this.waitForNotiCount = true;
        this.CMSUser = new __WEBPACK_IMPORTED_MODULE_5__model_CMSUsers__["a" /* CMSUsersModel */]();
        this.groupModel = new Array();
        this.eventModel = new Array();
        this.validator.validateUser("notification");
    }
    NotificationComponent.prototype.ngOnInit = function () {
        this.isSuperAdmin = this.validator.user.groups[0].groupId == 'SuperAdminGrp';
        this.loadNotificationCount();
        this.loadToGrid();
    };
    NotificationComponent.prototype.loadNotificationCount = function () {
        var _this = this;
        this.waitForNotiCount = true;
        var lotificationCountUrl;
        console.log("validator 0 ", this.validator.user);
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            console.log("validator 1 ", this.validator.user);
            lotificationCountUrl = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "groups/getCount";
        }
        else {
            console.log("validator 2 ", this.validator.user);
            lotificationCountUrl = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "groups/getCount?userId=" + this.validator.getUserId();
        }
        this.commonService.processGet(lotificationCountUrl).subscribe(function (respose) {
            var res = respose.getCount.responseData;
            _this.avilNoti = res.tot_notification_limit;
            _this.sentNoti = res.tot_notification;
            if (_this.avilNoti == -1) {
                _this.avilNoti = "Unlimited";
            }
            _this.waitForNotiCount = false;
        }, function (error) {
        });
    };
    NotificationComponent.prototype.openSendNotificationEdit = function (notificationId) {
        var _this = this;
        this.editMode = true;
        this.notificationTitleBar = "Edit Notification";
        var notification = this.NotificationModelArr.filter(function (e) {
            return e.notificationId == notificationId;
        })[0];
        console.log("notification ", notification);
        this.NotificationModel = notification;
        this.loadGroupsToList(0, 5000);
        this.newNotification.show();
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventsByGroupId?groupId=" + notification.notificationGroupId.groupId;
        // let url = "http://192.0.0.59:8080/Majlis-EventManagement_merge-1.0/service/events/getEventsByGroupId?groupId="+ this.groupId2;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            //this.groupModel = res.getGroupList.responseData;
            _this.eventModel = res.getEventsByGroupId.responseData;
            console.log("events ", _this.eventModel);
        }, function (error) {
            console.log("error ", error);
        });
    };
    NotificationComponent.prototype.loadToGrid = function () {
        var _this = this;
        var urlPublic;
        // let urlPublic = "http://192.0.0.59:8080/Majlis-NotificationManagement-1.0/service/notification/getAllNotificationList";
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            urlPublic = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "notification/getAllNotificationList";
        }
        else {
            urlPublic = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "notification/getAllNotificationList?userId=" + this.validator.getUserId();
        }
        this.commonService.processGet(urlPublic).subscribe(function (res) {
            console.log("res", res);
            var response = res.getAllNotificationList;
            if (response.responseCode != 999) {
                _this.NotificationModelArr = response.responseData;
                _this.showLoader = false;
            }
            console.log("NotificationModelArr", _this.NotificationModelArr);
            // this.loadGroupsToList(0, 50);
        }, function (error) {
            console.log("console ", error);
        });
        this.uploader.onAfterAddingFile = function (file) {
            _this.showAddNewWindow = true;
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.showAddNewWindow = false;
            console.log("aaa", responsePath);
            _this.NotificationModel.notificationIconPath = responsePath.responseData;
        };
    };
    NotificationComponent.prototype.selectEvents = function (target) {
        this.NotificationModel.notificationEventId = this.eventModel.filter(function (e) {
            return e.eventCaption == target.value;
        })[0];
        if (this.editMode == true) {
        }
        else {
        }
    };
    NotificationComponent.prototype.saveNewNotification = function (evt) {
        var _this = this;
        this.showAddNewWindow = true;
        var header = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        //  let reader=new FileReader();
        console.log("saveNewNotification ", this.NotificationModel);
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "notification/createNotification";
        this.CMSUser.userId = this.validator.getUserId();
        this.NotificationModel.notificationUserId = this.CMSUser;
        // let status:MDCodeModel;
        // status.codeId =  this.status;//parseInt(this.status);
        // console.log("status",this.status);
        this.NotificationModel.notificationStatus = new __WEBPACK_IMPORTED_MODULE_7__model_MDCode__["a" /* MDCodeModel */]();
        this.NotificationModel.notificationStatus.codeId = this.notificationStatus;
        this.commonService.processPostWithHeaders(url, this.NotificationModel, option).subscribe(function (res) {
            var response = res.createNotification;
            _this.showAddNewWindow = false;
            if (response.responseCode == 1) {
                _this.loadToGrid();
                _this.newNotification.hide();
                _this.successMessage = "Notification has been saved!";
                _this.success.show();
            }
            else {
                _this.loadToGrid();
                _this.failMessage = "Cannot create notification at this time. Please try agian later!";
                _this.fail.show();
            }
        }, function (error) {
            _this.loadToGrid();
            console.log("error >>> ", error);
            _this.showAddNewWindow = false;
            _this.failMessage = "Cannot connect to the server.";
            _this.fail.show();
        });
    };
    NotificationComponent.prototype.saveNewNotificationChanges = function () {
        var _this = this;
        this.showAddNewWindow = true;
        this.NotificationModel.notificationStatus = new __WEBPACK_IMPORTED_MODULE_7__model_MDCode__["a" /* MDCodeModel */]();
        this.NotificationModel.notificationStatus.codeId = this.notificationStatus;
        console.log(">>> ", this.NotificationModel);
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "notification/updateNotificationCms";
        this.commonService.processPost(url, this.NotificationModel).subscribe(function (res) {
            _this.isSuccess = true;
            console.log("Notification send", res);
            var response = res.updateNotificationCms;
            if (response.responseCode == 1) {
                _this.successMessage = "Notification send!";
                _this.newNotification.hide();
                _this.successMessage = "Notification has been updated!";
                _this.success.show();
            }
            else {
                _this.loadToGrid();
                _this.failMessage = "Cannot save the notification at this time. Trye agin later!";
                _this.fail.show();
            }
            _this.showAddNewWindow = false;
        }, function (error) {
            _this.failMessage = "Cannot save the notification at this time. Trye agin later!";
            _this.fail.show();
            _this.loadToGrid();
            _this.showAddNewWindow = false;
        });
    };
    NotificationComponent.prototype.sendToConfirm = function () {
        var _this = this;
        this.confirmLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "notification/createNotificationDetail";
        var notifi = new __WEBPACK_IMPORTED_MODULE_9__model_NotificationSend__["a" /* NotificationSendModel */]();
        var type = this.sendType;
        console.log("notifi", this.NotificationModel);
        notifi.notificationId = this.NotificationModel.notificationId;
        if (type == 17) {
            notifi.notificationSendTo = this.NotificationModel.notificationGroupId.groupId;
        }
        else if (type == 18 || type == 19 || type == 20) {
            notifi.notificationSendTo = this.NotificationModel.notificationEventId.eventId;
        }
        var header = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("userGroup", this.validator.user.groups[0].groupId);
        option.headers = header;
        notifi.notifictionType = type;
        this.commonService.processPostWithHeaders(url, notifi, option).subscribe(function (res) {
            _this.isSuccess = true;
            console.log("Notification send", res);
            try {
                var response = res.createNotificationDetail;
                _this.confirmation.hide();
                if (response.responseCode == 1) {
                    _this.successMessage = "Notification has been sent to the relevant parties";
                    _this.success.show();
                }
                else if (response.responseCode == 2) {
                    _this.failMessage = "Operation failed! You have exceeded your allowed notification quota";
                    _this.fail.show();
                }
                else {
                    _this.failMessage = "Operation cannot complete at this time. Please try agian later";
                    _this.fail.show();
                }
                _this.loadNotificationCount();
                _this.loadToGrid();
                _this.confirmLoader = false;
            }
            catch (error) {
                _this.confirmation.hide();
                _this.failMessage = "Cannot connect to the server.";
                _this.fail.show();
            }
        }, function (error) {
            console.log("error >>> ", error);
            _this.confirmation.hide();
            _this.failMessage = "Cannot connect to the server.";
            _this.fail.show();
        });
    };
    NotificationComponent.prototype.sendTo = function (type) {
        this.sendType = type;
        this.sendNotification.hide();
        this.confirmation.show();
    };
    NotificationComponent.prototype.openSendNotificationForm = function (selectedNoitificationObj) {
        this.NotificationModel = selectedNoitificationObj;
        this.sendNotification.show();
    };
    NotificationComponent.prototype.loadGroupsToList = function (start, limit) {
        var _this = this;
        var url;
        if (this.validator.user.groups[0].groupId == "SuperAdminGrp") {
            url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "groups/getAllGroupsById";
        }
        else {
            url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "groups/getAllGroupsById?user=" + this.validator.getUserId();
        }
        // let url = "http://192.0.0.69:9090/Majlis-GroupManagement-1.0/service/groups/getAllGroupsById?userId=1";
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            _this.groupModel = res.getAllGroupsById.responseData;
            // this.groupModel = res.responseData;
            console.log("response api ", _this.groupModel);
            _this.showAddNewWindow = false;
        }, function (error) {
            console.log("error ", error);
        });
    };
    NotificationComponent.prototype.selectEventsForGroups = function (target) {
        var _this = this;
        this.showAddNewWindow = true;
        this.NotificationModel.notificationGroupId = this.groupModel.filter(function (e) {
            return e.groupTitle == target.value;
        })[0];
        console.log("notification mode", this.NotificationModel);
        this.groupId2 = this.NotificationModel.notificationGroupId.groupId;
        console.log("groupId if", this.groupId2);
        // this.groupId =5;
        var url = __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].BASE_PATH + "events/getEventsByGroupId?groupId=" + this.groupId2;
        // let url = "http://192.0.0.59:8080/Majlis-EventManagement_merge-1.0/service/events/getEventsByGroupId?groupId="+ this.groupId2;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log("response ", res);
            //this.groupModel = res.getGroupList.responseData;
            _this.eventModel = res.getEventsByGroupId.responseData;
            console.log("events ", _this.eventModel);
            _this.showAddNewWindow = false;
        }, function (error) {
            _this.showAddNewWindow = false;
        });
    };
    NotificationComponent.prototype.openAddNew = function () {
        var _this = this;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_4__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-NotificationManagement-1.0/service/notification/saveNotificationIcon", queueLimit: 1 });
        this.uploader.onAfterAddingFile = function (file) {
            _this.showAddNewWindow = true;
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            _this.showAddNewWindow = false;
            console.log("aaa", responsePath);
            _this.NotificationModel.notificationIconPath = responsePath.responseData;
        };
        this.NotificationModel = new __WEBPACK_IMPORTED_MODULE_2__model_Notification__["a" /* NotificationModel */]();
        console.log("notification model", this.NotificationModel);
        this.editMode = false;
        this.loadGroupsToList(0, 5000);
        this.notificationTitleBar = "Add New Notification";
        this.newNotification.show();
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("sendNotification"), 
        __metadata('design:type', Object)
    ], NotificationComponent.prototype, "sendNotification", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("confirmation"), 
        __metadata('design:type', Object)
    ], NotificationComponent.prototype, "confirmation", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], NotificationComponent.prototype, "success", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fail"), 
        __metadata('design:type', Object)
    ], NotificationComponent.prototype, "fail", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("newNotification"), 
        __metadata('design:type', Object)
    ], NotificationComponent.prototype, "newNotification", void 0);
    NotificationComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__("../../../../../src/app/notification/notification.component.html"),
            styles: [__webpack_require__("../../../../../src/app/notification/notification.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_2__model_Notification__["a" /* NotificationModel */], __WEBPACK_IMPORTED_MODULE_10__alert_alert_component__["a" /* AlertComponent */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_10__alert_alert_component__["a" /* AlertComponent */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_10__alert_alert_component__["a" /* AlertComponent */]) === 'function' && _c) || Object])
    ], NotificationComponent);
    return NotificationComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=notification.component.js.map

/***/ }),

/***/ "../../../../../src/app/pagetop/pagetop.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navbar-fixed-left {\r\n  width: 140px;\r\n  position: fixed;\r\n  border-radius: 0;\r\n  height: 100%;\r\n}\r\n\r\n.navbar-fixed-left .navbar-nav > li {\r\n  float: none; \r\n  width: 139px;\r\n}\r\n\r\n.navbar-fixed-left + .container {\r\n  padding-left: 160px;\r\n}\r\n.navbar-brand{\r\n  margin-top: -11px;\r\n}\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 30px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n.navbar-brand {\r\n  margin-left: 35px;\r\n}\r\n.navbar-right{\r\n  margin-right:35px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/pagetop/pagetop.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<nav class=\"navbar navbar-inverse navbar-static-top navbar-fixed-top\">\r\n  <div class=\"navbar-brand text-hide\" href=\"#\"><img src=\"assets/img/MajlisLogoNav.png\"></div>\r\n  <ul class=\"nav navbar-nav navbar-right\">\r\n\r\n    <li><a href=\"#\">{{userId}}</a></li>\r\n    <li><a href=\"#\"><span class=\"glyphicon glyphicon-log-out\"></span> Log Out</a></li>\r\n  </ul>\r\n</nav>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/pagetop/pagetop.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagetopComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagetopComponent = (function () {
    function PagetopComponent(validator) {
        this.validator = validator;
    }
    PagetopComponent.prototype.ngOnInit = function () {
        this.userId = this.validator.getUserId();
    };
    PagetopComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pagetop',
            template: __webpack_require__("../../../../../src/app/pagetop/pagetop.component.html"),
            styles: [__webpack_require__("../../../../../src/app/pagetop/pagetop.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_login_validator__["a" /* LoignValidator */]) === 'function' && _a) || Object])
    ], PagetopComponent);
    return PagetopComponent;
    var _a;
}());
//# sourceMappingURL=pagetop.component.js.map

/***/ }),

/***/ "../../../../../src/app/service/common-web.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonWebService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CommonWebService = (function () {
    function CommonWebService(http) {
        this.http = http;
    }
    /**
     *
     * @param url - Destination URL
     */
    CommonWebService.prototype.processGet = function (url) {
        return this.http.get(url).map(this.extractData);
    };
    /**
     *
     * @param url
     * @param options - Options with header params. If there is no header put null
     */
    CommonWebService.prototype.processGetWithHeaders = function (url, options) {
        return this.http.get(url, options).map(this.extractData);
    };
    /**
     *
     * @param url - Destination URL
     * @param query - JSON query of request payload
     */
    CommonWebService.prototype.processPost = function (url, query) {
        return this.http.post(url, query).map(this.extractData);
    };
    /**
     *
     * @param url
     * @param query
     * @param options - Option with header params. If there is no header put null
     */
    CommonWebService.prototype.processPostWithHeaders = function (url, query, options) {
        return this.http.post(url, query, options).map(this.extractData);
    };
    CommonWebService.prototype.extractData = function (res) {
        return res.json() || {};
    };
    CommonWebService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _a) || Object])
    ], CommonWebService);
    return CommonWebService;
    var _a;
}());
//# sourceMappingURL=common-web.service.js.map

/***/ }),

/***/ "../../../../../src/app/service/login-validator.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoignValidator; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoignValidator = (function () {
    function LoignValidator(router) {
        this.router = router;
    }
    LoignValidator.prototype.getUserId = function () {
        var userToken = sessionStorage.getItem('id_token');
        this.user = JSON.parse(atob(userToken));
        return this.user.userId;
    };
    LoignValidator.prototype.validateUser = function (menuName) {
        var userToken = sessionStorage.getItem('id_token');
        if (userToken == null) {
            this.router.navigate(['./login']);
        }
        else {
            this.user = JSON.parse(atob(userToken));
            var index = this.user.functions.findIndex(function (e) { return e.menuAction == menuName; });
            if (index == -1 && this.getUserId() == null) {
                this.router.navigate(['./login']);
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    };
    LoignValidator = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _a) || Object])
    ], LoignValidator);
    return LoignValidator;
    var _a;
}());
//# sourceMappingURL=login-validator.js.map

/***/ }),

/***/ "../../../../../src/app/shared-service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SharedService = (function () {
    function SharedService() {
    }
    SharedService.prototype.getLoggedInUsers = function () {
        return this.getLoggedInUsers;
    };
    SharedService.prototype.setLoggedInUserst = function (userData) {
        this.userData = userData;
    };
    SharedService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [])
    ], SharedService);
    return SharedService;
}());
//# sourceMappingURL=shared-service.js.map

/***/ }),

/***/ "../../../../../src/app/side-bar/side-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "    body,html{\r\n\t\theight: 100%;\r\n\t}\r\n\r\n\t/* remove outer padding */\r\n\t.main .row{\r\n\t\tpadding: 0px;\r\n\t\tmargin: 0px;\r\n\t}\r\n\r\n\t/*Remove rounded coners*/\r\n\r\n\tnav.sidebar.navbar {\r\n\t\tborder-radius: 0px;\r\n\t}\r\n\r\n\tnav.sidebar, .main{\r\n\t transition: margin 200ms ease-out;\r\n\t}\r\n\r\n\t/* Add gap to nav and right windows.*/\r\n\t.main{\r\n\t\tpadding: 10px 10px 0 10px;\r\n\t}\r\n\r\n\t/* .....NavBar: Icon only with coloring/layout.....*/\r\n\r\n\t/*small/medium side display*/\r\n\t@media (min-width: 768px) {\r\n\r\n\t\t/*Allow main to be next to Nav*/\r\n\t\t.main{\r\n\t\t\tposition: absolute;\r\n\t\t\twidth: calc(100% - 40px); /*keeps 100% minus nav size*/\r\n\t\t\tmargin-left: 40px;\r\n\t\t\tfloat: right;\r\n\t\t}\r\n\r\n\t\t/*lets nav bar to be showed on mouseover*/\r\n\t\tnav.sidebar:hover + .main{\r\n\t\t\tmargin-left: 200px;\r\n\t\t}\r\n\r\n\t\t/*Center Brand*/\r\n\t\tnav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {\r\n\t\t\tmargin-left: 0px;\r\n\t\t}\r\n\t\t/*Center Brand*/\r\n\t\tnav.sidebar .navbar-brand, nav.sidebar .navbar-header{\r\n\t\t\ttext-align: center;\r\n\t\t\twidth: 100%;\r\n\t\t\tmargin-left: 0px;\r\n\t\t}\r\n\r\n\t\t/*Center Icons*/\r\n\t\tnav.sidebar a{\r\n\t\t\tpadding-right: 13px;\r\n            margin-top: 15px;\r\n            margin-bottom: 15px;\r\n\t\t}\r\n\r\n\t\t/*adds border top to first nav box */\r\n\t\t/*nav.sidebar .navbar-nav > li:first-child{\r\n\t\t\tborder-top: 1px #e5e5e5 solid;\r\n\t\t}*/\r\n\r\n\t\t/*adds border to bottom nav boxes*/\r\n\t\tnav.sidebar .navbar-nav > li{\r\n\t\t\tborder-bottom: 1px #333 solid;\r\n\t\t}\r\n\r\n\t\t/* Colors/style dropdown box*/\r\n\t\tnav.sidebar .navbar-nav .open .dropdown-menu {\r\n\t\t\tposition: static;\r\n\t\t\tfloat: none;\r\n\t\t\twidth: auto;\r\n\t\t\tmargin-top: 0;\r\n\t\t\tbackground-color: transparent;\r\n\t\t\tborder: 0;\r\n\t\t\tbox-shadow: none;\r\n\t\t}\r\n\r\n\t\t/*allows nav box to use 100% width*/\r\n\t\tnav.sidebar .navbar-collapse, nav.sidebar .container-fluid{\r\n\t\t\tpadding: 0 0px 0 0px;\r\n            height:1000px;\r\n            \r\n\t\t}\r\n\r\n\t\t/*colors dropdown box text */\r\n\t\t.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {\r\n\t\t\tcolor: #777;\r\n\t\t}\r\n\r\n\t\t/*gives sidebar width/height*/\r\n\t\tnav.sidebar{\r\n\t\t\twidth: 210px;\r\n\t\t\theight: 100%;\r\n\t\t\tmargin-left: -160px;\r\n\t\t\tfloat: left;\r\n\t\t\tz-index: 8000;\r\n\t\t\tmargin-bottom: 0px;\r\n            margin-top: 50px;\r\n\t\t}\r\n\r\n\t\t/*give sidebar 100% width;*/\r\n\t\tnav.sidebar li {\r\n\t\t\twidth: 100%;\r\n\t\t}\r\n\r\n\t\t/* Move nav to full on mouse over*/\r\n\t\tnav.sidebar:hover{\r\n\t\t\tmargin-left: 0px;\r\n\t\t}\r\n\t\t/*for hiden things when navbar hidden*/\r\n\t\t.forAnimate{\r\n\t\t\topacity: 0;\r\n\t\t}\r\n\t}\r\n\r\n\r\n    \r\n\r\n\tnav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {\r\n\t\tcolor: #CCC;\r\n\t\tbackground-color: transparent;\r\n        cursor: pointer;\r\n\t}\r\n\r\n\tnav:hover .forAnimate{\r\n\t\topacity: 1;\r\n\t}\r\n\tsection{\r\n\t\tpadding-left: 15px;\r\n\t}\r\n    .dashIcon{\r\n        margin-top:-5px\r\n    }\r\n\r\n\r\n.active{\r\n\tbackground: #009688;\r\n}\r\n.active:hover{\r\n\tbackground: #009688;\r\n}\r\n.active:focus{\r\n\tbackground: #009688;\r\n}\r\n.active a{\r\n\tbackground: #009688;\r\n}\r\n.active a:hover{\r\n\tbackground: #009688;\r\n}\r\n.active a:focus{\r\n\tbackground: #009688;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/side-bar/side-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"navbar navbar-inverse navbar-fixed-left\">\r\n  <a class=\"navbar-brand\" href=\"\">Brand</a>\r\n  <ul class=\"nav navbar-nav\">\r\n\r\n\r\n<div class=\"list-group\">\r\n  <button type=\"button\" class=\"list-group-item\" (click)=\"btnClick();\"><span class=\"glyphicon glyphicon-user\">User Management</span></button>\r\n  <button type=\"button\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-list-alt\" (click)=btnCat();>Category Management</span></button>\r\n  <button type=\"button\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-dashboard\" (click)=btnGrp();>Group Management</span></button>\r\n  <button type=\"button\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-list-alt\" (click)=btnEvt();>Event Management</span></button>\r\n  <button type=\"button\" class=\"list-group-item\"><span class=\"glyphicon glyphicon-list-alt\" (click)=btnTmplt();>Template Management</span></button>\r\n</div>\r\n\r\n  </ul>\r\n\r\n </div>-->\r\n\r\n\r\n\r\n\r\n<nav class=\"navbar navbar-inverse navbar-fixed-top sidebar\" role=\"navigation\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Collect the nav links, forms, and other content for toggling -->\r\n\r\n\r\n    <div class=\"collapse navbar-collapse\" id=\"bs-sidebar-navbar-collapse-1\">\r\n      <ul class=\"nav navbar-nav\" style=\"width: 100%\">\r\n        <li *ngIf=\"getPermission('dashboard')\" routerLinkActive=\"active\"><a [routerLink]=\"['/dashboard']\">Dashboard<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisDashboard.png\"  width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('user-management')\" routerLinkActive=\"active\"><a [routerLink]=\"['/user-management']\">Users<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisUser.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('mobileuser')\" routerLinkActive=\"active\"><a [routerLink]=\"['/mobileuser']\">Mobile Users<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisMobile.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('group-management')\" routerLinkActive=\"active\"><a [routerLink]=\"['/group-management']\">Groups<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisGroup.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('category-management')\" routerLinkActive=\"active\"><a [routerLink]=\"['/category-management']\">Categories<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisCategory.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('event-management')\" routerLinkActive=\"active\"><a [routerLink]=\"['/event-management']\">Events<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisEvent.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('template-management')\" routerLinkActive=\"active\"><a [routerLink]=\"['/template-management']\">Templates<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisTemplate.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('notification')\" routerLinkActive=\"active\"><a [routerLink]=\"['/notification']\">Notifications<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisNotification.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n        <li *ngIf=\"getPermission('custom-reports')\" routerLinkActive=\"active\"><a [routerLink]=\"['/custom-reports']\">Reports<span class=\"pull-right hidden-xs showopacity\"><img class=\"dashIcon\" src=\"assets/img/icons/MajlisReport.png\" height=\"30px\" width=\"30px\"/></span></a></li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>"

/***/ }),

/***/ "../../../../../src/app/side-bar/side-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideBarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SideBarComponent = (function () {
    function SideBarComponent(router) {
        // this.list = [
        //    'MajlisDashboard',
        //    'MajlisUser',
        //    'MajlisGroup',
        //    'MajlisCategory',
        //    'MajlisEvent',
        //    'MajlisTemplate',
        //    'MajlisNotification',
        //    'MajlisReport'
        // ];
        this.router = router;
        this.btnClickDash = function () {
            console.log('Dashboard');
            // this.router.navigateByUrl('/usermgt');
            this.router.navigate(['./dash']);
        };
        this.btnClickUser = function () {
            console.log('User Mgt');
            // this.router.navigateByUrl('/usermgt');
            this.router.navigate(['./usermgt']);
        };
        this.btnGrp = function () {
            console.log('Grp Mgt');
            this.router.navigate(['./grpmngmnt']);
        };
        this.btnCat = function () {
            console.log('Cat Mgt');
            this.router.navigate(['./catgrymngmnt']);
        };
        this.btnEvt = function () {
            console.log('Evt Mgt');
            this.router.navigate(['./evntmngmnt']);
        };
        this.btnTmplt = function () {
            console.log('Tmplte Mgt');
            this.router.navigate(['./templatemngmnt']);
        };
        this.btnNotif = function () {
            console.log('Tmplte Mgt');
            this.router.navigate(['./templatemngmnt']);
        };
        this.btnReport = function () {
            console.log('Tmplte Mgt');
            this.router.navigate(['./templatemngmnt']);
        };
        // this.methodName = [
        //    'btnClickDash',
        //    'btnClickUser',
        //    'btnGrp',
        //    'btnCat',
        //    'btnEvt',
        //    'btnTmplt',
        //    'btnNotif',
        //    'btnReport'
        // ];
        //     this.list={
        //       "iconName" : 'MajlisDashboard',
        //       "methodName" : 'btnClickDash',
        //       "dashTitle" : 'Dashboard'
        //     };
        // console.log(this.list);
        // let obj = this.list;
        // console.log('###################################',obj.iconName);
        var userToken = sessionStorage.getItem('id_token');
        this.user = JSON.parse(atob(userToken));
        console.log("user model", this.user);
    }
    SideBarComponent.prototype.getPermission = function (menuName) {
        var index = this.user.functions.findIndex(function (e) { return e.menuAction == menuName; });
        if (index == -1) {
            return false;
        }
        else {
            return true;
        }
    };
    SideBarComponent.prototype.select = function (item) {
        this.selected = item;
    };
    ;
    SideBarComponent.prototype.isActive = function (item) {
        return this.selected === item;
    };
    ;
    SideBarComponent.prototype.ngOnInit = function () {
    };
    SideBarComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-side-bar',
            template: __webpack_require__("../../../../../src/app/side-bar/side-bar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/side-bar/side-bar.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _a) || Object])
    ], SideBarComponent);
    return SideBarComponent;
    var _a;
}());
//# sourceMappingURL=side-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/styles/loaders.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.cssload-container{\r\n\tposition:relative;\r\n}\r\n\t\r\n.cssload-whirlpool,\r\n.cssload-whirlpool::before,\r\n.cssload-whirlpool::after {\r\n\tposition: absolute;\r\n\ttop: 50%;\r\n\tleft: 50%;\r\n\tborder: 1px solid rgb(204,204,204);\r\n\tborder-left-color: rgb(0,0,0);\r\n\tborder-radius: 974px;\r\n\t\t-o-border-radius: 974px;\r\n\t\t-ms-border-radius: 974px;\r\n\t\t-webkit-border-radius: 974px;\r\n\t\t-moz-border-radius: 974px;\r\n}\r\n\r\n.cssload-whirlpool {\r\n\tmargin: -24px 0 0 -24px;\r\n\theight: 49px;\r\n\twidth: 49px;\r\n\tanimation: cssload-rotate 1150ms linear infinite;\r\n\t\t-o-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-ms-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-webkit-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-moz-animation: cssload-rotate 1150ms linear infinite;\r\n}\r\n\r\n.cssload-whirlpool::before {\r\n\tcontent: \"\";\r\n\tmargin: -22px 0 0 -22px;\r\n\theight: 43px;\r\n\twidth: 43px;\r\n\tanimation: cssload-rotate 1150ms linear infinite;\r\n\t\t-o-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-ms-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-webkit-animation: cssload-rotate 1150ms linear infinite;\r\n\t\t-moz-animation: cssload-rotate 1150ms linear infinite;\r\n}\r\n\r\n.cssload-whirlpool::after {\r\n\tcontent: \"\";\r\n\tmargin: -28px 0 0 -28px;\r\n\theight: 55px;\r\n\twidth: 55px;\r\n\tanimation: cssload-rotate 2300ms linear infinite;\r\n\t\t-o-animation: cssload-rotate 2300ms linear infinite;\r\n\t\t-ms-animation: cssload-rotate 2300ms linear infinite;\r\n\t\t-webkit-animation: cssload-rotate 2300ms linear infinite;\r\n\t\t-moz-animation: cssload-rotate 2300ms linear infinite;\r\n}\r\n\r\n\r\n\r\n@keyframes cssload-rotate {\r\n\t100% {\r\n\t\t-webkit-transform: rotate(360deg);\r\n\t\t        transform: rotate(360deg);\r\n\t}\r\n}\r\n\r\n@-webkit-keyframes cssload-rotate {\r\n\t100% {\r\n\t\t-webkit-transform: rotate(360deg);\r\n\t}\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/templatemangmnt/templatemangmnt.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    /*position: absolute;*/\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n\r\n.temp-title{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.temp-msg{\r\n    color: #6f7982;\r\n}\r\n.btn-primary{\r\n\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n/*modal*/\r\n.modal-body {\r\n    max-height: calc(100vh - 210px);\r\n    overflow-y: auto;\r\n}\r\n\r\n/*toggle styles*/\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\r\n  margin-left: 60px;\r\n}\r\n\r\n.switch input {display:none;}\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked + .slider {\r\n  background-color: #009486;\r\n}\r\n\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #009486;\r\n}\r\n\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}\r\n\r\nth{\r\n    text-align: center;\r\n}\r\n\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/templatemangmnt/templatemangmnt.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop></app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n\r\n\r\n<div class=\"container\">\r\n  <div class=\"page-title\">\r\n    <h2>Birthday Templates</h2>\r\n  </div>\r\n  <div class=\"pull-right\"><button class=\"btn btn-primary\" (click)=\"addTemplate.show()\">New Template</button></div><br><br><br>\r\n  <div class=\"panel panel-default\">\r\n<img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n<img class=\"center-block\" *ngIf=\"showLoader\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n    <table class=\"table table-hover\" *ngIf=\"!showLoader\">\r\n      <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n        <tr>\r\n          <th></th>\r\n          <th>Title</th>\r\n          <th>Message</th>\r\n          <th>Status</th>\r\n        </tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let a of birthdayModelArr;\">\r\n          <td>\r\n            <img class=\"circle-image pull-left\" src=\"{{a.templateIconPath}}\"\r\n            />\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 temp-title\">\r\n              {{a.templateTitle}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <div class=\"col-sm-12 temp-msg\">\r\n            {{a.templateMessage}}\r\n            </div>\r\n          </td>\r\n          <td>\r\n            <label class=\"switch\">\r\n                            <input type=\"radio\"(click)=\"selectTemplate(a.templateId)\"  [checked]=\"a.templateStatus.codeId== 13\" name=\"template-status\">\r\n                            <div class=\"slider round\"></div>\r\n                        </label>\r\n          </td>\r\n        </tr>\r\n\r\n\r\n      </tbody>\r\n    </table>\r\n  </div>\r\n\r\n  <!--############## Add new Template ################-->\r\n  <div bsModal #addTemplate=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n      <div class=\"modal-content\">\r\n\r\n        <form> <!--############## start form ###############-->\r\n        <div class=\"modal-header\">\r\n          <button type=\"button\" class=\"close pull-right\" (click)=\"addTemplate.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n       <div class=\"alert alert-success\" *ngIf=\"isSuccess\">\r\n        <strong>Success!</strong> You have successfully created New Template.\r\n      </div>    \r\n          <h4 class=\"modal-title\">New Template</h4>\r\n          \r\n        </div>\r\n        <div class=\"modal-body\">\r\n\r\n          <div class=\"form-group\">\r\n            <label for=\"template\">Title</label>\r\n            <input type=\"text\" name=\"Title\" [(ngModel)]=\"BirthdayModel.templateTitle\" class=\"form-control\" required>\r\n          </div>\r\n\r\n          <div class=\"form-group\">\r\n            <label for=\"template\">Message</label>\r\n            <input type=\"text\" name=\"Message\" [(ngModel)]=\"BirthdayModel.templateMessage\" class=\"form-control\" required>\r\n          </div>\r\n\r\n          <label class=\"control-label\">Select Template Image (Supported File type - JPG)</label>\r\n           <input type=\"file\" accept=\"image/jpeg\" ng2FileSelect [uploader]=\"uploader\" />\r\n            <div class=\"col-md-9\" style=\"margin-bottom: 40px\">\r\n\r\n              <table class=\"table\" style=\"margin-bottom: 5px\">\r\n                <thead>\r\n                  <tr>\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr *ngFor=\"let item of uploader.queue\">\r\n                    <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                    <td *ngIf=\"uploader.isHTML5\">\r\n                      <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                      </div>\r\n                    </td>\r\n                    <td class=\"text-center\">\r\n                      <span *ngIf=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                      <span *ngIf=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                      <span *ngIf=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                    </td>\r\n                    <td nowrap>\r\n                      <button type=\"button\" class=\"btn btn-success btn-xs\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                        </button>\r\n                      <button type=\"button\" class=\"btn btn-warning btn-xs\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                        </button>\r\n                      <button type=\"button\" class=\"btn btn-danger btn-xs\" (click)=\"item.remove()\">\r\n                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                        </button>\r\n                    </td>\r\n                    <!--<td>\r\n                      <!--<input type=\"file\" (change)=\"fileChangeEvent($event)\" placeholder=\"Upload file...\" />-->\r\n                    <!--<input type=\"file\" onchange=\"previewFile()\"><br>-->\r\n                    <!--<img src=\"\" height=\"200\" alt=\"Image preview...\">-->\r\n                    <!--</td>-->\r\n                  </tr>\r\n                </tbody>\r\n              </table>\r\n\r\n              <div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n            <div >\r\n\r\n\r\n             \r\n            </div>\r\n\r\n          </div>\r\n\r\n\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"addTemplate.hide()\">Close</button>\r\n          <button type=\"submit\" class=\"btn btn-primary\" (click)=\"saveBirthdayTemplate()\" >&nbsp;&nbsp;Add&nbsp;&nbsp;</button>\r\n        </div>\r\n        </form><!--############## end form ################-->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/templatemangmnt/templatemangmnt.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_BirthdayTemplate__ = __webpack_require__("../../../../../src/app/model/BirthdayTemplate.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_MDCode__ = __webpack_require__("../../../../../src/app/model/MDCode.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TemplatemangmntComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TemplatemangmntComponent = (function () {
    function TemplatemangmntComponent(commonService, validator) {
        this.commonService = commonService;
        this.validator = validator;
        this.fakeArray = new Array(12);
        this.birthdayModelArr = new Array();
        this.arrTemplates = [];
        this.BirthdayModel = new __WEBPACK_IMPORTED_MODULE_2__model_BirthdayTemplate__["a" /* BirthdayTemplateModel */]();
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/saveTemplateIcon", queueLimit: 1 });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.showLoader = true;
        this.isSuccess = false;
        this.successMessage = "";
        this.validator.validateUser("template-management");
    }
    TemplatemangmntComponent.prototype.ngOnInit = function () {
        this.loadToGrid();
    };
    TemplatemangmntComponent.prototype.loadToGrid = function () {
        var _this = this;
        var urlPublic = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "birthdaytemplate/getTemplateList";
        //let urlPublic = "http://192.0.0.59:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/getTemplateList";
        this.commonService.processGet(urlPublic).subscribe(function (res) {
            console.log("res", res);
            var response = res.getTemplateList;
            if (response.responseCode != 999) {
                // if (res.responseCode != 999) {
                _this.birthdayModelArr = response.responseData;
                _this.showLoader = false;
                // this.birthdayModelArr = res.responseData;
                console.log("this.birthdayModelArr", _this.birthdayModelArr);
                // this.birthdayModelArr.forEach(){
                // }
                _this.birthdayModelArr.forEach(function (model) {
                    console.log("model");
                    var mdcode = new __WEBPACK_IMPORTED_MODULE_4__model_MDCode__["a" /* MDCodeModel */]();
                    mdcode = model.templateStatus;
                    console.log("mdcode:", mdcode);
                    if (mdcode.codeMessage == "Active") {
                        _this.status = model.templateId;
                        console.log("status:", _this.status);
                    }
                });
                console.log("birthday model", _this.birthdayModelArr);
            }
        }, function (error) {
            console.log("console ", error);
        });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.BirthdayModel.templateIconPath = responsePath.responseData;
        };
    };
    TemplatemangmntComponent.prototype.selectTemplate = function (value) {
        console.log("value" + value);
        var urlPublic = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "birthdaytemplate/activation?templateId=" + value;
        //let urlPublic = "http://localhost:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/activation?templateId=" + value;
        this.commonService.processGet(urlPublic).subscribe(function (res) {
            console.log("res", res);
            var response = res.activation;
            if (response.responseCode != 999) {
                // if (res.responseCode != 999) {
                console.log("success");
            }
        }, function (error) {
            console.log("console ", error);
        });
    };
    TemplatemangmntComponent.prototype.saveBirthdayTemplate = function (evt) {
        var _this = this;
        var header = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        console.log("headers", header);
        option.headers = header;
        //  let reader=new FileReader();
        console.log("saveBirthdayTemplate ", this.BirthdayModel);
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "birthdaytemplate/createBirthDayTemplate";
        //let url = "http://192.0.0.59:8080/Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/createBirthDayTemplate";
        this.commonService.processPostWithHeaders(url, this.BirthdayModel, option).subscribe(function (res) {
            var response = res.createBirthDayTemplate;
            if (response.responseCode == 1) {
                // if (res.responseCode == 1) {
                _this.isSuccess = true;
                _this.showLoader = false;
                _this.successMessage = "Birthday Template Added!";
                _this.loadToGrid();
                _this.resetFields(_this.BirthdayModel);
            }
            else {
                _this.successMessage = "Error in birthday Template!";
            }
        }, function (error) {
            console.log("error >>> ", error);
        });
    };
    TemplatemangmntComponent.prototype.resetFields = function (mod) {
        this.showLoader = true;
        this.BirthdayModel.templateMessage = "";
        this.BirthdayModel.templateTitle = "";
        this.isSuccess = false;
    };
    TemplatemangmntComponent.prototype.openNewTemplate = function () {
        var _this = this;
        this.BirthdayModel = new __WEBPACK_IMPORTED_MODULE_2__model_BirthdayTemplate__["a" /* BirthdayTemplateModel */]();
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_file_upload__["FileUploader"]({ url: __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].MEDIA_SERVER + "Majlis-BirthDayTemplateManagement-1.0/service/birthdaytemplate/saveTemplateIcon" });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            //  console.log("zzz",m);
            //  console.log("vvv",v);
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.BirthdayModel.templateIconPath = responsePath.responseData;
        };
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploader.onCompleteItem = function (m, v) {
            var responsePath = JSON.parse(v);
            console.log("aaa", responsePath);
            _this.BirthdayModel.templateIconPath = responsePath.responseData;
        };
    };
    TemplatemangmntComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-templatemangmnt',
            template: __webpack_require__("../../../../../src/app/templatemangmnt/templatemangmnt.component.html"),
            styles: [__webpack_require__("../../../../../src/app/templatemangmnt/templatemangmnt.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */], __WEBPACK_IMPORTED_MODULE_2__model_BirthdayTemplate__["a" /* BirthdayTemplateModel */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__service_login_validator__["a" /* LoignValidator */]) === 'function' && _b) || Object])
    ], TemplatemangmntComponent);
    return TemplatemangmntComponent;
    var _a, _b;
}());
//# sourceMappingURL=templatemangmnt.component.js.map

/***/ }),

/***/ "../../../../../src/app/usermanagement/usermanagement.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".panel-heading{\r\n    background-color: #009688;\r\n    margin-top: 40px;\r\n}\r\n\r\n.input-group.input-group-unstyled input.form-control {\r\n    border-radius: 4px;\r\n}\r\n.input-group-unstyled .input-group-addon {\r\n    border-radius: 4px;\r\n    border: 0px;\r\n    background-color: transparent;\r\n}\r\n\r\n#btnUser{\r\n    margin-left: 170px;\r\n}\r\n\r\n.container{\r\n    margin-top: 50PX;\r\n    padding: 10px 10px 0 10px;\r\n    position: absolute;\r\n\twidth: calc(100% - 51px); \r\n\tmargin-left: 50px;\r\n\tfloat: right;\r\n    overflow-x:hidden;\r\n}\r\n\r\n.circle-user{\r\n width: 40px;\r\n  height: 40px;\r\n  border-radius: 50%;\r\n  font-size: 20px;\r\n  color: #fff;\r\n  line-height: 40px;\r\n  text-align: center;\r\n  background: #009486;\r\n  margin-left: 20px;\r\n}\r\n\r\n\r\n.panel-body{\r\n    margin-top: -30px;\r\n}\r\n\r\n.btn-primary{\r\n    padding: 0 10%; \r\n    margin-top:10px;\r\n    background: #02685e;\r\n    border-color: #02685e;\r\n}\r\n.btn-success{\r\n    background: #c24a25;\r\n    border-color: #c24a25;\r\n    color: #ffffff;\r\n}\r\n\r\n.page-title h2{\r\n  color:#b7b7b7;\r\n  padding-top:-100px;\r\n}\r\n\r\n\r\n/*Model styles*/\r\n.circle-image{\r\n  margin-top: 6px;\r\n  margin-left: 10px;\r\n    width: 40px;\r\n    height: 40px;\r\n    border-radius: 50px;\r\n}\r\n/*toggle styles*/\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\r\n  margin-left: 60px;\r\n}\r\n\r\n.switch input {display:none;}\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked + .slider {\r\n  background-color: #009486;\r\n}\r\n\r\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #009486;\r\n}\r\n\r\ninput:checked + .slider:before {\r\n  -webkit-transform: translateX(26px);\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n.slider.round {\r\n  border-radius: 34px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 50%;\r\n}\r\n\r\n\r\n/*modal*/\r\n.modal-body {\r\n    max-height: calc(100vh - 210px);\r\n    overflow-y: auto;\r\n}\r\n\r\n.glyphicon{\r\n    margin-top: 10px;\r\n}\r\n.glyphicon-ok{\r\n    color: #20b32d;\r\n}\r\n\r\n.glyphicon-remove{\r\n    color: #f02e1a;\r\n}\r\n\r\n.user-name{\r\n    color: #30363a;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.user-email{\r\n    color: #6f7982;\r\n}\r\n\r\n\r\n.modal {\r\n  text-align: center;\r\n  padding: 0!important;\r\n}\r\n\r\n.modal:before {\r\n  content: '';\r\n  display: inline-block;\r\n  height: 100%;\r\n  vertical-align: middle;\r\n  margin-right: -4px;\r\n}\r\n\r\n.modal-dialog {\r\n  display: inline-block;\r\n  text-align: left;\r\n  vertical-align: middle;\r\n}\r\n\r\n.cat-list{\r\n    color: #30363a;\r\n    font-size: 16px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/usermanagement/usermanagement.component.html":
/***/ (function(module, exports) {

module.exports = "<app-pagetop>\r\n</app-pagetop>\r\n<app-side-bar></app-side-bar>\r\n<div class=\"container\">\r\n    <div class=\"page-title\">\r\n        <h2>User Management <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"gridLoader\" style=\"width: 25px\" /> </span></h2>\r\n    </div>\r\n    <div class=\"pull-right\"><button class=\"btn btn-success\" (click)=\"openCreateUser()\">New User</button></div><br><br><br>\r\n    <!-- <img *ngIf=\"isLoading==true\" src=\"../assets/img/loader2.gif\"/> -->\r\n    <div class=\"panel panel-default\">\r\n\r\n        <img class=\"center-block\" *ngIf=\"isLoading\" style=\"margin-top:300px\" src=\"assets/img/MajlisLogoNav.png\" />\r\n        <img class=\"center-block\" *ngIf=\"isLoading\" style=\"margin-top:20px\" src=\"assets/img/loader2.gif\" />\r\n\r\n        <table class=\"table table-hover\" *ngIf=\"!isLoading\">\r\n            <thead style=\"background-color: #02685E;color: azure;box-shadow: 0px 5px 15px#02685E\">\r\n                <tr>\r\n                    <!--<th></th>-->\r\n                    <th>User</th>\r\n                    <th>Assign Groups</th>\r\n                    <!--<th>Assign Group Admin</th>-->\r\n                    <th>Update</th>\r\n                    <th>Status</th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n\r\n                <tr *ngFor=\"let a of arrNewUserModel\">\r\n                    <!--<td>-->\r\n                    <!--<div class=\"circle-user col-sm-1\">{{myName}}</div>-->\r\n                    <!--</td>-->\r\n                    <td>\r\n                        <div class=\"col-sm-12 user-name\">{{a.user.userId}}</div>\r\n                        <div class=\"col-sm-12 user-email\">{{a.user.email}}</div>\r\n                    </td>\r\n                    <td>\r\n                        <button class=\"btn btn-primary\" (click)=\"getUserDetailsById(a.user.userId)\">\r\n                            Assign\r\n                        </button>\r\n                    </td>\r\n                    <td>\r\n                        <button class=\"btn btn-primary\" (click)=\"getUserId(a)\">\r\n                            Update\r\n                        </button>\r\n                    </td>\r\n                    <td>\r\n                        <i *ngIf=\"a.user.status == 1\" class=\"fa fa-check\" aria-hidden=\"true\"></i>\r\n                        <i *ngIf=\"a.user.status !=1\" class=\"fa fa-times\" aria-hidden=\"true\"></i>\r\n                    </td>\r\n\r\n                </tr>\r\n\r\n\r\n            </tbody>\r\n        </table>\r\n\r\n    </div>\r\n\r\n\r\n\r\n\r\n    <!--###########Assing to group modal#############-->\r\n    <div bsModal #assignToGroup=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-md\">\r\n\r\n            <div class=\"modal-content\">\r\n                <form #assignGroupForm=\"ngForm\" (ngSubmit)=\"submitForm($e)\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"assignToGroup.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                        <h4 class=\"modal-title\">Assign Groups</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div *ngFor=\"let a of allGroups;\" class=\"row\">\r\n\r\n                            <div class=\"col-md-9\">\r\n\r\n                                <img class=\"circle-image \" [src]=\"a.groupIconPath\" />\r\n                                <span style=\"display: inline-block;vertical-align: middle;line-height: normal;font-weight: bold\"> {{a.groupTitle}} </span>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <label class=\"switch\" *ngIf=\"a.groupId!=selectedGroupId\">\r\n                            <input type=\"checkbox\" (change)=\"changeGroupAdmin($event,a.groupId)\" [checked]=\"getUserGroupStatus(a.groupId)\"  name=\"template-status\">\r\n                            <div class=\"slider round\"></div>\r\n                                </label>\r\n                                <img src=\"assets/inlineLoader.gif\" *ngIf=\"a.groupId==selectedGroupId\" class=\"pull-right\" style=\"width: 20px\" /> \r\n\r\n                            </div>\r\n\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"assignToGroup.hide()\">Close</button>\r\n                    </div>\r\n                </form>\r\n                <!--################ end form ################-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!--#################Make Group Admin Modal###############-->\r\n    <div bsModal #makeGroupAdmin=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-lg\">\r\n\r\n            <div class=\"modal-content\">\r\n                <form #makeAdminForm=\"ngForm\" (ngSubmit)=\"submitForm($e)\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\">\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"makeGroupAdmin.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                        <h4 class=\"modal-title\">Assign Group Admin</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div class=\"row\" *ngFor=\"let a of fakeArray1;\">\r\n                            <div class=\"col-sm-12\"><br></div>\r\n                            <div class=\"col-sm-1\">\r\n                                <div class=\"circle-user col-sm-1 pull-left\">{{myName}}</div>\r\n                            </div>\r\n\r\n                            <div class=\"col-sm-7\">\r\n                                <div class=\"col-sm-12\">\r\n                                    This is the title of group - This is the title of group </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2\">\r\n                                <label class=\"switch\">\r\n                                <input type=\"checkbox\">\r\n                                <div class=\"slider round\"></div>\r\n                            </label>\r\n                            </div>\r\n                            <div class=\"col-sm-2\">\r\n                                <!--<a class=\"btn pull-right\" (click)=\"makeGroupAdminSetting.show()\">Setting</a>-->\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"makeGroupAdmin.hide()\">Close</button>\r\n                        <button type=\"button\" class=\"btn btn-success\">Save changes</button>\r\n                    </div>\r\n                </form>\r\n                <!--################ end form ################-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!--#################Make Group Admin Setting Modal###############-->\r\n    <div bsModal #makeGroupAdminSetting=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-md\">\r\n\r\n            <div class=\"modal-content\">\r\n                <form #adminSettingForm=\"ngForm\" (ngSubmit)=\"btnUSubmit()\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"makeGroupAdminSetting.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                        <h4 class=\"modal-title\">Update</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">First Name</label>\r\n                            <input type=\"text\" [(ngModel)]=\"user.user.firstName\" name=\"userModelDetail.firstName\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Last Name</label>\r\n                            <input type=\"text\" [(ngModel)]=\"user.user.lastName\" name=\"userModelDetail.lastName\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Telephone</label>\r\n                            <input type=\"text\" max=\"999999999999999\" [(ngModel)]=\"user.user.telephoneNumber\" name=\"userModelDetail.telephoneNumber\" class=\"form-control\"\r\n                                required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Email</label>\r\n                            <input type=\"text\" [(ngModel)]=\"user.user.email\" name=\"userModelDetail.email\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">User Status</label>\r\n                            <select [(ngModel)]=\"user.user.status\" name=\"newUserModel.user.status\" class=\"form-control\">\r\n                                <option  [value]=\"2\">Inactive</option>\r\n                                <option  [value]=\"1\">Active</option>\r\n                            \r\n                        </select>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Address</label>\r\n                            <input type=\"text\" [(ngModel)]=\"user.user.address\" name=\"userModelDetail.address\" class=\"form-control\">\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Expiry Date</label>\r\n                            <input type=\"date\" [(ngModel)]=\"user.expiryDate\" name=\"userModelDetail.expiryDate\" class=\"form-control\">\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">No. of allowed notifications (\"-1\" if unlimited)</label>\r\n                            <input type=\"text\" [(ngModel)]=\"user.allowedNotificationCnt\" name=\"allowedNotificationCnt\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n\r\n                    </div>\r\n\r\n                    <div class=\"modal-footer\">\r\n                        <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"updateUserLoade\" style=\"width: 25px\" /> </span>\r\n                        <button type=\"button\" class=\"btn btn-default\" (click)=\"makeGroupAdminSetting.hide()\">Close</button>\r\n                        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"updateUserLoade\" (click)=\"saveUserChanges(event)\">Save changes</button>\r\n                    </div>\r\n                </form>\r\n                <!--################ end form ################-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n    <!--   New User Modal-->\r\n    <div bsModal #makeUserSetting=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-md\">\r\n\r\n            <div class=\"modal-content\">\r\n                <form #makeUserSettingForm=\"ngForm\" (ngSubmit)=\"btnUSubmit()\">\r\n                    <!--########## start form ########-->\r\n                    <div class=\"modal-header\" style=\"background-color: #02685E;color: azure\">\r\n                        <!-- <img *ngIf=\"isLoading==true\" src=\"../assets/img/loader2.gif\"/> -->\r\n                        <button type=\"button\" class=\"close pull-right\" (click)=\"makeUserSetting.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n\r\n                        <h4 class=\"modal-title\">Create User</h4>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">User Name</label>\r\n                            <input placeholder=\"This will be the User ID of user. User should log with this User ID\" type=\"text\" [(ngModel)]=\"newUserModel.user.userId\"\r\n                                name=\"newUserModel.user.userId\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">First Name</label>\r\n                            <input type=\"text\" [(ngModel)]=\"newUserModel.user.firstName\" name=\"newUserModel.user.firstName\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Last Name</label>\r\n                            <input type=\"text\" [(ngModel)]=\"newUserModel.user.lastName\" name=\"newUserModel.user.lastName\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Telephone</label>\r\n                            <input type=\"number\" [(ngModel)]=\"newUserModel.user.telephoneNumber\" name=\"newUserModel.user.telephoneNumber\" class=\"form-control\"\r\n                                required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Email</label>\r\n                            <input type=\"text\" [(ngModel)]=\"newUserModel.user.email\" name=\"newUserModel.user.email\" class=\"form-control\" required>\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">User Status</label>\r\n                            <!--<input type=\"text\" [(ngModel)]=\"newUserModel.user.status\" name=\"newUserModel.user.status\" class=\"form-control\" required>-->\r\n                            <select [(ngModel)]=\"newUserModel.user.status\" name=\"newUserModel.user.status\" class=\"form-control\">\r\n                            <option  [ngValue]=\"1\">Active</option>\r\n                            <option  [ngValue]=\"2\">Inactive</option>\r\n                        </select>\r\n\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Address</label>\r\n                            <input type=\"text\" [(ngModel)]=\"newUserModel.user.address\" name=\"newUserModel.user.address\" class=\"form-control\">\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Allowed Notification Count</label>\r\n                            <input type=\"number\" [(ngModel)]=\"newUserModel.allowedNotificationCnt\" name=\"newUserModel.allowedNotificationCnt\" class=\"form-control\">\r\n                        </div>\r\n\r\n                        <div class=\"form-group\">\r\n                            <label for=\"template\">Expiry Date</label>\r\n                            <input type=\"date\" [(ngModel)]=\"newUserModel.expiryDate\" name=\"newUserModel.expiryDate\" class=\"form-control\">\r\n                        </div>\r\n\r\n\r\n                    </div>\r\n\r\n                    <div class=\"modal-footer\">\r\n                        <span><img src=\"assets/inlineLoader.gif\" *ngIf=\"createUserLoader\" style=\"width: 25px\" /> </span>\r\n\r\n                        <button type=\"reset\" class=\"btn btn-default\" (click)=\"makeUserSetting.hide()\">Close</button>\r\n                        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"createUserLoader\" (click)=\"createUser(evt)\">Save changes</button>\r\n                    </div>\r\n                </form>\r\n                <!--################ end form ################-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!--   End User Modal-->\r\n\r\n    <!--####################Show Update Modal#######################-->\r\n    <div bsModal #showCategory=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\r\n        aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-sm\">\r\n\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <button type=\"button\" class=\"close pull-right\" (click)=\"showCategory.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                    <h4 class=\"modal-title\">Categories</h4>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n\r\n\r\n\r\n                    <div class=\"row\" *ngFor=\"let a of fakeArray1;\">\r\n                        <div class=\"col-sm-12\"><br></div>\r\n\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"cat-list col-sm-12 text-center \">\r\n                                Birthday\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-default\" (click)=\"showCategory.hide()\">Close</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n<div bsModal #success=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"modal-header\" style=\"background-color: #449D44; color: azure\">\r\n                <button type=\"button\" class=\"close pull-right\" (click)=\"success.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                <h4 class=\"modal-title\">Operation Completed</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <p>{{successMessage}}</p>\r\n\r\n            </div>\r\n\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-default\" (click)=\"success.hide()\">Close</button>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div bsModal #fail=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-md\">\r\n\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"modal-header\" style=\"background-color: chocolate;color: azure\">\r\n                <button type=\"button\" class=\"close pull-right\" (click)=\"fail.hide()\" aria-label=\"Close\">\r\n                                    <span aria-hidden=\"true\">&times;</span>\r\n                                </button>\r\n                <h4 class=\"modal-title\">Operation Failed!</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <p>{{failMessage}}</p>\r\n\r\n            </div>\r\n\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-default\" (click)=\"fail.hide()\">Close</button>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/usermanagement/usermanagement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_NewUserModel__ = __webpack_require__("../../../../../src/app/model/NewUserModel.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__ = __webpack_require__("../../../../../src/app/service/common-web.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__model_Group__ = __webpack_require__("../../../../../src/app/model/Group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__model_User__ = __webpack_require__("../../../../../src/app/model/User.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_login_validator__ = __webpack_require__("../../../../../src/app/service/login-validator.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsermanagementComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { DatepickerModule } from 'ngx-bootstrap/datepicker';
// import { DialogService } from "ng2-bootstrap-modal";
var UsermanagementComponent = (function () {
    function UsermanagementComponent(commonService, validator) {
        this.commonService = commonService;
        this.validator = validator;
        this.gridLoader = false;
        this.updateUserLoade = false;
        this.isSuccess = false;
        this.isLoading = true;
        this.successMessage = "";
        this.myName = "Kushan";
        this.fakeArray = new Array(12);
        this.fakeArray1 = new Array(10);
        this.userModel = new Array();
        this.newUserModel = new __WEBPACK_IMPORTED_MODULE_7__model_User__["a" /* User */]();
        this.user = new __WEBPACK_IMPORTED_MODULE_7__model_User__["a" /* User */]();
        this.arrNewUserModel = new Array();
        this.userModelDetail = new __WEBPACK_IMPORTED_MODULE_2__model_NewUserModel__["a" /* NewUserModel */]();
        this.userGrpModel = new Array();
        this.userGrpModelDetail = new Array();
        this.totalItems = 64;
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.groupModel = new Array();
        this.groupModelDetail = new __WEBPACK_IMPORTED_MODULE_6__model_Group__["a" /* GroupModel */]();
        this.validator.validateUser("user-management");
    }
    UsermanagementComponent.prototype.ngOnInit = function () {
        this.myName = this.myName.substring(0, 1);
        this.loadUserToGrid();
    };
    UsermanagementComponent.prototype.showChildModal = function () {
        this.childModal.show();
    };
    UsermanagementComponent.prototype.hideChildModal = function () {
        this.childModal.hide();
    };
    UsermanagementComponent.prototype.openCreateUser = function () {
        this.makeUserSetting.show();
        this.newUserModel = new __WEBPACK_IMPORTED_MODULE_7__model_User__["a" /* User */]();
    };
    UsermanagementComponent.prototype.changeGroupAdmin = function ($event, id) {
        var _this = this;
        console.log("Status... ", $event.target.checked);
        this.selectedGroupId = id;
        var header = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.selectedUserId);
        header.set("groupId", id);
        header.set("operation", $event.target.checked);
        option.headers = header;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "users/changeGroupUser";
        var user = new __WEBPACK_IMPORTED_MODULE_7__model_User__["a" /* User */]();
        this.commonService.processPostWithHeaders(url, user, option).subscribe(function (response) {
            if (response.changeGroupUser.responseCode == 1) {
                _this.getUserDetailsById(_this.selectedUserId);
            }
            else {
                _this.failMessage = "Cannot complete the operation at this time!";
                _this.fail.show();
            }
            _this.selectedGroupId = 0;
        }, function (error) {
            _this.failMessage = "Cannot complete the operation at this time!";
            _this.fail.show();
            _this.selectedGroupId = 0;
        });
    };
    UsermanagementComponent.prototype.createUser = function (event) {
        var _this = this;
        this.createUserLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "users/addUser";
        var header = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        option.headers = header;
        this.newUserModel.user.groups = [{
                "groupId": "GroupAdminGrp",
                "groupName": "Group Admin"
            }];
        this.commonService.processPostWithHeaders(url, this.newUserModel, option).subscribe(function (res) {
            var response = res;
            console.log("response", response);
            if (response.addUser.flag == 100) {
                _this.isSuccess = true;
                _this.successMessage = "User created successfully!";
                _this.isLoading = false;
                _this.resetFields(_this.newUserModel);
                _this.successMessage = "User has been created successfully. Password will be sent to the email address";
                _this.success.show();
            }
            else if (response.addUser.flag == 808) {
                _this.failMessage = response.addUser.exceptionMessages[0];
                _this.fail.show();
            }
            else {
                _this.failMessage = "Cannot create the user at this time";
                _this.fail.show();
            }
            _this.createUserLoader = false;
            _this.loadUserToGrid();
        }, function (error) {
            _this.createUserLoader = false;
            _this.failMessage = "Error in connection. Try again later!";
            _this.fail.show();
        });
    };
    UsermanagementComponent.prototype.resetFields = function (mod) {
        this.isSuccess = false;
        mod.user.userId = "";
        mod.user.firstName = "";
        mod.user.lastName = "";
        mod.user.telephoneNumber = "";
        mod.user.email = "";
        mod.user.address = "";
        mod.allowedNotificationCnt = "";
    };
    UsermanagementComponent.prototype.loadUserToGrid = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "users/getCMSUsers?start=0&limit=1000";
        var header = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]();
        header.set("userId", "super");
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        option.headers = header;
        console.log("hhh ", header);
        this.commonService.processGetWithHeaders(url, option).subscribe(function (res) {
            // this.userModel = res.users.responseData;
            _this.arrNewUserModel = res.users.data;
            _this.isLoading = false;
        }, function (error) {
            _this.isLoading = false;
            console.log("error ", error);
        });
    };
    UsermanagementComponent.prototype.getUserId = function (userId) {
        this.user = userId;
        if (this.user.allowedNotificationCnt == 0) {
            this.user.allowedNotificationCnt = -1;
            console.log("byuserId>>>>>>>>>>>", userId);
        }
        this.updateUserLoade = false;
        this.makeGroupAdminSetting.show();
    };
    UsermanagementComponent.prototype.getUserDetailsById = function (userId) {
        var _this = this;
        this.selectedUserId = userId;
        this.gridLoader = true;
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "users/getCmsUserGroup?userId=" + userId;
        this.commonService.processGet(url).subscribe(function (res) {
            console.log(">>>> ", res.getCmsUserGroup);
            _this.userGroupList = new Array();
            _this.allGroups = new Array();
            if (res.getCmsUserGroup.responseCode == 1) {
                _this.userGroupList = res.getCmsUserGroup.responseData.userGroups;
                _this.allGroups = res.getCmsUserGroup.responseData.allGroups;
            }
            else {
                _this.failMessage = "Cannot fetch the data at this time";
                _this.fail.show();
            }
            _this.gridLoader = false;
        }, function (error) {
            _this.failMessage = "Cannot fetch the data at this time";
            _this.fail.show();
            _this.gridLoader = false;
            console.log("error >>> ", error);
        });
        this.assignToGroup.show();
    };
    UsermanagementComponent.prototype.getUserGroupStatus = function (id) {
        var idx = this.userGroupList.findIndex(function (e) { return e.groupId == id; });
        if (idx == -1) {
            return false;
        }
        else {
            return true;
        }
    };
    UsermanagementComponent.prototype.getGroupsById = function (userId) {
        var _this = this;
        console.log("GroupsById>>", userId);
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "groups/getAllGroupsById?user=" + userId;
        this.commonService.processGet(url).subscribe(function (res) {
            _this.groupModel = res.getAllGroupsById.responseData;
            console.log("groupModel >>> ", _this.groupModel);
        }, function (error) {
            console.log("error >>> ", error);
        });
        this.showGroups.show();
    };
    UsermanagementComponent.prototype.saveUserChanges = function (evt) {
        var _this = this;
        this.updateUserLoade = true;
        console.log("saveUserChanges ", this.userModelDetail);
        var url = __WEBPACK_IMPORTED_MODULE_3__app_module__["b" /* AppParams */].BASE_PATH + "users/modifyCmsUser";
        var header = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* Headers */]();
        var option = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]();
        header.set("userId", this.validator.getUserId());
        header.set("room", "MajlisCMSRoom");
        header.set("department", "DefaultDepartment");
        header.set("branch", "HeadOffice");
        header.set("countryCode", "AE");
        header.set("division", "MajlisAE");
        header.set("organization", "majlis");
        header.set("system", "MajlisCMS");
        option.headers = header;
        option.headers = header;
        this.commonService.processPostWithHeaders(url, this.user, option).subscribe(function (res) {
            _this.isSuccess = true;
            var response = res.updateGroup;
            try {
                if (response.responseCode == 1) {
                    _this.makeGroupAdminSetting.show();
                    _this.successMessage = "Changes saved successfully!";
                    _this.success.show();
                }
                else {
                    _this.failMessage = "Operation failed! Cannot change the user details at this time.";
                    _this.fail.show();
                }
                _this.updateUserLoade = false;
            }
            catch (res) {
                _this.failMessage = "Operation failed!";
                _this.fail.show();
                _this.updateUserLoade = false;
            }
        }, function (error) {
            _this.failMessage = "Operation failed!";
            _this.fail.show();
        });
    };
    UsermanagementComponent.prototype.pageChanged = function (event) {
        console.log('Page changed to: ' + event.page);
        console.log('Number items per page: ' + event.itemsPerPage);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("success"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "success", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fail"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "fail", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('childModal'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */]) === 'function' && _a) || Object)
    ], UsermanagementComponent.prototype, "childModal", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("assignToGroup"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "assignToGroup", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("makeGroupAdminSetting"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "makeGroupAdminSetting", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("makeUserSetting"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "makeUserSetting", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("showGroups"), 
        __metadata('design:type', Object)
    ], UsermanagementComponent.prototype, "showGroups", void 0);
    UsermanagementComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-usermanagement',
            template: __webpack_require__("../../../../../src/app/usermanagement/usermanagement.component.html"),
            styles: [__webpack_require__("../../../../../src/app/usermanagement/usermanagement.component.css")]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_common_web_service__["a" /* CommonWebService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_8__service_login_validator__["a" /* LoignValidator */]) === 'function' && _c) || Object])
    ], UsermanagementComponent);
    return UsermanagementComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=usermanagement.component.js.map

/***/ }),

/***/ "../../../../../src/app/userprompt/userprompt.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/userprompt/userprompt.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  userprompt works!\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/userprompt/userprompt.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserpromptComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserpromptComponent = (function () {
    function UserpromptComponent() {
    }
    UserpromptComponent.prototype.ngOnInit = function () {
    };
    UserpromptComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-userprompt',
            template: __webpack_require__("../../../../../src/app/userprompt/userprompt.component.html"),
            template: "<div class=\"modal-dialog\">\n                <div class=\"modal-content\">\n                   <div class=\"modal-header\">\n                     <button type=\"button\" class=\"close\" (click)=\"close()\">&times;</button>\n                     <h4 class=\"modal-title\">{{title || 'Prompt'}}</h4>\n                   </div>\n                   <div class=\"modal-body\">\n                    <label>{{question}}</label><input type=\"text\" class=\"form-control\" [(ngModel)]=\"message\" name=\"name\" >\n                   </div>\n                   <div class=\"modal-footer\">\n                     <button type=\"button\" class=\"btn btn-primary\" (click)=\"apply()\">OK</button>\n                     <button type=\"button\" class=\"btn btn-default\" (click)=\"close()\" >Cancel</button>\n                   </div>\n                 </div>\n                </div>",
            styles: [__webpack_require__("../../../../../src/app/userprompt/userprompt.component.css")]
        }), 
        __metadata('design:paramtypes', [])
    ], UserpromptComponent);
    return UserpromptComponent;
}());
//# sourceMappingURL=userprompt.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map